//
//  SwipeInitResultsViewController.m
//  MagTek.iDynamo.ObjC
//
//  Created by agharris73 on 9/19/13.
//  Copyright (c) 2013 Mercury. All rights reserved.
//

#import "SwipeInitResultsViewController.h"
#import "iROSAppDelegate.h"

@interface SwipeInitResultsViewController ()
{
#define kBaseUrlTsysSanbox @"http://stagegw.transnox.com:9300/servlets/TransNox_API_Server"
#define kBaseUrlTsysOriginal @"https://stagegw.transnox.com/servlets/TransNox_API_Server"
}

@end

@implementation SwipeInitResultsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton *backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 20, 70, 40)];
    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    
    iROSAppDelegate *appDelegate = (iROSAppDelegate*)[[UIApplication sharedApplication] delegate];
    EncryptedSwipeData *esd = appDelegate.encryptedSwipeData;
    self.maskedTrack2DataResult.text = esd.track2Masked;
    self.encryptedTrack2DataResult.text = esd.track2Encrypted;
    self.ksnResult.text = esd.ksn;

    
    NSLog(@"ESD  :%@ \n %@ \n %@ \n%@ \n%@ \n%@",esd.track1Masked,
          esd.track2Encrypted,
           esd.CardLast4,
           esd.CardPANLength,
          esd.CapMagStripeEncryption,
           esd.CardStatus);
    
    [self CardVarification];
    
}




-(void)CardVarification  // for in app integration
{
    iROSAppDelegate *appDelegate = (iROSAppDelegate*)[[UIApplication sharedApplication] delegate];
    EncryptedSwipeData *esd = appDelegate.encryptedSwipeData;
      
    ServerConnectionClass* network = [[ServerConnectionClass alloc]init] ;
    self.connObject = network;
    self.connObject.delegate = self;
    
    NSString* xmlString=[[NSString alloc]initWithFormat:@"{\"deviceID\":\"%@\" ,\"transactionKey\":\"%@\",\"cardDataSource\":\"%@\",\"currencyCode\":\"%@\",\"track1Data\":\"%@\" ,\"track2Data\":\"%@\" ,\"cardHolderName\":\"%@\",\"ksn\":\"%@\" ,\"tokenRequired\":\"%@\" ,\"firstName\":\"%@\" ,\"lastName\":\"%@\" }", @"66600000456701", @"39NFVH8G8EOYTFIR6X9PSH369Z38F8W1", @"SWIPE", @"INR", esd.track1Masked, esd.track2Masked ,esd.CardName, esd.ksn, @"Y", @"Harendra", @"Sharma"];
    
    NSString* jsonRequest=[[NSString alloc]initWithFormat:@"{\"CardAuthentication\":\%@ }",xmlString];
    [self.connObject CallWebApiPostData:[NSURL URLWithString:kBaseUrlTsysOriginal] PayLoadStr:jsonRequest];
    
}






#pragma mark- MyCabuUserNetwork delegate methods

- (void) connectionEndWithError:(NSError*)error
{
    [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [self removeConnectionClassObject];
}

- (void) connectionEndedSuccefullyWithData:(NSData*)data
{
    NSError *localError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    NSLog(@"MY dictionary====%@",dictionary);
    if (dictionary != nil)
    {
        }
    else
    {
        [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    
    [self removeConnectionClassObject];
}

- (void) removeConnectionClassObject
{
    self.connObject.delegate = nil;
    self.connObject = nil;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (void) backBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
