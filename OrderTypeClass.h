//
//  OrderTypeClass.h
//  iROS
//
//  Created by Dex on 15/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderTypeClass : UIView

@property (nonatomic, retain) UIView *bgVw;
@property (nonatomic, retain) UIButton* closeBtn;
@property (nonatomic, retain) UIButton *takeAwayBtn;
@property (nonatomic, retain) UIButton *dineInBtn;
@property (nonatomic, retain) UIButton *barBtn;

@end
