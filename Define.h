//
//  Define.h
//  iROS
//
//  Created by iMac Apple on 29/01/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//





#ifndef Define_h
#define Define_h


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)



#define kMobileOs @"iOS"
#define kMegensaServerURL @"https://mppg.magensa.net/MPPG/service.asmx"
#define kProcesscs10URL @"http://184.173.41.114/ws/process_cs.php?cmd=payment&pl="
#define kProcessTI10URL @"http://184.173.41.114/ws/process_ti.php?cmd=payment&pl="

#define kBaseUrl             @"http://184.173.41.114/ws/category.php?cmd="
#define kCategoryUrl         @"http://184.173.41.114/ws/category.php?cmd=category&pl="
#define kItemsUrl            @"http://184.173.41.114/ws/items.php?cmd=items&pl="
#define kSubmitOrderUrl      @"http://184.173.41.114/ws/order_submit.php?cmd=ordersubmit&pl="
#define kLoginUrl            @"http://184.173.41.114/ws/login.php?cmd=login&pl="
#define kAllOrdersUrl        @"http://184.173.41.114/ws/order_all.php?cmd=orderall&pl="
#define kCurrentOrdersUrl    @"http://184.173.41.114/ws/order_current.php?cmd=ordercurrent&pl="
#define kCurrentOrderDetailsUrl    @"http://184.173.41.114/ws/order_item_detail.php?cmd=orderitemdetail&pl="
#define kWaiterList  @"http://184.173.41.114/ws/waiter.php?cmd=list&pl="
#define kwaiterEntry  @"http://184.173.41.114/ws/entry.php?cmd=entry&pl="
#define kwaiterClockIn @"http://184.173.41.114/ws/clock_in.php?cmd=in&pl="
#define kwaiterClockOut @"http://184.173.41.114/ws/clock_out.php?cmd=out&pl="
#define kwaiterExit  @"http://184.173.41.114/ws/exit.php?cmd=exit&pl="
#define kTipAdjList  @"http://184.173.41.114/ws/process_list.php?cmd=orderall&pl="
#define kDiscountType  @"http://184.173.41.114/ws/promo.php?cmd=promo&pl="

// Color codes
#define kLightOrangeColor  [UIColor colorWithRed:234/255.0f  green:122/255.0f blue:77/255.0f alpha:1.0]
#define kOrangeColor  [UIColor colorWithRed:215.0f/255.0f green:72.0f/255.0f blue:17.0f/255.0f alpha:1]
#define kVioletColor  [UIColor colorWithRed:63.0/255.0f green:8.0/255.0f blue:102.0/255.0f alpha:1]
#define kGrayColor [UIColor colorWithRed:135.0/255.0f green:135.0/255.0f blue:135.0/255.0f alpha:1]
#define kDarkBrownColor  [UIColor colorWithRed:121/255.0f green:50/255.0f blue:4/255.0f alpha:1.0]



///UserDefault keys for Card details///
#define kCardNameKey               @"card_name"
#define kCardNumberKey             @"card_number"
#define kCardTypeKey               @"card_type"
#define kExpYearKey                @"exp_year"
#define kExpMonthKey               @"exp_month"
#define kCCVKey                    @"ccv"

#define kMobileOs                  @"iOS"
#define kDeviceToken               @"deviceToken"
#define kUserLoginStatus           @"isloggedIn"
#define kuserId                    @"User_id"
#define kuserType                  @"UserRole"
#define kitemType                  @"itemCategory"
#define kBookedTables_bar          @"TablesBookedforBar"
#define kBookedTables_food         @"TablesBookedforfood"

#define kmodifier1                 @"modifier1"
#define kmodifier2                 @"modifier2"
#define kmodifier3                 @"modifier3"
#define kmodifier4                 @"modifier4"
#define kmanagerID                 @"managerID"
#define kComplimentry              @"complimentry"
#define ksplInstr                  @"specialInstructions"

#define kIsEmpLoggedIn   @"isEmpLoggedIn"

#endif
