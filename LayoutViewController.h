//
//  LayoutViewController.h
//  Restaurant Menu
//
//  Created by Dex on 16/07/14.
//  Copyright (c) 2014 Dex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "partyDetailClass.h"

@interface LayoutViewController : UIViewController<UIScrollViewDelegate,UIAlertViewDelegate>
{
    UIScrollView *scrollView_layout;
    int tableIdfrom;
}
@property (nonatomic,retain)  NSString *userType;
@property (nonatomic, retain) partyDetailClass *uiasView_partyDetailClass;

@end
