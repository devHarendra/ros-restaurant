//
//  OrderDetailsClass.m
//  Restaurant Menu
//
//  Created by Dex on 16/07/14.
//  Copyright (c) 2014 Dex. All rights reserved.
//

#import "OrderDetailsClass.h"

@implementation OrderDetailsClass

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:0.8];
        
        UIView *dummyView=[[UIView alloc] init];
        [dummyView setBackgroundColor:[UIColor whiteColor]];
        [dummyView setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-800)/2, ([UIScreen mainScreen].bounds.size.height-450)/2,800 , 450)];
        self.bgVw=dummyView;
        [dummyView release];
        [self.bgVw.layer setBorderColor:[UIColor colorWithRed:77.0f/255.0f green:77.0f/255.0f blue:77.0f/255.0f alpha:1].CGColor];
        [self.bgVw.layer setBorderWidth:6.0f];
        [self.bgVw.layer setCornerRadius:4.0f];
        [self addSubview:self.bgVw];
        
        
        
        self.closeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeBtn setFrame:CGRectMake( self.bgVw.frame.size.width-60,1, 60, 60)];
        [self.closeBtn setBackgroundColor:[UIColor clearColor]];
        [self.closeBtn setExclusiveTouch:YES];
        [self.bgVw addSubview:self.closeBtn];
        [self.closeBtn setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        
        UIImageView *imgView=[[UIImageView alloc] init];
        self.imgView=imgView;
         [imgView release];
        [self.imgView setFrame:CGRectMake(10, 10, 300, 350)];
        [self.imgView setBackgroundColor:[UIColor clearColor]];
        [self.imgView setImage:[UIImage imageNamed:@"image4.png"]];
        [self.bgVw addSubview:self.imgView];
       
        
//        UILabel *actualCostLbl=[[UILabel alloc] init];
//        [actualCostLbl setFrame:CGRectMake(10, 365, 90, 30)];
//        [actualCostLbl setBackgroundColor:[UIColor clearColor]];
//        [actualCostLbl setTextAlignment:NSTextAlignmentLeft];
//        [actualCostLbl setTextColor:kOrangeColor];
//        [actualCostLbl setText:@"Actual Cost"];
//        [actualCostLbl setFont:[UIFont systemFontOfSize:15]];
//        [self.bgVw addSubview:actualCostLbl];
//        [actualCostLbl release];
//        
//        UITextField *actualCstTxtFld=[[UITextField alloc] init];
//        [actualCstTxtFld setDelegate:self];
//        [actualCstTxtFld setFrame:CGRectMake(10, 400, 90, 40)];
////        [actualCstTxtFld.layer setBorderColor:[UIColor lightGrayColor].CGColor];
////        [actualCstTxtFld.layer setBorderWidth:1.0f];
//        [actualCstTxtFld setText:@"$10.00"];
//        [actualCstTxtFld setFont:[UIFont systemFontOfSize:16]];
//        [self.bgVw addSubview:actualCstTxtFld];
//        [actualCstTxtFld release];
//        [actualCstTxtFld setTextAlignment:NSTextAlignmentCenter];
//        [actualCstTxtFld setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
//        
//        UILabel *discountLbl=[[UILabel alloc] init];
//        [discountLbl setFrame:CGRectMake(100, 365, 90, 30)];
//        [discountLbl setBackgroundColor:[UIColor clearColor]];
//        [discountLbl setTextAlignment:NSTextAlignmentCenter];
//        [discountLbl setTextColor:kOrangeColor];
//        [discountLbl setText:@"Discount"];
//        [discountLbl setFont:[UIFont systemFontOfSize:15]];
//        [self.bgVw addSubview:discountLbl];
//        [discountLbl release];
//        
//        UITextField *discountTxtFld=[[UITextField alloc] init];
//        [discountTxtFld setDelegate:self];
//        [discountTxtFld setFrame:CGRectMake(100, 400, 90, 40)];
//        [discountTxtFld.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//        [discountTxtFld.layer setBorderWidth:1.0f];
//        [discountTxtFld setFont:[UIFont systemFontOfSize:16]];
//        [discountTxtFld setTag:10];
//        [self.bgVw addSubview:discountTxtFld];
//        [discountTxtFld release];
//         [discountTxtFld setTextAlignment:NSTextAlignmentCenter];
//        [discountTxtFld setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
//        [discountTxtFld setKeyboardType:UIKeyboardTypeDecimalPad];
//        
//        UILabel *taxLbl=[[UILabel alloc] init];
//        [taxLbl setFrame:CGRectMake(190, 365, 60, 30)];
//        [taxLbl setBackgroundColor:[UIColor clearColor]];
//        [taxLbl setTextAlignment:NSTextAlignmentCenter];
//        [taxLbl setTextColor:kOrangeColor];
//        [taxLbl setText:@"Tax"];
//        [taxLbl setFont:[UIFont systemFontOfSize:15]];
//        [self.bgVw addSubview:taxLbl];
//        [taxLbl release];
//
//        UITextField *taxTxtFld=[[UITextField alloc] init];
//        [taxTxtFld setDelegate:self];
//        [taxTxtFld setFrame:CGRectMake(190, 400, 60, 40)];
////        [taxTxtFld.layer setBorderColor:[UIColor lightGrayColor].CGColor];
////        [taxTxtFld.layer setBorderWidth:1.0f];
//        [taxTxtFld setFont:[UIFont systemFontOfSize:16]];
//        [taxTxtFld setTag:10];
//        [taxTxtFld setText:@"12.5%"];
//        [taxTxtFld setTextAlignment:NSTextAlignmentCenter];
//        [self.bgVw addSubview:taxTxtFld];
//        [taxTxtFld release];
//        [taxTxtFld setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];

        UILabel *totalAmtLbl=[[UILabel alloc] init];
        [totalAmtLbl setFrame:CGRectMake(15, 375, 70, 40)];
        [totalAmtLbl setBackgroundColor:[UIColor clearColor]];
        [totalAmtLbl setTextAlignment:NSTextAlignmentLeft];
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
           [totalAmtLbl setTextColor:kDarkBrownColor];
        }
        else
        {
           [totalAmtLbl setTextColor:kOrangeColor];
        }
        
        [totalAmtLbl setText:@"Price :"];
        [totalAmtLbl setFont:[UIFont systemFontOfSize:20]];
        [self.bgVw addSubview:totalAmtLbl];
        [totalAmtLbl release];
        
        UITextField *totalAmtTxtFld=[[UITextField alloc] init];
        [totalAmtTxtFld setText:@""];
        [totalAmtTxtFld setFrame:CGRectMake(80, 375, 100, 40)];
        self.totalAmtTxtFld=totalAmtTxtFld;
        
         [totalAmtTxtFld release];
       
        
        [self.totalAmtTxtFld setFont:[UIFont systemFontOfSize:20]];
        [self.totalAmtTxtFld setTag:10];
        [self.totalAmtTxtFld setTextAlignment:NSTextAlignmentLeft];
        [self.bgVw addSubview:self.totalAmtTxtFld];
        [self.totalAmtTxtFld setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
       
        
        UILabel *popupHeader=[[UILabel alloc] init];
        self.popupHeader=popupHeader;
         [popupHeader release];
        [self.popupHeader setFrame:CGRectMake(320, 10, self.bgVw.frame.size.width-390, 50)];
        [self.popupHeader setBackgroundColor:[UIColor clearColor]];
        [self.popupHeader setTextAlignment:NSTextAlignmentLeft];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
           [self.popupHeader setTextColor:kDarkBrownColor];
        }
        else
        {
           [self.popupHeader setTextColor:kOrangeColor];
        }
        
        [self.popupHeader setText:@"PHO Beef"];
        [self.popupHeader setFont:[UIFont systemFontOfSize:25]];
        [self.bgVw addSubview:self.popupHeader];
       
        
        UIWebView *itemDetails=[[UIWebView alloc] init];
        self.itemDetails=itemDetails;
        [self.itemDetails release];
        [self.itemDetails setFrame:CGRectMake(315, 50, self.bgVw.frame.size.width-330, 62)];
        [self.itemDetails setBackgroundColor:[UIColor clearColor]];
        [self.itemDetails setOpaque:NO];
//        [self.itemDetails setText:@"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."];
       // [self.itemDetails setFont:[UIFont systemFontOfSize:14]];
        self.itemDetails.dataDetectorTypes=UIDataDetectorTypeAll;
        [self.bgVw addSubview:self.itemDetails];
       // [itemDetails setTextAlignment:NSTextAlignmentJustified];
        
        
        UILabel *qtyLbl=[[UILabel alloc] init];
        [qtyLbl setFrame:CGRectMake(320, 115, 100, 50)];
        [qtyLbl setBackgroundColor:[UIColor clearColor]];
        [qtyLbl setTextAlignment:NSTextAlignmentLeft];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [qtyLbl setTextColor:kDarkBrownColor];
        }
        else
        {
            [qtyLbl setTextColor:kOrangeColor];
        }
        
        [qtyLbl setText:@"Quantity:"];
        [qtyLbl setFont:[UIFont systemFontOfSize:18]];
        [self.bgVw addSubview:qtyLbl];
        [qtyLbl release];

        UIButton *increaseQtyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [increaseQtyBtn setFrame:CGRectMake(440, 120, 40, 40)];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
             [increaseQtyBtn setBackgroundColor:kDarkBrownColor];
        }
        else
        {
            [increaseQtyBtn setBackgroundColor:kOrangeColor];
        }
        [increaseQtyBtn setTitle:@"+" forState:UIControlStateNormal];
        [increaseQtyBtn.titleLabel setFont:[UIFont systemFontOfSize:20.0f]];
        [increaseQtyBtn addTarget:self action:@selector(increaseQty:) forControlEvents:UIControlEventTouchUpInside];
        [self.bgVw addSubview:increaseQtyBtn];
        
        UILabel *qtyNumLbl=[[UILabel alloc] init];
        self.qtyNumLbl=qtyNumLbl;
        [qtyNumLbl release];
        [ self.qtyNumLbl setFrame:CGRectMake(480, 120, 70, 40)];
        [ self.qtyNumLbl setBackgroundColor:[UIColor clearColor]];
        [ self.qtyNumLbl setTextAlignment:NSTextAlignmentCenter];
        [ self.qtyNumLbl setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
        [ self.qtyNumLbl setText:@"1"];
        [ self.qtyNumLbl setFont:[UIFont systemFontOfSize:20]];
        [ self.qtyNumLbl.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [ self.qtyNumLbl.layer setBorderWidth:1.0f];
        [self.bgVw addSubview: self.qtyNumLbl];
        
        
        UIButton * decreaseQtyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [decreaseQtyBtn setFrame:CGRectMake(550, 120, 40, 40)];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [decreaseQtyBtn setBackgroundColor:kDarkBrownColor];
        }
        else
        {
            [decreaseQtyBtn setBackgroundColor:kOrangeColor];
        }
       
        [decreaseQtyBtn setTitle:@"-" forState:UIControlStateNormal];
        [decreaseQtyBtn.titleLabel setFont:[UIFont systemFontOfSize:22.0f]];
        [decreaseQtyBtn addTarget:self action:@selector(decreaseQty:) forControlEvents:UIControlEventTouchUpInside];
        [self.bgVw addSubview:decreaseQtyBtn];

        if ([[defaults objectForKey:kitemType] isEqualToString:@"Bar"])
        {
            UIButton * smallQtyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            [smallQtyBtn setFrame:CGRectMake(600, 120, 90, 40)];
            if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
            {
                [smallQtyBtn setBackgroundColor:kDarkBrownColor];
            }
            else
            {
                [smallQtyBtn setBackgroundColor:kOrangeColor];
            }
            
            [smallQtyBtn setTitle:@"Small" forState:UIControlStateNormal];
            [smallQtyBtn.titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
            [smallQtyBtn addTarget:self action:@selector(smallQty) forControlEvents:UIControlEventTouchUpInside];
            [self.bgVw addSubview:smallQtyBtn];
            
            
            
            UIButton * largeQtyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            [largeQtyBtn setFrame:CGRectMake(692, 120, 90, 40)];
            if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
            {
                [largeQtyBtn setBackgroundColor:kDarkBrownColor];
            }
            else
            {
                [largeQtyBtn setBackgroundColor:kOrangeColor];
            }
            
            [largeQtyBtn setTitle:@"Large" forState:UIControlStateNormal];
            [largeQtyBtn.titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
            [largeQtyBtn addTarget:self action:@selector(largeQty) forControlEvents:UIControlEventTouchUpInside];
            [self.bgVw addSubview:largeQtyBtn];
        }
      
        

        
        
        
        
        
        
        
        UILabel *modifierLbl=[[UILabel alloc] init];
        [modifierLbl setFrame:CGRectMake(320, 170, 100, 50)];
        [modifierLbl setBackgroundColor:[UIColor clearColor]];
        [modifierLbl setTextAlignment:NSTextAlignmentLeft];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [modifierLbl setTextColor:kOrangeColor];
        }
        else
        {
            [modifierLbl setTextColor:kOrangeColor];
        }
        
        [modifierLbl setText:@"Modifier:"];
        [modifierLbl setFont:[UIFont systemFontOfSize:18]];
        [self.bgVw addSubview:modifierLbl];
        [modifierLbl release];
        
        UIButton *modifier1Btn=[UIButton buttonWithType:UIButtonTypeCustom];
        [modifier1Btn setFrame:CGRectMake(438, 175, 40, 40)];
        [modifier1Btn setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
        [modifier1Btn setBackgroundImage:[UIImage imageNamed:@"check_box_active"] forState:UIControlStateSelected];
        [modifier1Btn addTarget:self action:@selector(setModifier1:) forControlEvents:UIControlEventTouchUpInside];
        [modifier1Btn setTag:10];
        [self.bgVw addSubview:modifier1Btn];
        
        UILabel *modifier1=[[UILabel alloc] init];
        [modifier1 setFrame:CGRectMake(480, 175, 100, 40)];
        [modifier1 setBackgroundColor:[UIColor clearColor]];
        [modifier1 setTextAlignment:NSTextAlignmentCenter];
        [modifier1 setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
        [modifier1 setText:@"Modifier1"];
        [modifier1 setFont:[UIFont systemFontOfSize:15]];
        [self.bgVw addSubview:modifier1];
        [modifier1 release];
        
        UIButton *modifier2Btn=[UIButton buttonWithType:UIButtonTypeCustom];
        [modifier2Btn setFrame:CGRectMake(600, 175, 40, 40)];
        [modifier2Btn setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
        [modifier2Btn setBackgroundImage:[UIImage imageNamed:@"check_box_active"] forState:UIControlStateSelected];
        [modifier2Btn addTarget:self action:@selector(setModifier1:) forControlEvents:UIControlEventTouchUpInside];
        [self.bgVw addSubview:modifier2Btn];
        [modifier2Btn setTag:20];
        
        UILabel *modifier2=[[UILabel alloc] init];
        [modifier2 setFrame:CGRectMake(640, 175, 100, 40)];
        [modifier2 setBackgroundColor:[UIColor clearColor]];
        [modifier2 setTextAlignment:NSTextAlignmentCenter];
        [modifier2 setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
        [modifier2 setText:@"Modifier2"];
        [modifier2 setFont:[UIFont systemFontOfSize:15]];
        [self.bgVw addSubview:modifier2];
        [modifier2 release];
        
        
        
        
        UIButton *modifier3Btn=[UIButton buttonWithType:UIButtonTypeCustom];
        [modifier3Btn setFrame:CGRectMake(438, 215, 40, 40)];
        [modifier3Btn setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
        [modifier3Btn setBackgroundImage:[UIImage imageNamed:@"check_box_active"] forState:UIControlStateSelected];
        [modifier3Btn addTarget:self action:@selector(setModifier1:) forControlEvents:UIControlEventTouchUpInside];
        [self.bgVw addSubview:modifier3Btn];
        [modifier3Btn setTag:30];
        
        UILabel *modifier3=[[UILabel alloc] init];
        [modifier3 setFrame:CGRectMake(480, 215, 100, 40)];
        [modifier3 setBackgroundColor:[UIColor clearColor]];
        [modifier3 setTextAlignment:NSTextAlignmentCenter];
        [modifier3 setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
        [modifier3 setText:@"Modifier3"];
        [modifier3 setFont:[UIFont systemFontOfSize:15]];
        [self.bgVw addSubview:modifier3];
        [modifier3 release];
        
        UIButton *modifier4Btn=[UIButton buttonWithType:UIButtonTypeCustom];
        [modifier4Btn setFrame:CGRectMake(600, 215, 40, 40)];
        [modifier4Btn setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
        [modifier4Btn setBackgroundImage:[UIImage imageNamed:@"check_box_active"] forState:UIControlStateSelected];
        [modifier4Btn addTarget:self action:@selector(setModifier1:) forControlEvents:UIControlEventTouchUpInside];
        [self.bgVw addSubview:modifier4Btn];
        [modifier4Btn setTag:40];
        
        UILabel *modifier4=[[UILabel alloc] init];
        [modifier4 setFrame:CGRectMake(640, 215, 100, 40)];
        [modifier4 setBackgroundColor:[UIColor clearColor]];
        [modifier4 setTextAlignment:NSTextAlignmentCenter];
        [modifier4 setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
        [modifier4 setText:@"Modifier4"];
        [modifier4 setFont:[UIFont systemFontOfSize:15]];
        [self.bgVw addSubview:modifier4];
        [modifier4 release];
        
        
        UILabel *complimentaryLbl=[[UILabel alloc] init];
        [complimentaryLbl setFrame:CGRectMake(320, 255, 90, 50)];
        [complimentaryLbl setBackgroundColor:[UIColor clearColor]];
        [complimentaryLbl setNumberOfLines:2];
        [complimentaryLbl setTextAlignment:NSTextAlignmentLeft];
        
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
             [complimentaryLbl setTextColor:kDarkBrownColor];
        }
        else
        {
            [complimentaryLbl setTextColor:kOrangeColor];
        }
        
       
        [complimentaryLbl setText:@"Complimentary:"];
        [complimentaryLbl setFont:[UIFont systemFontOfSize:16]];
        [self.bgVw addSubview:complimentaryLbl];
        [complimentaryLbl release];
        
        UIButton *complimentaryBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [complimentaryBtn setFrame:CGRectMake(438, 260, 40, 40)];
        [complimentaryBtn setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
        [complimentaryBtn setBackgroundImage:[UIImage imageNamed:@"check_box_active"] forState:UIControlStateSelected];
        [complimentaryBtn addTarget:self action:@selector(setModifier2:) forControlEvents:UIControlEventTouchUpInside];
        [self.bgVw addSubview:complimentaryBtn];
        
        self.isComplimentary=@"No";
        
        slideVw=[[UIView alloc] init];
        [slideVw setFrame:CGRectMake(self.bgVw.frame.size.width, 260, 300, 40)];
        [slideVw setBackgroundColor:[UIColor clearColor]];
        //[self.bgVw addSubview:slideVw];

        UILabel *managerLbl=[[UILabel alloc] init];
        [managerLbl setFrame:CGRectMake(0, 0, 160, 40)];
        [managerLbl setBackgroundColor:[UIColor clearColor]];
        [managerLbl setTextAlignment:NSTextAlignmentLeft];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [managerLbl setTextColor:kDarkBrownColor];
        }
        else
        {
           [managerLbl setTextColor:kOrangeColor];
        }
        
        [managerLbl setText:@"Enter Manager's ID:"];
        [managerLbl setFont:[UIFont systemFontOfSize:16]];
        [slideVw addSubview:managerLbl];
        [managerLbl release];

        if ([self.isComplimentary isEqualToString:@"Yes"]) {
            self.managerID=managerLbl.text;
        }
        else
        {
             self.managerID=@"";
        }
        
        UITextField *managerIdTxtFld=[[UITextField alloc] init];
        [managerIdTxtFld setDelegate:self];
        [managerIdTxtFld setFrame:CGRectMake(160, 0, 130, 40)];
        [managerIdTxtFld.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [managerIdTxtFld.layer setBorderWidth:1.0f];
        [managerIdTxtFld setFont:[UIFont systemFontOfSize:18]];
        [managerIdTxtFld setTag:20];
        [slideVw addSubview:managerIdTxtFld];
        [managerIdTxtFld release];
        
        UILabel *instructionLbl=[[UILabel alloc] init];
        [instructionLbl setFrame:CGRectMake(320, 310, 250, 50)];
        [instructionLbl setBackgroundColor:[UIColor clearColor]];
        [instructionLbl setTextAlignment:NSTextAlignmentLeft];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [instructionLbl setTextColor:kDarkBrownColor];
        }
        else
        {
            [instructionLbl setTextColor:kOrangeColor];
        }
        
        [instructionLbl setText:@"Instructions:"];
        [complimentaryLbl setFont:[UIFont systemFontOfSize:18]];
        [self.bgVw addSubview:instructionLbl];
        [instructionLbl release];
        
        
        UITextView *instructionTxt=[[UITextView alloc] init];
        self.instructionTxt=instructionTxt;
        [instructionTxt release];
        [self.instructionTxt setFrame:CGRectMake(440, 310, 345, 60)];
        [self.bgVw addSubview:self.instructionTxt];
        [self.instructionTxt.layer setBorderWidth:1.0f];
        [self.instructionTxt.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [self.instructionTxt setFont:[UIFont systemFontOfSize:18]];
        [self.instructionTxt setDelegate:self];
        [self.instructionTxt setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
        
        
        self.AddItemBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.AddItemBtn setFrame:CGRectMake(705, 390, 80, 40)];
        [self.AddItemBtn setTitle:@"Add" forState:UIControlStateNormal];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [self.AddItemBtn setBackgroundColor:kDarkBrownColor];
        }
        else
        {
           [self.AddItemBtn setBackgroundColor:kOrangeColor];
        }
        
        [self.AddItemBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.bgVw addSubview:self.AddItemBtn];
        [self.AddItemBtn.titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
        
        
        self.isModifier1=@"No";
        self.isModifier2=@"No";
        self.isModifier3=@"No";
        self.isModifier4=@"No";
        
    }
    return self;
}

-(void) largeQty
{
    
}
-(void) smallQty
{
    
}
-(void) setModifier1:(id) sender
{

    if ([sender isSelected]) {
        [sender setSelected:NO];
        if ([sender tag]==10) {
            self.isModifier1=@"No";
        }
        else  if ([sender tag]==20) {
            self.isModifier2=@"No";
        }
        else  if ([sender tag]==30) {
            self.isModifier3=@"No";
        }
        else  if ([sender tag]==40) {
            self.isModifier4=@"No";
        }
    }
    else
    {
        [sender setSelected:YES];
        if ([sender tag]==10) {
            self.isModifier1=@"Yes";
        }
        else  if ([sender tag]==20) {
            self.isModifier2=@"Yes";
        }
        else  if ([sender tag]==30) {
            self.isModifier3=@"Yes";
        }
        else  if ([sender tag]==40) {
            self.isModifier4=@"Yes";
        }

    }
    
}
-(void) setModifier2:(id) sender
{
    
    if ([sender isSelected]) {
        [sender setSelected:NO];
        self.isComplimentary=@"No";
        popUp=1;
        [UIView animateWithDuration:0.75f animations:^{
            
            
            CGRect uiasViewFrame   = slideVw.frame;
            uiasViewFrame.origin.x = self.bgVw.frame.size.width ;
            
            slideVw.frame = uiasViewFrame;
            
            
            
        } completion:^(BOOL finished)
         {
             // completion block is important in each case. Use it
             [slideVw removeFromSuperview];
         }];
    }
    else
    {
        [sender setSelected:YES];
          self.isComplimentary=@"Yes";
        popUp=0;
        // show new order screen
        [UIView animateWithDuration:0.60f animations:^{
            
            
            CGRect uiasViewFrame   = slideVw.frame;
            uiasViewFrame.origin.x =495;
            slideVw.frame = uiasViewFrame;
            [self.bgVw addSubview:slideVw];
//            [slideVw release];
            
            
        } completion:^(BOOL finished) {
            
            
        }];
    }
    
}

-(void) increaseQty:(id) sender
{
    int qty=[ self.qtyNumLbl.text intValue];
    qty++;
    self.qtyNumLbl.text=[NSString stringWithFormat:@"%d",qty];
}
-(void) decreaseQty:(id) sender
{
    int qty=[ self.qtyNumLbl.text intValue];
    if (qty>1) {
         qty--;
    }
   else
   {
       qty=1;
   }
     self.qtyNumLbl.text=[NSString stringWithFormat:@"%d",qty];
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
     [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-800)/2, ([UIScreen mainScreen].bounds.size.height-450)/2-100,800 , 450)];
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
     [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-800)/2, ([UIScreen mainScreen].bounds.size.height-450)/2,800 , 450)];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag==10) {
          [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.height-800)/2, ([UIScreen mainScreen].bounds.size.width-450)/2-190,800 , 450)];
    } else {
          [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.height-800)/2, ([UIScreen mainScreen].bounds.size.width-450)/2-100,800 , 450)];
    }
   
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
     [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.height-800)/2, ([UIScreen mainScreen].bounds.size.width-450)/2,800 , 450)];
      self.managerID=textField.text;
}
@end
