//
//  OrderViewController.m
//  iROS
//
//  Created by iMac Apple on 04/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "OrderViewController.h"

@interface OrderViewController ()

@end

@implementation OrderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
     
    }
    return self;
  
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!arrSectionHeader)
    {
        arrSectionHeader=[[NSMutableArray alloc]initWithObjects:@"SOFT DRINKS", @"SALAD", @"DESSERTS", @"BREADS", @"SOUPS", nil];
    }
    
    dicSectionContent=[[NSMutableDictionary alloc]init];
    arrSoftDrinks     = [NSArray arrayWithObjects:@"Pepsi", @"Coke", @"Fanta", @"Mirinda", nil];
    arrSalad     = [NSArray arrayWithObjects:@"Mushroom salad", @"Corn & Spinach salad", @"Vegetable Mix", nil];
    arrDesserts     = [NSArray arrayWithObjects:@"Ice Cream", @"Baked Apple Dumplin", @"Fruit Cobbler with Ice Cream", @"Frozen Mug Sundaes", @"Chocolate-Mint Bars", nil];
    arrBreads     = [NSArray arrayWithObjects:@"Wheat Roti", @"Tandoori Naan", @"Banana bread", @"Bhakri", @"Bing", @"Cornbread", nil];
    arrSoups     = [NSArray arrayWithObjects:@"Manchaow", @"Sweet Corn",@"Cream Tomato",@"Peanut soup",@"Ramen", nil];
    
    

    dicContent=[[NSMutableDictionary alloc]init];
    
    [dicContent setValue:arrSoftDrinks forKey:[arrSectionHeader objectAtIndex:0]];
    
    [dicContent setValue:arrSalad forKey:[arrSectionHeader objectAtIndex:1]];
    
    [dicContent setValue:arrDesserts forKey:[arrSectionHeader objectAtIndex:2]];
    
    [dicContent setValue:arrBreads forKey:[arrSectionHeader objectAtIndex:3]];
    
    [dicContent setValue:arrSoups forKey:[arrSectionHeader objectAtIndex:4]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [arrSectionHeader count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    switch (section) {
  case 0:
            return [arrSoftDrinks count];
            break;
            
    case 1:
            return [arrSalad count];
            break;
    case 2:
            return [arrDesserts count];
            break;
  default:
    break;
}
    return 1;
}

//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//}
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    
//}



@end
