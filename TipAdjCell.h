//
//  TipAdjCell.h
//  iROS
//
//  Created by iMac Apple on 16/02/15.
//  Copyright (c) 2015 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TipAdjCell : UITableViewCell
{
}

@property (nonatomic, retain) IBOutlet UILabel *orderId;
@property (nonatomic, retain) IBOutlet UILabel *orderAmt;
@property (nonatomic, retain) IBOutlet UIButton *tipAdjBtn;
@property (nonatomic, retain) IBOutlet UILabel *trnsIdlbl;
@property (nonatomic, retain) IBOutlet UITextField *enterTipField;






@end
