//
//  TripAdjustmentVC.m
//  iROS
//
//  Created by iMac Apple on 16/02/15.
//  Copyright (c) 2015 Dex Consulting. All rights reserved.
//

#import "TripAdjustmentVC.h"
#import "TipAdjCell.h"

@interface TripAdjustmentVC ()

@end

@implementation TripAdjustmentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UITableView *tipAdjTble = (UITableView*)[self.view viewWithTag:10];
    [tipAdjTble setDataSource:self];
    [tipAdjTble setDelegate:self];
[tipAdjTble registerNib:[UINib nibWithNibName:@"TipAdjCell"bundle:nil]forCellReuseIdentifier:@"tipAdjcellidentifier"];
  
    [self fechOrders:self];

}

#pragma mark - Table View Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [orderList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"tipAdjcellidentifier";
    TipAdjCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[TipAdjCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
 
    NSMutableDictionary *dic  = [orderList objectAtIndex:indexPath.row];
    
      [cell.tipAdjBtn addTarget:self action:@selector(TipAdjustmentClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.orderId setText:[NSString stringWithFormat:@"%@", [dic objectForKey:@"order_id"]]];
    [cell.trnsIdlbl setText:[NSString stringWithFormat:@"%@", [dic objectForKey:@"transactionid"]]];
    [cell.orderAmt setText:[NSString stringWithFormat:@"%@", [dic objectForKey:@"amount"]]];


    return cell;
}

-(IBAction)GoBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)fechOrders:(id)sender
{
    
        ServerConnectionClass* network = [[ServerConnectionClass alloc]init] ;
        self.connObject = network;
        [network release];
        self.connObject.delegate = self;
        NSString* urlString=[[NSString alloc]initWithFormat:@"%@{\"user_id\":\"%d\"}",kTipAdjList, [iRestaurantsGlobalDataClass getInstance].loggedUserId];
        [self.connObject makeRequestForUrl:urlString];
      [urlString release];
    
}

-(IBAction)TipAdjustmentClicked:(UIButton*)btn
{
    UITableView * tbl = (UITableView*)[self.view viewWithTag:10];
//    
    TipAdjCell *cell = (TipAdjCell *)[btn superview];
    UITextField * txf = (UITextField*)[cell  viewWithTag:12];
    NSString * str = [NSString stringWithFormat:@"%@",txf.text];
    
    CGPoint center= btn.center;
    CGPoint rootViewPoint = [btn.superview convertPoint:center toView:tbl];
    NSIndexPath *indexPath = [tbl indexPathForRowAtPoint:rootViewPoint];

    
    ServerConnectionClass* network = [[ServerConnectionClass alloc]init] ;
    self.connObject = network;
    [network release];
    self.connObject.delegate = self;
    NSString* urlString=[[NSString alloc]initWithFormat:@"%@{\"id\":\"%@\", \"tip\":\"%@\"}",kProcessTI10URL, [[orderList objectAtIndex:indexPath.row]objectForKey:@"id"], str];
    [self.connObject makeRequestForUrl:urlString];
    [urlString release];
}




- (void) connectionEndedSuccefullyWithData:(NSData*)data
{
    NSError *localError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    NSLog(@"MY dictionary====%@",dictionary);
    if (dictionary != nil)
    {
        NSString* cmd = [dictionary objectForKey:@"cmd"];
        NSString* result = [dictionary objectForKey:@"result"];

        if ([cmd isEqualToString:@"orderall"])
        {
            if ([result isEqual:@"Success"])
            {
                orderList = [[NSArray alloc]initWithArray:[dictionary objectForKey:@"order_detail"]];
                [(UITableView*)[self.view viewWithTag:10] reloadData];

            }
            else
            {
               
            }
        }
        else  if ([cmd isEqualToString:@"payment"])
        {
             [Utils showAlertView:@"Alert" message:[dictionary objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [self.navigationController popViewControllerAnimated:YES];
        
        }
     }
    else
    {
        [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    [self removeConnectionClassObject];
}

- (void) removeConnectionClassObject
{
    self.connObject.delegate = nil;
    self.connObject = nil;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
