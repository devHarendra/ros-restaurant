//
//  LAYOUTRESTOBARVC.m
//  iROS
//
//  Created by iMac Apple on 05/02/15.
//  Copyright (c) 2015 Dex Consulting. All rights reserved.
//

#import "LAYOUTRESTOBARVC.h"
#import "MenuViewController.h"


@interface LAYOUTRESTOBARVC ()

@end

@implementation LAYOUTRESTOBARVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UICollectionView * collectionRestro = (UICollectionView*)[self.view viewWithTag:20];
    [collectionRestro registerNib:[UINib nibWithNibName:@"LayoutRestoCell" bundle:nil] forCellWithReuseIdentifier:@"RestoCell"];
    
      UICollectionView * collectionbar = (UICollectionView*)[self.view viewWithTag:21];
     [collectionbar registerNib:[UINib nibWithNibName:@"LayoutRestoCell" bundle:nil] forCellWithReuseIdentifier:@"RestoCell"];
    
    
    UIView *nOfpepleInParty = (UIView*)[self.view viewWithTag:30];
    [nOfpepleInParty setFrame:CGRectMake(0, self.view.frame.size.height, nOfpepleInParty.frame.size.width, nOfpepleInParty.frame.size.height)];
    [nOfpepleInParty setBackgroundColor:[UIColor clearColor]];
 
}

-(IBAction)BackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

 int tag = 0 ;
-(IBAction)ShowPopupNoPeopleInParty:(UIButton*)btn
{
    UIView *nOfpepleInParty = (UIView*)[self.view viewWithTag:30];
    if (btn.tag !=31) tag = btn.tag+1; // this store for for table id because 31 is assingned to submit btn , 31 never be  table id .
    if (nOfpepleInParty.frame.origin.y==self.view.frame.size.height)
    {
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             
                             [nOfpepleInParty setFrame:CGRectMake(0, 63, nOfpepleInParty.frame.size.width, nOfpepleInParty.frame.size.height)];
                             
                         }
                         completion:^(BOOL finished){
                             [nOfpepleInParty setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f  green:0.0f/255.0f blue:0.0f/255.0f alpha:0.5]];
                         }];
            }
    
    else
    {
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             
                             [nOfpepleInParty setFrame:CGRectMake(0, self.view.frame.size.height, nOfpepleInParty.frame.size.width, self.view.frame.size.height)];
                             
                         }
                         completion:^(BOOL finished){
                             [self.view endEditing:YES];
                             [nOfpepleInParty setBackgroundColor:[UIColor clearColor]];
                             
                             if (btn.tag ==301) return ; // tag 301 for cross/cancel button.
                            MenuViewController *menuVC=[[MenuViewController alloc] init];
                             [menuVC setTableID:[NSString stringWithFormat:@"%d",tag]];
                             [menuVC setFromClass:@"LayoutClass"];
                             [self.navigationController pushViewController:menuVC animated:YES];
                             [menuVC release];
                         }];
            }
 }

-(IBAction)ShowPopupNoPeopleInPartyBar:(UIButton*)btn
{
    UIView *nOfpepleInParty = (UIView*)[self.view viewWithTag:30];
    if (btn.tag !=31) tag = btn.tag+1; // this store for for table id because 31 is assingned to submit btn , 31 never be  table id .
    if (nOfpepleInParty.frame.origin.y==self.view.frame.size.height)
    {

        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             
                             [nOfpepleInParty setFrame:CGRectMake(510, 63, nOfpepleInParty.frame.size.width, nOfpepleInParty.frame.size.height)];
                             
                         }
                         completion:^(BOOL finished){
                             [nOfpepleInParty setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f  green:0.0f/255.0f blue:0.0f/255.0f alpha:0.5]];
                         }];
    }
    
    else
    {
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             
                             [nOfpepleInParty setFrame:CGRectMake(510, self.view.frame.size.height, nOfpepleInParty.frame.size.width, self.view.frame.size.height)];
                             
                         }
                         completion:^(BOOL finished){
                             [self.view endEditing:YES];
                             [nOfpepleInParty setBackgroundColor:[UIColor clearColor]];
                             
                             if (btn.tag ==301) return ; // tag 301 for cross/cancel button.
                             MenuViewController *menuVC=[[MenuViewController alloc] init];
                             [menuVC setTableID:[NSString stringWithFormat:@"%d",tag]];
                             [menuVC setFromClass:@"LayoutClass"];
                             [self.navigationController pushViewController:menuVC animated:YES];
                             [menuVC release];
                         }];
    }
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
    {
        iRestaurantsGlobalDataClass *obj=[iRestaurantsGlobalDataClass getInstance];
 
    if (collectionView.tag == 20)
    {
       // for ,  if RbookedSeatCount reaches up to 30 then add 10 more seats .
        if ([obj.tableBookedArr_food count]<=30)
        {
            return 30;
        }
        else
        {
          return [obj.tableBookedArr_food count] + 10;
        }
        
      }
    else  if (collectionView.tag == 21)
    {
        // for ,  if RbookedSeatCount reaches up to 30 then add 10 more seats .
        if ([obj.tableBookedArr_bar count]<=30)
        {
            return 30;
        }
        else
        {
            return [obj.tableBookedArr_bar count] + 10;
        }
    }
    
    else return 0;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (LayoutRestoCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    LayoutRestoCell *cell;
    iRestaurantsGlobalDataClass *obj=[iRestaurantsGlobalDataClass getInstance];
     if (collectionView.tag == 20)
     {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RestoCell" forIndexPath:indexPath];
         [cell.seatselBtn setTag:indexPath.item];
         [cell.seatselBtn setImage:[UIImage imageNamed:@"table.png"] forState:UIControlStateNormal];
         [cell.seatselBtn setImage:[UIImage imageNamed:@"table_active.png"] forState:UIControlStateSelected];
         [cell.seatselBtn addTarget:self action:@selector(ShowPopupNoPeopleInParty:) forControlEvents:UIControlEventTouchUpInside];

           [cell.tbleIdlbl setText:[NSString stringWithFormat:@"D%ld",(long)indexPath.item+1]];
         [cell.tbleIdlbl setTextColor:kOrangeColor];

         NSLog(@"%d", indexPath.item);
        if ([obj.tableBookedArr_food containsObject:[NSString stringWithFormat:@"%d",cell.seatselBtn.tag+1]])
         {
             [cell.seatselBtn setSelected:YES];
             [cell.seatselBtn setUserInteractionEnabled:NO];
         }
        else
        {
            [cell.seatselBtn setSelected:NO];
            [cell.seatselBtn setUserInteractionEnabled:YES];
        }
     }
     else
     {
         cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RestoCell" forIndexPath:indexPath];
         [cell.seatselBtn setImage:[UIImage imageNamed:@"table_bar.png"] forState:UIControlStateNormal];
         [cell.seatselBtn setImage:[UIImage imageNamed:@"table_active_bar.png"] forState:UIControlStateSelected];
         [cell.seatselBtn addTarget:self action:@selector(ShowPopupNoPeopleInPartyBar:) forControlEvents:UIControlEventTouchUpInside];

         [cell.seatselBtn setTag:indexPath.item];
         [cell.tbleIdlbl setFrame:CGRectMake(cell.tbleIdlbl.frame.origin.x, 100, cell.tbleIdlbl.frame.size.width, cell.tbleIdlbl.frame.size.height)];
         [cell.tbleIdlbl setText:[NSString stringWithFormat:@"B%ld",(long)indexPath.item+1]];
         [cell.tbleIdlbl setTextColor:kDarkBrownColor];
         NSLog(@"%d", indexPath.item);
         
        if ([obj.tableBookedArr_bar containsObject:[NSString stringWithFormat:@"%d",cell.seatselBtn.tag+1]])
         {
             [cell.seatselBtn setSelected:YES];
             [cell.seatselBtn setUserInteractionEnabled:NO];
         }
        else
        {
            [cell.seatselBtn setSelected:NO];
            [cell.seatselBtn setUserInteractionEnabled:YES];
        }
     }
//    cell.cellLabel.text = [NSString stringWithFormat:@"cell %i",indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    LayoutRestoCell *cell = (LayoutRestoCell*)[collectionView cellForItemAtIndexPath:indexPath];
//    NSArray *views = [cell.contentView subviews];
//    UILabel *label = [views objectAtIndex:0];
//    NSLog(@"Select %@",label.text);
    
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)GotoMenuClickedfrombar:(UIButton*)btn
{
    
    MenuViewController *menuVC=[[MenuViewController alloc] init];
    [menuVC setTableID:[NSString stringWithFormat:@"%d",btn.tag+1]];
    [menuVC setFromClass:@"LayoutClass"];
    [self.navigationController pushViewController:menuVC animated:YES];
    [menuVC release];
   

}
@end
