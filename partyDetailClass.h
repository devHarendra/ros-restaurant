//
//  partyDetailClass.h
//  iROS
//
//  Created by iMac Apple on 02/02/15.
//  Copyright (c) 2015 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface partyDetailClass : UIView<UITextFieldDelegate>

@property (nonatomic, retain) UIButton* closeBtn;
@property (nonatomic, retain) UIButton* DoneBtn;

@property (nonatomic, retain) UIView   *bgVw;
@property (nonatomic, retain) UITextField * noPeopleTF;
@property (nonatomic, retain) UITextField * partyNameTF;
@end
