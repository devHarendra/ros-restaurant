//
//  OrderViewController.h
//  iROS
//
//  Created by iMac Apple on 04/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

{
    IBOutlet UITableView *tblOrderList;
    NSMutableArray *arrSectionHeader;
    NSMutableDictionary *dicSectionContent;
    
    NSArray *arrSoftDrinks;
    NSArray *arrSalad;
    NSArray *arrDesserts;
    NSArray *arrBreads;
    NSArray *arrSoups;
    
    NSMutableDictionary *dicContent;
    
}

@end
