//
//  SwipeViewController.m
//  MagTek.iDynamo.ObjC
//
//  Created by agharris73 on 9/18/13.
//  Copyright (c) 2013 Mercury. All rights reserved.
//

#import "SwipeViewController.h"
#import "MTSCRA.h"
#import "iROSAppDelegate.h"
#import "HomeViewController.h"
#import "TripAdjustmentVC.h"

@interface SwipeViewController ()
#define SI [iRestaurantsGlobalDataClass getInstance]


@property (nonatomic, weak) IBOutlet UIImageView *ccSwipe;
    @property (strong, nonatomic) MTSCRA *magTek;
@end

@implementation SwipeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.magTek = [[MTSCRA alloc] init];
    [self.magTek listenForEvents:(TRANS_EVENT_OK|TRANS_EVENT_START|TRANS_EVENT_ERROR)];
    [self.magTek setDeviceProtocolString:(@"com.magtek.idynamo")];
    
    self.simSwipeuDynamoSwitch.on = false;
    self.simSwipeiDynamoSwitch.on = false;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)simSwipeuDynamoButtonPressed:(id)sender {
    [self.simSwipeuDynamoSwitch setOn:!self.simSwipeuDynamoSwitch.on animated:true];
    [self.simSwipeiDynamoSwitch setOn:false animated:true];

    [self magTek_deactivate];
    
    if (self.simSwipeuDynamoSwitch.on) {
        [self magtek_activateWithDeviceType:MAGTEKAUDIOREADER];
    }
}

- (IBAction)simSwipeduDynamoSwitchFlipped:(id)sender {
    [self.simSwipeiDynamoSwitch setOn:false animated:true];
    
    [self magTek_deactivate];
    
    if (self.simSwipeuDynamoSwitch.on) {
        [self magtek_activateWithDeviceType:MAGTEKAUDIOREADER];
    }
}

- (IBAction)simSwipeiDynamoButtonPressed:(id)sender {
    [self.simSwipeuDynamoSwitch setOn:false animated:true];
    [self.simSwipeiDynamoSwitch setOn:!self.simSwipeiDynamoSwitch.on animated:true];
    
    [self magTek_deactivate];
    
    if (self.simSwipeiDynamoSwitch.on) {
        [self magtek_activateWithDeviceType:MAGTEKIDYNAMO];
    }
}

- (IBAction)simSwipeiDynamoSwitchFlipped:(id)sender {
    [self.simSwipeuDynamoSwitch setOn:false animated:true];
    
    [self magTek_deactivate];
    
    if (self.simSwipeiDynamoSwitch.on) {
        [self magtek_activateWithDeviceType:MAGTEKIDYNAMO];
    }
}

//-----------------------------------------------------------------------------
#pragma mark - MagTek SDK activation/deactivation -
//-----------------------------------------------------------------------------

- (void)magtek_activateWithDeviceType:(UInt32)deviceType
{
    [self.magTek setDeviceType:deviceType];
    [self.magTek openDevice];
    [self magtek_registerObservers:true];
    [self displayDeviceStatus];
}

-(void)magTek_deactivate {
    [self magtek_registerObservers:false];
    [self disconnected];
    
    if (self.magTek != NULL && self.magTek.isDeviceOpened)
    {
        [self.magTek closeDevice];
    }
    
}

//-----------------------------------------------------------------------------
#pragma mark - MagTek SDK observers -
//-----------------------------------------------------------------------------

- (void)magtek_registerObservers:(BOOL) reg {
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    if (reg) {
        [nc addObserver:self selector:@selector(trackDataReady:) name:@"trackDataReadyNotification" object:nil];
        [nc addObserver:self selector:@selector(devConnStatusChange) name:@"devConnectionNotification" object:nil];
    }
    else {
        [nc removeObserver:self];
    }
}


- (void)trackDataReady:(NSNotification *)notification
{
    NSNumber *status = [[notification userInfo] valueForKey:@"status"];
    
    [self performSelectorOnMainThread:@selector(onDataEvent:)
                           withObject:status
                        waitUntilDone:NO];
}

- (void)onDataEvent:(id)status
{
    switch ([status intValue]) {
        case TRANS_STATUS_OK:
            NSLog(@"TRANS_STATUS_OK");
            self.encryptedSwipeData = [[EncryptedSwipeData alloc] init];
            self.encryptedSwipeData.track1Masked = self.magTek.getTrack1Masked;
            self.encryptedSwipeData.track2Masked = self.magTek.getTrack2Masked;
            self.encryptedSwipeData.track1Encrypted = self.magTek.getTrack1;
            self.encryptedSwipeData.track2Encrypted = self.magTek.getTrack2;
            self.encryptedSwipeData.CardLast4 = self.magTek.getCardLast4;
            self.encryptedSwipeData.CardName = self.magTek.getCardName;
            self.encryptedSwipeData.getMagnePrintData = self.magTek.getMagnePrint;
            self.encryptedSwipeData.getMagnePrintDataStatus = self.magTek.getMagnePrintStatus;
 

            self.encryptedSwipeData.ksn = self.magTek.getKSN;
            
          
            
//            NSLog(@"need it  :%@ \n %@ \n %@ \n%@ \n%@ \n",self.magTek.getTrack1Masked,
//                  self.magTek.getTrack2Masked,
//                  self.magTek.getTrack1,
//                  self.magTek.getTrack2,
//                  self.magTek.getKSN);
            
            NSLog(@"getTrack1 :\n%@",
                   self.magTek.getTrack1);
            
            NSLog(@"getTrack2 :\n%@",
                  self.magTek.getTrack2);
            
            NSLog(@"getTrack3 Is :\n%@",
                  self.magTek.getTrack3);
            
            NSLog(@"msked1 Is :\n%@",
                  self.magTek.getTrack1Masked);
            
            NSLog(@"msked2 Is :\n%@",
                  self.magTek.getTrack2Masked);


            NSLog(@"getCardIIN Is :\n%@ \nLast4digit%@",
                  self.magTek.getCardIIN,self.magTek.getCardLast4 );
            
            NSLog(@"ksn Is :\n%@",
                  self.magTek.getKSN);

            
            NSLog(@"encryption status Is :\n%@",
                  self.magTek.getCapMagStripeEncryption);
            
            NSLog(@"battery status Is :\n%ld",
                  self.magTek.getBatteryLevel);

            
            [self determineNextStep];
            break;
        case TRANS_STATUS_ERROR:
            NSLog(@"TRANS_STATUS_ERROR");
            break;
        default:
            break;
    }
}

- (void)devConnStatusChange
{
    [self displayDeviceStatus];
}

#pragma mark connection task

//-----------------------------------------------------------------------------
#pragma mark - Methods -
//-----------------------------------------------------------------------------

- (IBAction)disconnected {
    
    self.imageCardState.image = [UIImage imageNamed:@"ccStaticState.png"];
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade; // there are other types but this is the nicest
    transition.duration = 0.34; // set the duration that you like
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [self.imageCardState.layer addAnimation:transition forKey:nil];
}

- (IBAction)connected {
    self.imageCardState.image = [UIImage imageNamed:@"ccSwipeState.png"];
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionFade; // there are other types but this is the nicest
    transition.duration = 0.34; // set the duration that you like
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [self.imageCardState.layer addAnimation:transition forKey:nil];
}

- (void)displayDeviceStatus
{
    BOOL isMagTekDeviceConnected = [self.magTek isDeviceConnected];
    BOOL isMagTekDeviceOpen = [self.magTek isDeviceOpened];
    
    if ((self.magTek && isMagTekDeviceConnected && isMagTekDeviceOpen)){
        [self connected];
    }
    else {
        [self disconnected];
    }
}

- (int) encryptedLengthFromMasked: (int)maskedLength {
    int value = maskedLength;
    
    if (value > 0) {
        value = abs(8-(maskedLength % 8)) + maskedLength;
    }
    
    return value;
}

- (NSString*) hexFromData:(NSData*) data
{
    const unsigned char *dataBuffer = (const unsigned char *)[data bytes];
    
    if (!dataBuffer) {
        return [NSString string];
    }
    
    NSUInteger dataLength  = data.length;
    NSMutableString *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i) {
        [hexString appendString:[NSString stringWithFormat:@"%02x", (unsigned int)dataBuffer[i]]];
    }
    
    return [NSString stringWithString:hexString];
}

- (void)determineNextStep {
    
    [self magTek_deactivate];
    
    if (self.encryptedSwipeData != nil
        && self.encryptedSwipeData.track2Masked != nil
        && [self.encryptedSwipeData.track2Masked rangeOfString:@"="].location != NSNotFound)
    {
        iROSAppDelegate *appDelegate = (iROSAppDelegate*)[[UIApplication sharedApplication] delegate];
        appDelegate.encryptedSwipeData = self.encryptedSwipeData;
//        [self performSegueWithIdentifier:@"segueSwipeResults" sender:self]; // remove this go original view and enable autolayout in story board
//        [self performSegueWithIdentifier:@"paymentOpt" sender:self]; // done by harry
        [self ProcessCS10:self];

    }
    else if (self.simSwipeiDynamoSwitch.on) {
        [self magtek_activateWithDeviceType: MAGTEKIDYNAMO];
    }
    else if(self.simSwipeuDynamoSwitch.on) {
        [self magtek_activateWithDeviceType: MAGTEKAUDIOREADER];
    }
}


#pragma Custom methods starts from here ---


-(IBAction)Goback:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)Gohome:(id)sender
{
    HomeViewController *HVC = [[HomeViewController alloc]init];
    [self.navigationController pushViewController:HVC animated:YES];
}

-(IBAction)ProcessCS10:(id)sender
{
    requestTyepe = 1;
    iROSAppDelegate *appDelegate = (iROSAppDelegate*)[[UIApplication sharedApplication] delegate];
    EncryptedSwipeData *esd = appDelegate.encryptedSwipeData;
    NSString  *Amt = [NSString stringWithFormat:@"%.2f",[iRestaurantsGlobalDataClass getInstance].amtToPay];

    
    NSString *   soapMessage = [NSString stringWithFormat:
                                @"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                                "<soap:Body>\n"
                                "<ProcessCS10 xmlns=\"http://www.magensa.net/\">"
                                "<ProcessCS10_Input>"
                                "<TransactionType>A</TransactionType>"
                                "<HostID>MPH596289521</HostID>"
                                "<HostPW>M@dgTi3$a8tV7A</HostPW>"
                                "<MerchantID>MPM953468091</MerchantID>"
                                "<MerchantPW>Webt4mppg!T3$7</MerchantPW>"
                                "<EncryptedTrack1>%@</EncryptedTrack1>"
                                "<EncryptedTrack2>%@</EncryptedTrack2>"
                                "<EncryptedTrack3></EncryptedTrack3>"
                                "<EncryptedMP>%@</EncryptedMP>"
                                "<KSN>%@</KSN>"
                                "<MPStatus>%@</MPStatus>"
                                "<Amt>%.2f</Amt>"
                                "<TaxAmt></TaxAmt>"
                                "<PurchaseOrder></PurchaseOrder>"
                                "<InvoiceNumber></InvoiceNumber>"
                                "<CVV></CVV>"
                                "<ZIP></ZIP>"
                                "<AuthCode></AuthCode>"
                                "</ProcessCS10_Input>"
                                "</ProcessCS10>"
                                "</soap:Body>"
                                "</soap:Envelope>",esd.track1Encrypted,esd.track2Encrypted, esd.getMagnePrintData,esd.ksn, esd.getMagnePrintDataStatus, SI.amtToPay];
    NSLog(@"Soap message - %@",soapMessage);
    ServerConnectionClass* network = [[ServerConnectionClass alloc]init] ;
    self.connObject = network;
    self.connObject.delegate = self;
    [self.connObject SoapWebCall:kMegensaServerURL SoapMessage:soapMessage SoapAction:@"http://www.magensa.net/ProcessCS10"];
    
  }


-(IBAction)SaveProcessCS10
{
    requestTyepe = 2;
       
    NSString  *Amt = [NSString stringWithFormat:@"%.2f",[iRestaurantsGlobalDataClass getInstance].amtToPay];
    
    NSString* ReqStr = [[NSString alloc]initWithFormat:@"%@{\"user_id\":\"%d\" ,\"order_id\":\"%@\",\"transactionid\":\"%@\",\"transactionmsg\":\"%@\",\"authcode\":\"%@\",\"chdname\":\"%@\" ,\"pantast4\":\"%@\" ,\"cardexpdt\":\"%@\" ,\"amount\":\"%f\"}",kProcesscs10URL,SI.loggedUserId,[iRestaurantsGlobalDataClass getInstance].orderidForpay,[SI.DicProcessCS10 objectForKey:@"TransactionID"], [SI.DicProcessCS10 objectForKey:@"TransactionMsg"], [SI.DicProcessCS10 objectForKey:@"AuthCode"], [SI.DicProcessCS10 objectForKey:@"CHDName"],[SI.DicProcessCS10 objectForKey:@"PANLast4"], [SI.DicProcessCS10 objectForKey:@"CardExpDt"] , 0.01];
    ServerConnectionClass* network = [[ServerConnectionClass alloc]init] ;
    self.connObject = network;
    self.connObject.delegate = self;

    [self.connObject makeRequestForUrl:ReqStr];

    
}




#pragma XML parser delegates   ---  ---  ---

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *) namespaceURI qualifiedName:(NSString *)qName
   attributes: (NSDictionary *)attributeDict
{
    {        if(!soapResults)
        {
            soapResults = [[NSMutableString alloc] init];
        }
}
}
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
     soapResults = [[NSMutableString alloc] init];
    [soapResults appendString: string];
}
-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
//    switch (buttonClicked) {
//        case 1:
            if( [elementName isEqualToString:@"TransactionID"])
            {
                [[iRestaurantsGlobalDataClass getInstance].DicProcessCS10 setObject:soapResults forKey:@"TransactionID"];
            }
            else  if( [elementName isEqualToString:@"TransactionMsg"])
            {
                [[iRestaurantsGlobalDataClass getInstance].DicProcessCS10 setObject:soapResults forKey:@"TransactionMsg"];
            }

            else  if( [elementName isEqualToString:@"TransactionStatus"])
            {
                [[iRestaurantsGlobalDataClass getInstance].DicProcessCS10 setObject:soapResults forKey:@"TransactionStatus"];
            }
            else  if( [elementName isEqualToString:@"StatusCode"])
            {
                [[iRestaurantsGlobalDataClass getInstance].DicProcessCS10 setObject:soapResults forKey:@"StatusCode"];
            }
            else  if( [elementName isEqualToString:@"StatusMsg"])
            {
                [[iRestaurantsGlobalDataClass getInstance].DicProcessCS10 setObject:soapResults forKey:@"StatusMsg"];
            }
            else  if( [elementName isEqualToString:@"AuthCode"])
            {
                [[iRestaurantsGlobalDataClass getInstance].DicProcessCS10 setObject:soapResults forKey:@"AuthCode"];
            }
            else  if( [elementName isEqualToString:@"PANLast4"])
            {
                [[iRestaurantsGlobalDataClass getInstance].DicProcessCS10 setObject:soapResults forKey:@"PANLast4"];
            }
            else  if( [elementName isEqualToString:@"CHDName"])
            {
                [[iRestaurantsGlobalDataClass getInstance].DicProcessCS10 setObject:soapResults forKey:@"CHDName"];
            }
            else  if( [elementName isEqualToString:@"CardExpDt"])
            {
                [[iRestaurantsGlobalDataClass getInstance].DicProcessCS10 setObject:soapResults forKey:@"CardExpDt"];
            }




            NSLog(@"Trans ProcessCS10 Dic %@ ",[iRestaurantsGlobalDataClass getInstance].DicProcessCS10);
//            break;
//            
//        case 2:
//            break;
//        default:
//            break;
//    }
    
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
 
         UILabel *statusLbl = (UILabel*)[self.view viewWithTag:120];
        [statusLbl setText:[[iRestaurantsGlobalDataClass getInstance].DicProcessCS10 objectForKey:@"TransactionMsg" ]];
        
    [xmlParser abortParsing];
    xmlParser.delegate =nil;
    xmlParser = nil;
    [self SaveProcessCS10];

}



#pragma mark- MyCabuUserNetwork delegate methods

- (void) connectionEndWithError:(NSError*)error
{
    [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [self removeConnectionClassObject];
}

- (void) connectionEndedSuccefullyWithData:(NSData*)data
{
    if (requestTyepe ==1)
    {
    
    NSString *  theXMLResponse = [[NSString alloc] initWithBytes: [data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    if (theXMLResponse.length >0 && ![theXMLResponse isKindOfClass:[NSNull class]])
    {
//        [self performSegueWithIdentifier:@"InvoiceReciept" sender:self]; // done by harry
        NSLog(@"%@", theXMLResponse);
        
        xmlParser = [[NSXMLParser alloc] initWithData: data];
        [xmlParser setDelegate: self];
        [xmlParser setShouldResolveExternalEntities: YES];
        [xmlParser parse];
        [self removeConnectionClassObject];

        }
    else
    {
        [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    }
    else if (requestTyepe==2)
    {
        NSError *localError = nil;
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        
        NSLog(@"MY dictionary====%@",dictionary);
        if (dictionary != nil)
        {
            NSString* result = [dictionary objectForKey:@"result"];
            if ([result isEqualToString:@"Success"])
            {
                TripAdjustmentVC *layoutVC=[[TripAdjustmentVC alloc] init];
                [self.navigationController pushViewController:layoutVC animated:YES];
            }
       }
        else
        {
            [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        }

    }
       [self removeConnectionClassObject];
    }


- (void) removeConnectionClassObject
{
    self.connObject.delegate = nil;
    self.connObject = nil;
}






@end
