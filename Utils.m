//
//  Utils.m
//  MyCabuUserIPhone
//
//  Created by iMac Apple on 29/01/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "Utils.h"
#import "iROSAppDelegate.h"
#import <sys/utsname.h>
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>

@implementation Utils


#pragma mark - AlertView
////----- show a alert massage
+ (void) showAlertView :(NSString*)title message:(NSString*)msg delegate:(id)delegate
      cancelButtonTitle:(NSString*)CbtnTitle otherButtonTitles:(NSString*)otherBtnTitles
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:delegate
										  cancelButtonTitle:CbtnTitle otherButtonTitles:otherBtnTitles, nil];
	[alert show];
	[alert release];
    
}

//---------------Email format check-------------//
+ (BOOL) validateEmail: (NSString *) candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

+ (void)startActivityIndicator
{
    iROSAppDelegate *appDelegate = (iROSAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![appDelegate.window.subviews containsObject:appDelegate.activityView])
        [appDelegate.window addSubview:appDelegate.activityView];
    
}

+ (void)stopActivityIndicator
{
    iROSAppDelegate *appDelegate = (iROSAppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate.window.subviews containsObject:appDelegate.activityView])
        [appDelegate.activityView removeFromSuperview];
}

+ (void) showBackgroundView
{
    iROSAppDelegate *appDelegate = (iROSAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (![appDelegate.window.subviews containsObject:appDelegate.customBackgroundView])
        [appDelegate.window addSubview:appDelegate.customBackgroundView];
}

+ (void) removeBackroundView
{
    iROSAppDelegate *appDelegate = (iROSAppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate.window.subviews containsObject:appDelegate.customBackgroundView])
        [appDelegate.customBackgroundView removeFromSuperview];
}

+ (UIView*) backGroundView
{
    iROSAppDelegate *appDelegate = (iROSAppDelegate *)[[UIApplication sharedApplication] delegate];
    return appDelegate.customBackgroundView;
}

+ (void) showBackgroundImage
{
    iROSAppDelegate *appDelegate = (iROSAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.backgroundmageView.alpha = 1;
}
+ (void) removeBackgrounImage
{
    iROSAppDelegate *appDelegate = (iROSAppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate.window.subviews containsObject:appDelegate.backgroundmageView])
        [appDelegate.backgroundmageView setAlpha:0];
}

+ (NSString *) deviceid
{
     NSUUID* uuid = [[UIDevice currentDevice] identifierForVendor];
    
    return [uuid UUIDString];
}
+ (NSString* )valueForKey:(NSString*)key
{
    NSUserDefaults* userDefault = [[NSUserDefaults alloc] init];
    NSString* value = [userDefault objectForKey:key];
    [userDefault release];
    return value;
}

+ (BOOL) connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

+ (UIImage *)makeRoundedImage:(UIImage *) image radius: (float) radius
{
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    imageLayer.contents = (id) image.CGImage;
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = radius;
    
    UIGraphicsBeginImageContext(image.size);
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;
}

+ (UIImage*) buttonImageColor:(UIColor*)color frame:(CGRect)frame
{
    CGSize size = CGSizeMake(frame.size.width, frame.size.height);
    UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    [color set];
    UIRectFill(CGRectMake(0, 0, size.width, size.height));
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    image = [Utils makeRoundedImage:image radius:5.0f];
    return image;
}


@end
