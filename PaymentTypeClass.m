//
//  DriverNewOrderClass.m
//  MyCabuDriverIphone
//
//  Created by Dex on 05/04/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "PaymentTypeClass.h"
#import "HomeViewController.h"

@implementation PaymentTypeClass

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:0.8];
               self.bgVw=[[UIView alloc] init];
        [self.bgVw setBackgroundColor:[UIColor whiteColor]];
        [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-500)/2, ([UIScreen mainScreen].bounds.size.height-200)/2,500 , 250)];
        [self addSubview:self.bgVw];
        [self.bgVw.layer setBorderColor:[UIColor colorWithRed:77.0f/255.0f green:77.0f/255.0f blue:77.0f/255.0f alpha:1].CGColor];
        [self.bgVw.layer setBorderWidth:6.0f];
        [self.bgVw.layer setCornerRadius:4.0f];
        
        
        
        
        self.closeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeBtn setFrame:CGRectMake( self.bgVw.frame.size.width-60,1, 60, 60)];
        [self.closeBtn setBackgroundColor:[UIColor clearColor]];
        [self.closeBtn setExclusiveTouch:YES];
        [self.bgVw addSubview:self.closeBtn];
        [self.closeBtn setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        
        
        UILabel *popupHeader=[[UILabel alloc] init];
        [popupHeader setFrame:CGRectMake(0, 0, self.bgVw.frame.size.width, 60)];
        [popupHeader setBackgroundColor:[UIColor clearColor]];
        [popupHeader setTextAlignment:NSTextAlignmentCenter];
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [popupHeader setTextColor:kDarkBrownColor];
        }
        else
        {
            [popupHeader setTextColor:kOrangeColor];
        }
       
        [popupHeader setText:@"Payment"];
        [popupHeader setFont:[UIFont systemFontOfSize:25]];
        [self.bgVw addSubview:popupHeader];
        
        UILabel *popupHeaderlbl=[[UILabel alloc] init];
        [popupHeaderlbl setFrame:CGRectMake(30, popupHeader.frame.origin.y+50, self.bgVw.frame.size.width-60, 40)];
        [popupHeaderlbl setBackgroundColor:[UIColor clearColor]];
        [popupHeaderlbl setTextAlignment:NSTextAlignmentLeft];
        [popupHeaderlbl setTextColor:[UIColor darkGrayColor]];
        [popupHeaderlbl setText:@"Please select the payment method:"];
        [popupHeaderlbl setFont:[UIFont systemFontOfSize:18]];
        [self.bgVw addSubview:popupHeaderlbl];
        
        UIView *viewLine=[[UIView alloc] init];
        [viewLine setBackgroundColor:[UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1]];
        [viewLine setFrame:CGRectMake(30, popupHeaderlbl.frame.origin.y+40, self.bgVw.frame.size.width-60, 1)];
        [self.bgVw addSubview:viewLine];
        
        
        UILabel *orlbl=[[UILabel alloc] init];
        [orlbl setFrame:CGRectMake((self.bgVw.frame.size.width-30)/2, viewLine.frame.origin.y+36, 30, 30)];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [orlbl setBackgroundColor:kDarkBrownColor];
        }
        else
        {
            [orlbl setBackgroundColor:kOrangeColor];
        }
        
        [orlbl setTextAlignment:NSTextAlignmentCenter];
        [orlbl setTextColor:[UIColor whiteColor]];
        [orlbl setText:@"or"];
        orlbl.layer.cornerRadius = orlbl.frame.size.width / 2;
        orlbl.clipsToBounds = YES;
        [self.bgVw addSubview:orlbl];
        
        
        
        fullPaymntBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [fullPaymntBtn setFrame:CGRectMake(30, viewLine.frame.origin.y+30, 190, 40)];
        [fullPaymntBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 0.0f, 0.0f)];
        [fullPaymntBtn setTitle:@"Full Payment" forState:UIControlStateNormal];
        [fullPaymntBtn setBackgroundColor:[UIColor clearColor]];
        [fullPaymntBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [fullPaymntBtn addTarget:self action:@selector(fullPaymentOptions:) forControlEvents:UIControlEventTouchUpInside];
        [fullPaymntBtn setExclusiveTouch:YES];
        [fullPaymntBtn setSelected:NO];
        [self.bgVw addSubview:fullPaymntBtn];
        UIImageView *imgvw2=[[UIImageView alloc] init];
        [imgvw2 setFrame:CGRectMake(-17, -14, 70, 70)];
        [imgvw2 setImage:[UIImage imageNamed:@"radio_off"]];
        [fullPaymntBtn addSubview:imgvw2];
        
        splitPaymntBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [splitPaymntBtn setFrame:CGRectMake(orlbl.frame.origin.x+50, viewLine.frame.origin.y+30, 190, 40)];
        [splitPaymntBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 0.0f, 0.0f)];
        [splitPaymntBtn setTitle:@"Split Payment" forState:UIControlStateNormal];
        [splitPaymntBtn setBackgroundColor:[UIColor clearColor]];
        [splitPaymntBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [splitPaymntBtn addTarget:self action:@selector(splitPaymentOptions:) forControlEvents:UIControlEventTouchUpInside];
        [splitPaymntBtn setExclusiveTouch:YES];
        [splitPaymntBtn setSelected:NO];
        [self.bgVw addSubview:splitPaymntBtn];
        UIImageView *imgvw3=[[UIImageView alloc] init];
        [imgvw3 setFrame:CGRectMake(-17, -14, 70, 70)];
        [imgvw3 setImage:[UIImage imageNamed:@"radio_off"]];
        [splitPaymntBtn addSubview:imgvw3];
        
        
        
        
        self.cashPaymntBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [ self.cashPaymntBtn setFrame:CGRectMake(30, fullPaymntBtn.frame.origin.y+60, 140, 40)];
        //[cashPaymntBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 0.0f, 0.0f)];
        [ self.cashPaymntBtn setTitle:@"Cash" forState:UIControlStateNormal];
        [ self.cashPaymntBtn setBackgroundColor:[UIColor clearColor]];
        [ self.cashPaymntBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [ self.cashPaymntBtn addTarget:self action:@selector(cashPayment:) forControlEvents:UIControlEventTouchUpInside];
        [ self.cashPaymntBtn setExclusiveTouch:YES];
        [ self.cashPaymntBtn setSelected:NO];
        [self.bgVw addSubview: self.cashPaymntBtn];
        UIImageView *imgvw4=[[UIImageView alloc] init];
        [imgvw4 setFrame:CGRectMake(-17, -14, 70, 70)];
        [imgvw4 setImage:[UIImage imageNamed:@"radio_off"]];
        [ self.cashPaymntBtn addSubview:imgvw4];
        [ self.cashPaymntBtn setHidden:YES];
        
         self.creditPaymntBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [ self.creditPaymntBtn setFrame:CGRectMake(orlbl.frame.origin.x+50, fullPaymntBtn.frame.origin.y+60, 140, 40)];
        //[creditPaymntBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 0.0f, 0.0f, 0.0f)];
        [ self.creditPaymntBtn setTitle:@"Credit" forState:UIControlStateNormal];
        [ self.creditPaymntBtn setBackgroundColor:[UIColor clearColor]];
        [ self.creditPaymntBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [ self.creditPaymntBtn addTarget:self action:@selector(creditPayment:) forControlEvents:UIControlEventTouchUpInside];
        [ self.creditPaymntBtn setExclusiveTouch:YES];
        [ self.creditPaymntBtn setSelected:NO];
        [self.bgVw addSubview: self.creditPaymntBtn];
        UIImageView *imgvw5=[[UIImageView alloc] init];
        [imgvw5 setFrame:CGRectMake(-17, -14, 70, 70)];
        [imgvw5 setImage:[UIImage imageNamed:@"radio_off"]];
        [ self.creditPaymntBtn addSubview:imgvw5];
        [ self.creditPaymntBtn setHidden:YES];

        self.splitByItemBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [ self.splitByItemBtn setFrame:CGRectMake(30, fullPaymntBtn.frame.origin.y+60, 190, 40)];
        //[cashPaymntBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 0.0f, 0.0f)];
        [ self.splitByItemBtn setTitle:@"Split by Item" forState:UIControlStateNormal];
        [ self.splitByItemBtn setBackgroundColor:[UIColor clearColor]];
        [ self.splitByItemBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [ self.splitByItemBtn addTarget:self action:@selector(splitByItemPayment:) forControlEvents:UIControlEventTouchUpInside];
        [ self.splitByItemBtn setExclusiveTouch:YES];
        [ self.splitByItemBtn setSelected:NO];
        [self.bgVw addSubview: self.splitByItemBtn];
        UIImageView *imgvw6=[[UIImageView alloc] init];
        [imgvw6 setFrame:CGRectMake(-17, -14, 70, 70)];
        [imgvw6 setImage:[UIImage imageNamed:@"radio_off"]];
        [ self.splitByItemBtn addSubview:imgvw6];
        [ self.splitByItemBtn setHidden:YES];
        
        self.splitByPeopleBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [ self.splitByPeopleBtn setFrame:CGRectMake(orlbl.frame.origin.x+50, fullPaymntBtn.frame.origin.y+60, 200, 40)];
        //[creditPaymntBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 0.0f, 0.0f, 0.0f)];
        [ self.splitByPeopleBtn setTitle:@"Split by people" forState:UIControlStateNormal];
        [ self.splitByPeopleBtn setBackgroundColor:[UIColor clearColor]];
        [ self.splitByPeopleBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [ self.splitByPeopleBtn addTarget:self action:@selector(splitByPeoplePayment:) forControlEvents:UIControlEventTouchUpInside];
        [ self.splitByPeopleBtn setExclusiveTouch:YES];
        [ self.splitByPeopleBtn setSelected:NO];
        [self.bgVw addSubview: self.splitByPeopleBtn];
        UIImageView *imgvw7=[[UIImageView alloc] init];
        [imgvw7 setFrame:CGRectMake(-17, -14, 70, 70)];
        [imgvw7 setImage:[UIImage imageNamed:@"radio_off"]];
        [ self.splitByPeopleBtn addSubview:imgvw7];
        [ self.splitByPeopleBtn setHidden:YES];
        
        
        numberOfPeoplelbl=[[UILabel alloc] init];
        [numberOfPeoplelbl setFrame:CGRectMake(30, self.splitByPeopleBtn.frame.origin.y+50, 180, 40)];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [numberOfPeoplelbl setBackgroundColor:kDarkBrownColor];
        }
        else
        {
            [numberOfPeoplelbl setBackgroundColor:kOrangeColor];
        }
        
        [numberOfPeoplelbl setTextAlignment:NSTextAlignmentCenter];
        [numberOfPeoplelbl setTextColor:[UIColor whiteColor]];
        [numberOfPeoplelbl setText:@"Number of people"];
        [numberOfPeoplelbl setFont:[UIFont systemFontOfSize:18]];
        [self.bgVw addSubview:numberOfPeoplelbl];
        [numberOfPeoplelbl setHidden: YES];
        
        
        numberFld=[[UITextField alloc] init];
        [numberFld setFrame:CGRectMake(212,self.splitByPeopleBtn.frame.origin.y+50,208, 40)];
        numberFld.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
        numberFld.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
        [numberFld setBackgroundColor:[UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1]];
        [numberFld setTextColor:[UIColor darkGrayColor]];
        [self.bgVw addSubview:numberFld];
        UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 30)];// view added for padding
        numberFld.leftView = paddingView3;
        numberFld.leftViewMode = UITextFieldViewModeAlways;
        [numberFld setHidden:YES];
        [numberFld setDelegate:self];
        [numberFld setKeyboardType:UIKeyboardTypeNumberPad];

        self.goBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.goBtn setFrame:CGRectMake(424,self.splitByPeopleBtn.frame.origin.y+50,40, 40)];
        [self.goBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 0.0f, 0.0f)];
        [self.goBtn setTitle:@"OK" forState:UIControlStateNormal];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [self.goBtn setBackgroundColor:kDarkBrownColor];
        }
        else
        {
            [self.goBtn setBackgroundColor:kOrangeColor];
        }
        [self.goBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.goBtn setExclusiveTouch:YES];
        [self.goBtn setHidden:YES];
        [self.bgVw addSubview:self.goBtn];
        
        _proceedBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [_proceedBtn setFrame:CGRectMake(self.bgVw.frame.size.width/2-60,self.bgVw.frame.size.height-42,120, 30)];
        [_proceedBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 0.0f, 0.0f)];
        [_proceedBtn setTitle:@"Pay" forState:UIControlStateNormal];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [_proceedBtn setBackgroundColor:kDarkBrownColor];
        }
        else
        {
            [_proceedBtn setBackgroundColor:kOrangeColor];
        }
        
        [_proceedBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [_proceedBtn setExclusiveTouch:YES];
        [_proceedBtn setHidden:NO];
        [self.bgVw addSubview:_proceedBtn];
    }
  
    return self;
}



-(void)fullPaymentOptions:(id)sender
{
     UIImageView *imgvw=(UIImageView*)[[sender subviews] objectAtIndex:0];
    [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-500)/2, ([UIScreen mainScreen].bounds.size.height-280)/2,500 , 280)];
    [numberFld setHidden:YES];
    [numberOfPeoplelbl setHidden:YES];
    [_proceedBtn setFrame:CGRectMake(self.bgVw.frame.size.width/2-60,self.bgVw.frame.size.height-42,120, 30)];

    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        [splitPaymntBtn setSelected:NO];
         UIImageView *imgvw_split=(UIImageView*)[[splitPaymntBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_off"]];
        [imgvw setImage:[UIImage imageNamed:@"radio_on"]];
        [self.creditPaymntBtn setHidden:NO];
        [self.cashPaymntBtn setHidden:NO];
        [self.splitByPeopleBtn setHidden:YES];
        [self.splitByItemBtn setHidden:YES];
        [self.goBtn setHidden:YES];
        
        [self.creditPaymntBtn setSelected:NO];
        [self.cashPaymntBtn setSelected:NO];
        UIImageView *imgvw_split2=(UIImageView*)[[self.creditPaymntBtn subviews] objectAtIndex:0];
        [imgvw_split2 setImage:[UIImage imageNamed:@"radio_off"]];
        UIImageView *imgvw_split3=(UIImageView*)[[self.cashPaymntBtn subviews] objectAtIndex:0];
        [imgvw_split3 setImage:[UIImage imageNamed:@"radio_off"]];
    }
    else
    {
         [sender setSelected:NO];
        [imgvw setImage:[UIImage imageNamed:@"radio_off"]];
        [self.creditPaymntBtn setHidden:YES];
        [self.cashPaymntBtn setHidden:YES];
        
        [splitPaymntBtn setSelected:YES];
        UIImageView *imgvw_split=(UIImageView*)[[splitPaymntBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_on"]];
        [self.splitByPeopleBtn setHidden:NO];
        [self.splitByItemBtn setHidden:NO];
        [self.goBtn setHidden:YES];
        [self.splitByPeopleBtn setSelected:NO];
        [self.splitByItemBtn setSelected:NO];
        UIImageView *imgvw_split2=(UIImageView*)[[self.splitByPeopleBtn subviews] objectAtIndex:0];
        [imgvw_split2 setImage:[UIImage imageNamed:@"radio_off"]];
        UIImageView *imgvw_split3=(UIImageView*)[[self.splitByItemBtn subviews] objectAtIndex:0];
        [imgvw_split3 setImage:[UIImage imageNamed:@"radio_off"]];
    }
   
    
   
}
-(void)splitPaymentOptions:(id)sender
{
    UIImageView *imgvw=(UIImageView*)[[sender subviews] objectAtIndex:0];
    [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-500)/2, ([UIScreen mainScreen].bounds.size.height-300)/2,500 ,300)];
    [_proceedBtn setFrame:CGRectMake(self.bgVw.frame.size.width/2-60,self.bgVw.frame.size.height-42,120, 30)];

    
    [numberFld setHidden:YES];
    [numberOfPeoplelbl setHidden:YES];
    
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        [imgvw setImage:[UIImage imageNamed:@"radio_on"]];
        
        [self.splitByPeopleBtn setHidden:NO];
        [self.splitByItemBtn setHidden:NO];
        [self.splitByPeopleBtn setSelected:NO];
        [self.splitByItemBtn setSelected:NO];
        [self.goBtn setHidden:YES];
        [fullPaymntBtn setSelected:NO];
        UIImageView *imgvw_split=(UIImageView*)[[fullPaymntBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_off"]];
        UIImageView *imgvw_split1=(UIImageView*)[[self.splitByItemBtn subviews] objectAtIndex:0];
        [imgvw_split1 setImage:[UIImage imageNamed:@"radio_off"]];
        UIImageView *imgvw_split2=(UIImageView*)[[self.splitByPeopleBtn subviews] objectAtIndex:0];
        [imgvw_split2 setImage:[UIImage imageNamed:@"radio_off"]];
        [self.creditPaymntBtn setHidden:YES];
        [self.cashPaymntBtn setHidden:YES];
       
    }
    else
    {
        [sender setSelected:NO];
        [imgvw setImage:[UIImage imageNamed:@"radio_off"]];
        [self.splitByPeopleBtn setHidden:YES];
        [self.splitByItemBtn setHidden:YES];
        [numberFld setHidden:YES];
        [numberOfPeoplelbl setHidden:YES];
        [self.goBtn setHidden:YES];
        [fullPaymntBtn setSelected:YES];
        UIImageView *imgvw_split=(UIImageView*)[[fullPaymntBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_on"]];
        [self.creditPaymntBtn setHidden:NO];
        [self.cashPaymntBtn setHidden:NO];
        [self.creditPaymntBtn setSelected:NO];
        [self.cashPaymntBtn setSelected:NO];
        UIImageView *imgvw_split2=(UIImageView*)[[self.creditPaymntBtn subviews] objectAtIndex:0];
        [imgvw_split2 setImage:[UIImage imageNamed:@"radio_off"]];
        UIImageView *imgvw_split3=(UIImageView*)[[self.cashPaymntBtn subviews] objectAtIndex:0];
        [imgvw_split3 setImage:[UIImage imageNamed:@"radio_off"]];


    }
}
-(void)cashPayment:(id)sender
{
    UIImageView *imgvw=(UIImageView*)[[sender subviews] objectAtIndex:0];
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        [imgvw setImage:[UIImage imageNamed:@"radio_on"]];
       
        [self.creditPaymntBtn setSelected:NO];
        UIImageView *imgvw_split=(UIImageView*)[[self.creditPaymntBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_off"]];
    }
    else
    {
        [sender setSelected:NO];
        [imgvw setImage:[UIImage imageNamed:@"radio_off"]];
        
        [self.creditPaymntBtn setSelected:YES];
        UIImageView *imgvw_split=(UIImageView*)[[self.creditPaymntBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_on"]];
    }
}

-(void)creditPayment:(id)sender
{
    UIImageView *imgvw=(UIImageView*)[[sender subviews] objectAtIndex:0];
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        [imgvw setImage:[UIImage imageNamed:@"radio_on"]];
        
        [self.cashPaymntBtn setSelected:NO];
        UIImageView *imgvw_split=(UIImageView*)[[self.cashPaymntBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_off"]];
    }
    else
    {
        [sender setSelected:NO];
        [imgvw setImage:[UIImage imageNamed:@"radio_off"]];
        
        [self.cashPaymntBtn setSelected:YES];
        UIImageView *imgvw_split=(UIImageView*)[[self.cashPaymntBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_on"]];

    }
}
-(void)splitByItemPayment:(id)sender
{
    UIImageView *imgvw=(UIImageView*)[[sender subviews] objectAtIndex:0];
     [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-500)/2, ([UIScreen mainScreen].bounds.size.height-320)/2,500 , 320)];
    [_proceedBtn setFrame:CGRectMake(self.bgVw.frame.size.width/2-60,self.bgVw.frame.size.height-42,120, 30)];

    if (![sender isSelected])
    {
        [sender setSelected:YES];
        [imgvw setImage:[UIImage imageNamed:@"radio_on"]];
        
        [self.splitByPeopleBtn setSelected:NO];
        UIImageView *imgvw_split=(UIImageView*)[[self.splitByPeopleBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_off"]];
        
        
        [numberFld setHidden:NO];
        [numberOfPeoplelbl setHidden:NO];
        [self.goBtn setHidden:NO];
    }
    else
    {
        [sender setSelected:NO];
        [imgvw setImage:[UIImage imageNamed:@"radio_off"]];
        
        [self.splitByPeopleBtn setSelected:YES];
        UIImageView *imgvw_split=(UIImageView*)[[self.splitByPeopleBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_on"]];

    }
}
-(void)splitByPeoplePayment:(id)sender
{
    UIImageView *imgvw=(UIImageView*)[[sender subviews] objectAtIndex:0];
     [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-500)/2, ([UIScreen mainScreen].bounds.size.height-320)/2,500 , 320)];
    [_proceedBtn setFrame:CGRectMake(self.bgVw.frame.size.width/2-60,self.bgVw.frame.size.height-42,120, 30)];

    if (![sender isSelected])
    {
        [sender setSelected:YES];
        [imgvw setImage:[UIImage imageNamed:@"radio_on"]];
        
        [self.splitByItemBtn setSelected:NO];
        UIImageView *imgvw_split=(UIImageView*)[[self.splitByItemBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_off"]];
        
        [numberFld setHidden:NO];
        [numberOfPeoplelbl setHidden:NO];
        [self.goBtn setHidden:NO];

    }
    else
    {
        [sender setSelected:NO];
        [imgvw setImage:[UIImage imageNamed:@"radio_off"]];
        
        [self.splitByItemBtn setSelected:YES];
        UIImageView *imgvw_split=(UIImageView*)[[self.splitByItemBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_on"]];
        
    }
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
        [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-500)/2, ([UIScreen mainScreen].bounds.size.height-320)/2-120,500 , 320)];
    [_proceedBtn setFrame:CGRectMake(self.bgVw.frame.size.width/2-60,self.bgVw.frame.size.height-42,120, 30)];
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-500)/2, ([UIScreen mainScreen].bounds.size.height-320)/2,500 , 320)];
    [_proceedBtn setFrame:CGRectMake(self.bgVw.frame.size.width/2-60,self.bgVw.frame.size.height-42,120, 30)];}
@end
