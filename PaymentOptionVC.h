//
//  PaymentOptionVC.h
//  iROS
//
//  Created by iMac Apple on 18/12/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>


@interface PaymentOptionVC : UIViewController<ServerConnectionDelegate,NSXMLParserDelegate,MFMailComposeViewControllerDelegate>
{
    NSMutableDictionary *RecieptdataResponseDic;
    int btnActionType;
    
    
    NSMutableData *webData;
    NSMutableString *soapResults;
    NSXMLParser *xmlParser;
    BOOL *recordResults;
    MFMailComposeViewController *mailComposer;
    NSString *message ;
    NSString *Response;
    int buttonClicked;



    
    
}
@property(nonatomic, retain) NSMutableData *webData;
@property(nonatomic, retain) NSMutableString *soapResults;
@property(nonatomic, retain) NSXMLParser *xmlParser;

@property (retain, nonatomic) ServerConnectionClass*  connObject;

@end
