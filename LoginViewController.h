//
//  LoginViewController.h
//  iROS
//
//  Created by Dex on 31/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate,ServerConnectionDelegate>
{
   
}
@property (retain, nonatomic) ServerConnectionClass*  connObject;
@end
