//
//  EncryptedSwipedData.h
//  MagTek.iDynamo.ObjC
//
//  Created by Kevin Oliver on 9/24/13.
//  Copyright (c) 2013 Mercury. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EncryptedSwipeData : NSObject

@property (strong, nonatomic) NSString *track1Masked;
@property (strong, nonatomic) NSString *track2Masked;
@property (strong, nonatomic) NSString *track1Encrypted;
@property (strong, nonatomic) NSString *track2Encrypted;
@property (strong, nonatomic) NSString *ksn;
@property (strong, nonatomic) NSString *CardName;
@property (strong, nonatomic) NSString *CardLast4;
@property (strong, nonatomic) NSString *CardExpDate;
@property (strong, nonatomic) NSString *CardPANLength;
@property (strong, nonatomic) NSString *CapMagStripeEncryption;
@property (strong, nonatomic) NSString *CardStatus;
@property (strong, nonatomic) NSString *getMagnePrintData;
@property (strong, nonatomic) NSString *getMagnePrintDataStatus;






//[self.mtSCRALib getResponseType],
//[self.mtSCRALib getTrackDecodeStatus],
//[self.mtSCRALib getCardStatus],
//[self.mtSCRALib getEncryptionStatus],
//[self.mtSCRALib getBatteryLevel],
//[self.mtSCRALib getSwipeCount],
//[self.mtSCRALib getMaskedTracks],
//[self.mtSCRALib getTrack1Masked],
//[self.mtSCRALib getTrack2Masked],
//[self.mtSCRALib getTrack3Masked],
//[self.mtSCRALib getTrack1],
//[self.mtSCRALib getTrack2],
//[self.mtSCRALib getTrack3],
//[self.mtSCRALib getMagnePrint],
//[self.mtSCRALib getMagnePrintStatus],
//[self.mtSCRALib getSessionID],
//[self.mtSCRALib getCardIIN],
//[self.mtSCRALib getCardName],
//[self.mtSCRALib getCardLast4],
//[self.mtSCRALib getCardExpDate],
//[self.mtSCRALib getCardServiceCode],
//[self.mtSCRALib getCardPANLength],
//[self.mtSCRALib getKSN],
//[self.mtSCRALib getDeviceSerial],
//[self.mtSCRALib getTagValue:TLV_CARDIIN],
//[self.mtSCRALib getMagTekDeviceSerial],
//[self.mtSCRALib getFirmware],
//[self.mtSCRALib getTLVVersion],
//[self.mtSCRALib getDeviceName],
//[self.mtSCRALib getCapMSR],
//[self.mtSCRALib getCapTracks],
//[self.mtSCRALib getCapMagStripeEncryption]];
@end
