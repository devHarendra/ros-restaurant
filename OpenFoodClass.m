//
//  OpenFoodClass.m
//  iROS
//
//  Created by iMac Apple on 19/01/15.
//  Copyright (c) 2015 Dex Consulting. All rights reserved.
//

#import "OpenFoodClass.h"
#import "Define.h"

@implementation OpenFoodClass

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:0.8];
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];

        
        
        UIView *dummyView=[[UIView alloc] init];
        [dummyView setBackgroundColor:[UIColor whiteColor]];
        [dummyView setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-600)/2, ([UIScreen mainScreen].bounds.size.height-300)/2,600 , 250)];
        self.bgVw=dummyView;
        [dummyView release];
        [self.bgVw.layer setBorderColor:[UIColor colorWithRed:77.0f/255.0f green:77.0f/255.0f blue:77.0f/255.0f alpha:1].CGColor];
        [self.bgVw.layer setBorderWidth:6.0f];
        [self.bgVw.layer setCornerRadius:4.0f];
        [self addSubview:self.bgVw];
        
        self.closeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeBtn setFrame:CGRectMake( self.bgVw.frame.size.width-60,1, 60, 60)];
        [self.closeBtn setBackgroundColor:[UIColor clearColor]];
        [self.closeBtn setExclusiveTouch:YES];
        [self.bgVw addSubview:self.closeBtn];
        [self.closeBtn setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        
        
        UILabel *AmountLbl=[[UILabel alloc] init];
        [AmountLbl setFrame:CGRectMake(20, 50, 250, 20)];
        [AmountLbl setBackgroundColor:[UIColor clearColor]];
        [AmountLbl setTextAlignment:NSTextAlignmentLeft];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [AmountLbl setTextColor:kDarkBrownColor];
        }
        else
        {
            [AmountLbl setTextColor:kOrangeColor];
        }
        
        [AmountLbl setText:@"Amount:"];
        [self.bgVw addSubview:AmountLbl];
        [AmountLbl release];

        
        _amtTxt = [[ UITextField alloc]init];
        [_amtTxt setFrame:CGRectMake(140, 50, 375, 30)];
        [_amtTxt setPlaceholder:@"Enter Amount"];
        [self.bgVw addSubview:_amtTxt];
        [_amtTxt.layer setBorderWidth:1.0f];
        [_amtTxt.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [_amtTxt setFont:[UIFont systemFontOfSize:18]];
        [_amtTxt setDelegate:self];
        [_amtTxt setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
        [_amtTxt setKeyboardType:UIKeyboardTypeNumberPad];
        
        
        UILabel *instructionLbl=[[UILabel alloc] init];
        [instructionLbl setFrame:CGRectMake(20, 100, 250, 70)];
        [instructionLbl setBackgroundColor:[UIColor clearColor]];
        [instructionLbl setTextAlignment:NSTextAlignmentLeft];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [instructionLbl setTextColor:kDarkBrownColor];
        }
        else
        {
            [instructionLbl setTextColor:kOrangeColor];
        }
        
        [instructionLbl setText:@"Instructions:"];
        [self.bgVw addSubview:instructionLbl];
        [instructionLbl release];
        
        
        UITextView *instructionTxt=[[UITextView alloc] init];
        self.instructionTxt=instructionTxt;
        [instructionTxt release];
        [self.instructionTxt setFrame:CGRectMake(140, 100, 375, 80)];
        [self.bgVw addSubview:self.instructionTxt];
        [self.instructionTxt.layer setBorderWidth:1.0f];
        [self.instructionTxt.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [self.instructionTxt setFont:[UIFont systemFontOfSize:18]];
        [self.instructionTxt setDelegate:self];
        [self.instructionTxt setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
        
        self.UpdateBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.UpdateBtn setFrame:CGRectMake(self.bgVw.frame.size.width/2-60, self.bgVw.frame.size.height-60, 120, 40)];
        [self.UpdateBtn setBackgroundColor:[UIColor clearColor]];
        [self.UpdateBtn setTitle:@"Done" forState:UIControlStateNormal];
        
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [self.UpdateBtn setTitleColor:kDarkBrownColor forState:UIControlStateNormal];
            [self.UpdateBtn.layer setBorderColor:kDarkBrownColor.CGColor];
        }
        else
        {
            [self.UpdateBtn setTitleColor:kOrangeColor forState:UIControlStateNormal];
            [self.UpdateBtn.layer setBorderColor:kOrangeColor.CGColor];
            
        }
        [self.UpdateBtn.layer setBorderWidth:1.0f];
        [self.UpdateBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
        [self.bgVw addSubview:self.UpdateBtn];
     
    }
    return self;
}



@end
