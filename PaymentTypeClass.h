//
//  DriverNewOrderClass.h
//  MyCabuDriverIphone
//
//  Created by Dex on 05/04/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentTypeClass : UIView<UITextFieldDelegate>
{
    UIButton *fullPaymntBtn;
    UIButton *splitPaymntBtn;
    UITextField *numberFld;
    UILabel *numberOfPeoplelbl;
    
}

@property (nonatomic, retain) UIView *bgVw;

@property (nonatomic, retain) UIButton *cashPaymntBtn;
@property (nonatomic, retain) UIButton *creditPaymntBtn;
@property (nonatomic, retain) UIButton *splitByPeopleBtn;
@property (nonatomic, retain) UIButton *splitByItemBtn;
@property (nonatomic, retain) UIButton* closeBtn;
@property (nonatomic, retain) UIButton* proceedBtn;
@property (nonatomic, retain) UIButton *goBtn;
@end
