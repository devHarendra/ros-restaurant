//
//  ReciptInvoiceVC.m
//  iROS
//
//  Created by iMac Apple on 18/12/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "ReciptInvoiceVC.h"

@interface ReciptInvoiceVC ()

@end

@implementation ReciptInvoiceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    NSLog(@"MY dictionary====%@",_ReciptDataDic);
    
    
    if (_btnActionType == 1) {
        NSDictionary * dic = [_ReciptDataDic objectForKey:@"CardAuthenticationResponse"];

        UILabel *cardNo = (UILabel*)[self.view viewWithTag:1];
        [cardNo setText:[NSString stringWithFormat:@"*********%@",[dic objectForKey:@"maskedCardNumber"]]];
        
        UILabel *status = (UILabel*)[self.view viewWithTag:2];
        [status setText:[NSString stringWithFormat:@"%@",[dic objectForKey:@"responseMessage"]]];
        
        UITextView *reciept = (UITextView*)[self.view viewWithTag:60];
        [reciept setText:[NSString stringWithFormat:@"%@",[dic objectForKey:@"customerReceipt"]]];

    }

    else  if (_btnActionType== 3)
    {
        NSDictionary * dic = [_ReciptDataDic objectForKey:@"SaleResponse"];

        UILabel *cardNo = (UILabel*)[self.view viewWithTag:1];
        [cardNo setText:[NSString stringWithFormat:@"%@",[dic objectForKey:@"transactionAmount"]]];
        
        UILabel *status = (UILabel*)[self.view viewWithTag:2];
        [status setText:[NSString stringWithFormat:@"%@",[dic objectForKey:@"status"]]];
        
        UITextView *reciept = (UITextView*)[self.view viewWithTag:60];
        [reciept setText:[NSString stringWithFormat:@"%@",[dic objectForKey:@"customerReceipt"]]];
        
        cardNo = (UILabel*)[self.view viewWithTag:20];
        [cardNo setText:@"Trans. amt"];

        
    }

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
