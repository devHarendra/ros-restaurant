//
//  iROSiROSNewLoginVC.m
//  iROS
//
//  Created by iMac Apple on 23/01/15.
//  Copyright (c) 2015 Dex Consulting. All rights reserved.
//

#import "iROSiROSNewLoginVC.h"
#import "HomeViewController.h"


@interface iROSiROSNewLoginVC ()

@end

@implementation iROSiROSNewLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIButton *keyBtns;
    for (int tag = 1 ; tag<= 12; tag++) {
        keyBtns = (UIButton*)[self.view viewWithTag:tag];
        [keyBtns.layer setBorderColor:[UIColor whiteColor].CGColor];
        [keyBtns.layer setBorderWidth:1.5];
        [keyBtns.layer setCornerRadius:keyBtns.frame.size.height/2];
        [keyBtns setBackgroundColor:[UIColor clearColor]];
        [keyBtns addTarget:self action:@selector(KeyTapHandler:) forControlEvents:UIControlEventTouchUpInside];
    }

    UIButton *loginBtn = (UIButton*)[self.view viewWithTag:13] ;
    [loginBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
    [loginBtn setBackgroundColor:[UIColor clearColor]];
    [loginBtn.layer setBorderWidth:1.0];
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self SetupPinDots];
}


-(void)SetupPinDots
{
    UILabel *circles ;
    for (int tag = 101 ; tag<= 104; tag++) {
        circles = (UILabel*)[self.view viewWithTag:tag];
        [circles.layer setBorderColor:[UIColor whiteColor].CGColor];
        [circles.layer setBorderWidth:1.5];
        [circles.layer setCornerRadius:circles.frame.size.height/2];
        [circles setBackgroundColor:[UIColor clearColor]];
    }
    ActiveDotsCount =0;
    aStr = 0;
}

-(IBAction)KeyTapHandler:(UIButton*)btn
{
    if ([[NSString stringWithFormat:@"%d",aStr] length] > 3) return;  // preventing key stroks after 4 digits
    
    NSString *bStr;
    if (btn.tag == 10) {
        bStr = [NSString stringWithFormat:@"%d%d",aStr, 0];
        aStr = bStr.intValue;
        ActiveDotsCount ++;
        [self DotsHandler];
    }
    else if (btn.tag ==11)
    {
        // handle code here for these buttons
    }
    else if (btn.tag ==12)
    {
        [btn addTarget:self action:@selector(BackSpaceHandler:) forControlEvents:UIControlEventTouchUpInside];
    }

   else
   {
       bStr = [NSString stringWithFormat:@"%d%d",aStr, btn.tag];
       aStr = bStr.intValue;
       ActiveDotsCount ++;
       [self DotsHandler];
       NSLog(@"Keys \n %d",aStr);
   }
   

}

-(void)DotsHandler
{
    UILabel *circles ;
    int aTag = 100 + ActiveDotsCount;
    for (int tag = 101 ; tag<= aTag; tag++)
    {
        circles = (UILabel*)[self.view viewWithTag:tag];
        [circles.layer setBorderColor:[UIColor whiteColor].CGColor];
        [circles.layer setBorderWidth:1.5];
        [circles setClipsToBounds:YES];
        [circles.layer setCornerRadius:circles.frame.size.height/2];
        [circles setBackgroundColor:[UIColor lightGrayColor]];
    }
}

-(IBAction)BackSpaceHandler:(id)sender
{
    UILabel *circles ;
    for (int tag = 101 ; tag<= 104; tag++)
    {
        circles = (UILabel*)[self.view viewWithTag:tag];
        [circles setBackgroundColor:[UIColor clearColor]];
    }

    NSString *oStr = [NSString stringWithFormat:@"%d",aStr];
    NSString * dStr;
    if (ActiveDotsCount>0)
    {
        dStr = [NSString stringWithFormat:@"%@",[oStr substringToIndex:[oStr length]-1]];
        aStr = dStr.intValue;
        ActiveDotsCount--;
        [self DotsHandler];
        NSLog(@"Keys \n %d",aStr);
    }
 }


- (IBAction)SignIn:(id)sender
{
    ServerConnectionClass* network = [[ServerConnectionClass alloc]init] ;
    self.connObject = network;
    [network release];
    self.connObject.delegate = self;
    NSString *taStr;
    taStr = nil;
    taStr = [NSString stringWithFormat:@"%d", aStr];
    if ([taStr length] == 1)
    {
        taStr = [NSString stringWithFormat:@"000%d",aStr];
    }
    
    else if ([taStr length] == 2)
    {
        taStr = [NSString stringWithFormat:@"00%d",aStr];
    }

    
   else if ([taStr length] == 3)
    {
        taStr = [NSString stringWithFormat:@"0%d",aStr];
    }
   
      NSString*  urlString = [[NSString alloc]initWithFormat:@"%@{\"employee_id\":\"%@\" }",kwaiterEntry,taStr];
      [self.connObject makeRequestForUrl:urlString];
    [urlString release];
    
    }


#pragma Clock in Clock out implement starts from here ----

-(IBAction)ClockIO:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please Select  "
                                                    message:nil
                                                   delegate:self
                                          cancelButtonTitle:@"Clock In"
                                          otherButtonTitles:@"Clock Out", nil];
    [alert setTag:4544];
    [alert show];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    if (alertView.tag == 4544)
    {
        ServerConnectionClass* network = [[ServerConnectionClass alloc]init] ;
        self.connObject = network;
        [network release];
        self.connObject.delegate = self;
        NSString *taStr;
        taStr = nil;
        taStr = [NSString stringWithFormat:@"%d", aStr];
        if ([taStr length] == 1)
        {
            taStr = [NSString stringWithFormat:@"000%d",aStr];
        }
        
        else if ([taStr length] == 2)
        {
            taStr = [NSString stringWithFormat:@"00%d",aStr];
        }
        
        else if ([taStr length] == 3)
        {
            taStr = [NSString stringWithFormat:@"0%d",aStr];
        }
        
        NSString* urlString;
        if (buttonIndex == 0)
        {
            urlString = [[NSString alloc]initWithFormat:@"%@{\"employee_id\":\"%@\" }",kwaiterClockIn,taStr];
        }
        else
        {
            urlString = [[NSString alloc]initWithFormat:@"%@{\"employee_id\":\"%@\" }",kwaiterClockOut,taStr];
        }
            [self.connObject makeRequestForUrl:urlString];
            [urlString release];
    }
}
#pragma Clock in Clock out implement ends here ----


- (void) removeConnectionClassObject
{
    self.connObject.delegate = nil;
    self.connObject = nil;
}
#pragma mark- MyCabuUserNetwork delegate methods

- (void) connectionEndWithError:(NSError*)error
{
    [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [self removeConnectionClassObject];
}

- (void) connectionEndedSuccefullyWithData:(NSData*)data
{
    NSError *localError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    NSLog(@"MY dictionary====%@",dictionary);
    if (dictionary != nil)
    {
        NSString* result = [dictionary objectForKey:@"result"];
        NSString* cmd = [dictionary objectForKey:@"cmd"];

        if ([cmd isEqualToString:@"entry"] && [result isEqualToString:@"Success"])
        {
            HomeViewController *newVC=[[HomeViewController alloc] init];
            [iRestaurantsGlobalDataClass getInstance].loggedUserId = [[dictionary objectForKey:@"user_id"] intValue];
            [self.navigationController pushViewController:newVC animated:YES];
            [newVC release];
        }
        
//      else if ([cmd isEqualToString:@"in"] && [result isEqualToString:@"Success"])
//        {
//            
//        }

        
        
        else // handle alert regarding Diffrent cmd
        {
            if ([cmd isEqualToString:@"entry"])
            {
                [self SetupPinDots];
                [Utils showAlertView:@"Alert" message:[dictionary objectForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            }
            
            else if ([cmd isEqualToString:@"in"])
            {
                 [Utils showAlertView:@"Alert" message:[dictionary objectForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            }
            else if ([cmd isEqualToString:@"out"])
            {
                [Utils showAlertView:@"Alert" message:[dictionary objectForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            }

           
        }
    }
    else
    {
        [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    [self removeConnectionClassObject];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
