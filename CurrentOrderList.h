//
//  CurrentOrderList.h
//  Restaurant Menu
//
//  Created by Dex on 16/07/14.
//  Copyright (c) 2014 Dex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrentOrderList : UIView<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    NSIndexPath *indxPth;
    CGPoint buttonPosition;
   // int rowCount;
}

@property(retain,nonatomic)   UITableView *tblList1;
@property(retain,nonatomic)   UIButton *updateOrderBtn;
@property(retain,nonatomic)   NSArray* cartItems;
@property(assign,nonatomic)   NSInteger rowCount;
@property(retain,nonatomic)   UILabel *currentPriceLbl;
@property(retain,nonatomic)   UILabel *QtyLbl;
@property(retain,nonatomic)   UILabel *lblPrice;
@property(retain,nonatomic)    UIView *footerVw;
@property(retain,nonatomic)    NSString* bgColor;


@end
