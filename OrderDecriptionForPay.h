//
//  OrderDecriptionForPay.h
//  iROS
//
//  Created by iMac Apple on 03/01/15.
//  Copyright (c) 2015 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchPrinterClass.h"
#import <StarIO/SMPort.h>

@interface OrderDecriptionForPay : UIViewController<UITableViewDataSource, UITableViewDelegate, ReturnSelectedCellTextDelegate,UITextFieldDelegate,UICollectionViewDataSource,UICollectionViewDelegate,ServerConnectionDelegate>
{
    IBOutlet UITableView *oTable;
    int Alerttag;
    int popUp_printerList;
    
    IBOutlet UIView *NumpadV;
    IBOutlet UIView *BalReturnV;
    IBOutlet UITextField *mangerId;

    
    UITextField *activeField;
    IBOutlet UIScrollView *scroll;
   float currentCartPriceWithTax;
   float currentCartPriceWithoutTax;
    NSMutableArray *DiscountsArr;

}

@property (nonatomic, retain) NSMutableArray *orderListArr;
@property (nonatomic, retain) SearchPrinterClass *uiasView_printerList;
@property (retain, nonatomic) ServerConnectionClass*  connObject;



@end
