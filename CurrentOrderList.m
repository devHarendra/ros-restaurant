//
//  CurrentOrderList.m
//  Restaurant Menu
//
//  Created by Dex on 16/07/14.
//  Copyright (c) 2014 Dex. All rights reserved.
//

#import "CurrentOrderList.h"
#import "Category_ItemDetail.h"
@implementation CurrentOrderList

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
         self.backgroundColor = [UIColor whiteColor];
        self.rowCount=[self.cartItems count];
        
        UIView *footerVw=[[UIView alloc] init];
        
        [footerVw setFrame:CGRectMake(0, 0, 692, 50)];
        self.footerVw=footerVw;
        [footerVw release];
        [self addSubview:self.footerVw];
        if ([self.bgColor isEqualToString:@"brown"])
        {
            [self.footerVw setBackgroundColor:kDarkBrownColor];
        }
        else
        {
            [self.footerVw setBackgroundColor:kOrangeColor];
        }
        
        
        UIImageView *imgVw=[[UIImageView alloc] init];
        [imgVw setImage:[UIImage imageNamed:@"dine_icon"]];
        [imgVw setFrame:CGRectMake(10 , 5, 40, 40)];
        [footerVw addSubview:imgVw];
        [imgVw release];
        
        UILabel *orderlistLbl=[[UILabel alloc] init];
        orderlistLbl.frame=CGRectMake(60, 0, 180, 50);
        [orderlistLbl setText:@"Current order list"];
        [orderlistLbl setTextAlignment:NSTextAlignmentLeft];
        [orderlistLbl setBackgroundColor:[UIColor clearColor]];
        [orderlistLbl setFont:[UIFont systemFontOfSize:18.0f]];
        [orderlistLbl setTextColor:[UIColor whiteColor]];
        [footerVw addSubview:orderlistLbl];
        [orderlistLbl release];
        
        
        UILabel *currentPriceLbl=[[UILabel alloc] init];
        self.currentPriceLbl=currentPriceLbl;
         [currentPriceLbl release];
        self.currentPriceLbl.frame=CGRectMake(485, 5, 100, 40);
       // [self.currentPriceLbl setText:@"$35.00"];
        [self.currentPriceLbl setTextAlignment:NSTextAlignmentCenter];
        [self.currentPriceLbl setBackgroundColor:[UIColor clearColor]];
        [self.currentPriceLbl setFont:[UIFont boldSystemFontOfSize:18.0f]];
        [self.currentPriceLbl setTextColor:[UIColor whiteColor]];
        [footerVw addSubview:self.currentPriceLbl];
        [self.currentPriceLbl.layer setBorderWidth:1.0f];
        [self.currentPriceLbl.layer setBorderColor:[UIColor whiteColor].CGColor];
       
        
        
        UITableView *tabelVw=[[UITableView alloc] init];
        tabelVw.frame=CGRectMake(0, 50, 692, 600);
        self.tblList1=tabelVw;
        [tabelVw release];
        [self.tblList1 setDelegate:self];
        [self.tblList1 setDataSource:self];
        [self.tblList1 setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:self.tblList1];
      
        
        UIView *viewLine=[[UIView alloc] init];
        [viewLine setBackgroundColor:[UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1]];
        [viewLine setFrame:CGRectMake(0, 651, 692, 1)];
        [self addSubview:viewLine];
        [viewLine release];

       self.updateOrderBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.updateOrderBtn setFrame:CGRectMake(571, 665, 120, 40)];
        [self.updateOrderBtn setBackgroundColor:[UIColor clearColor]];
          [self.updateOrderBtn setTitle:@"Submit Order" forState:UIControlStateNormal];
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [self.updateOrderBtn setTitleColor:kDarkBrownColor forState:UIControlStateNormal];
            [self.updateOrderBtn.layer setBorderColor:kDarkBrownColor.CGColor];
        }
        else
        {
            [self.updateOrderBtn setTitleColor:kOrangeColor forState:UIControlStateNormal];
            [self.updateOrderBtn.layer setBorderColor:kOrangeColor.CGColor];
            
        }
        [self.updateOrderBtn.layer setBorderWidth:1.0f];
        [self.updateOrderBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
        [self addSubview:self.updateOrderBtn];
        
            
    }
    return self;
}
#pragma mark - Table View Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return self.rowCount;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        //[cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        UIView *bgColorView = [[UIView alloc] init];
        [bgColorView setBackgroundColor:[UIColor colorWithRed:234/255.0f  green:122/255.0f blue:77/255.0f alpha:1.0]];
        
        [cell setSelectedBackgroundView:bgColorView];
        [bgColorView release];
        
        UIImageView *itemImg=[[UIImageView alloc]initWithFrame:CGRectMake(10,10,140,131)];
        [cell.contentView addSubview:itemImg];
        [itemImg setTag:200];
        [itemImg release];
        
        // Setting Labels On Table Cotent View
        UILabel *lblToken=[[UILabel alloc]initWithFrame:CGRectMake(160,0,140,151)];
        [lblToken setNumberOfLines:2];
         [lblToken setTag:300];
        [lblToken setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
        [lblToken setTextAlignment:NSTextAlignmentCenter];
        [lblToken setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:lblToken];
        [lblToken release];
        
        UILabel *lblOrder=[[UILabel alloc]initWithFrame:CGRectMake(310,0,40,151)];
         [lblOrder setTag:400];
        [lblOrder setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
        [lblOrder setTextAlignment:NSTextAlignmentRight];
        [lblOrder setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:lblOrder];
        [lblOrder release];
        
        UIButton * increaseQtyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [increaseQtyBtn setFrame:CGRectMake(357, 56, 40, 40)];
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [increaseQtyBtn setBackgroundColor:kDarkBrownColor];
        }
        else
        {
             [increaseQtyBtn setBackgroundColor:kOrangeColor];
        }
        [increaseQtyBtn setTitle:@"+" forState:UIControlStateNormal];
        [increaseQtyBtn.titleLabel setFont:[UIFont systemFontOfSize:22.0f]];
        [increaseQtyBtn addTarget:self action:@selector(increaseQty:) forControlEvents:UIControlEventTouchUpInside];
        [increaseQtyBtn setTag:indexPath.row];
        [cell.contentView addSubview:increaseQtyBtn];

        UILabel *QtyLbl=[[UILabel alloc]initWithFrame:CGRectMake(397,55.5,50,40)];
        self.QtyLbl=QtyLbl;
        [self.QtyLbl setTag:500];
        [self.QtyLbl setTextAlignment:NSTextAlignmentCenter];
        [self.QtyLbl setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:self.QtyLbl];
        [self.QtyLbl.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [self.QtyLbl.layer setBorderWidth:1.0f];
        [QtyLbl release];
        
        UIButton * decreaseQtyBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [decreaseQtyBtn setFrame:CGRectMake(447, 56, 40, 40)];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [decreaseQtyBtn setBackgroundColor:kDarkBrownColor ];
        }
        else
        {
            [decreaseQtyBtn setBackgroundColor:kOrangeColor ];

        }
        [decreaseQtyBtn setTitle:@"-" forState:UIControlStateNormal];
        [decreaseQtyBtn.titleLabel setFont:[UIFont systemFontOfSize:22.0f]];
        [decreaseQtyBtn addTarget:self action:@selector(decreaseQty:) forControlEvents:UIControlEventTouchUpInside];
        [decreaseQtyBtn setTag:indexPath.row];
        [cell.contentView addSubview:decreaseQtyBtn];

        UILabel *lblPrice=[[UILabel alloc]initWithFrame:CGRectMake(500,0,80,151)];
        self.lblPrice=lblPrice;
        [lblPrice release];
        
        [self.lblPrice setTextAlignment:NSTextAlignmentCenter];
        [self.lblPrice setBackgroundColor:[UIColor clearColor]];
        [self.lblPrice setTag:600];
        [cell.contentView addSubview:self.lblPrice];
       
       
        UIButton *btnPayment=[UIButton buttonWithType:UIButtonTypeCustom];
        [btnPayment setFrame:CGRectMake(610,50,80,51)];
        [btnPayment setBackgroundColor:[UIColor clearColor]];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [btnPayment setTitleColor:kDarkBrownColor forState:UIControlStateNormal];
        }
        else
        {
             [btnPayment setTitleColor:kOrangeColor forState:UIControlStateNormal];
        }
        [btnPayment setTag:100];
        [cell.contentView addSubview:btnPayment];
    }
    if (indexPath.row%2==0)
    {
        cell.backgroundColor=[UIColor whiteColor];
    } else {
        cell.backgroundColor=[UIColor colorWithRed:245/255.0f green:245/255.0f blue:245/255.0f alpha:1.0];
    }
    if (self.rowCount==0) {
        [self.tblList1 setSeparatorColor:[UIColor clearColor]];
    }
    
     UIImageView *img =(UIImageView*) [cell viewWithTag:200];
     img.image=[[(Category_ItemDetail*)[self.cartItems objectAtIndex:indexPath.row] imgVw] image];
    
     UILabel *lblToken =(UILabel*) [cell viewWithTag:300];
     [lblToken setText:[(Category_ItemDetail*)[self.cartItems objectAtIndex:indexPath.row] itemName]];
    
     UILabel *lblOrder =(UILabel*) [cell viewWithTag:400];
    [lblOrder setText:@"Qty"];
    
     UILabel *QtyLbl =(UILabel*) [cell viewWithTag:500];
    [QtyLbl setText:[(Category_ItemDetail*)[self.cartItems objectAtIndex:indexPath.row] quantity]];
    
     UILabel *lblPrice =(UILabel*) [cell viewWithTag:600];
    [lblPrice setText:[NSString stringWithFormat:@"$%@",[(Category_ItemDetail*)[self.cartItems objectAtIndex:indexPath.row] price]]];
    
    UIButton *lblType =(UIButton*) [cell viewWithTag:100];
    [lblType setTitle:@"X" forState:UIControlStateNormal];
    [lblType.titleLabel setFont:[UIFont boldSystemFontOfSize:17.0f]];
    [lblType addTarget:self action:@selector(rowDeleteBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    indxPth=indexPath;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 151;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    tableView.userInteractionEnabled = NO;

    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
    {
        [[[[[[[[tableView subviews] objectAtIndex:0] subviews] objectAtIndex:indexPath.row] contentView] subviews] objectAtIndex:3] setBackgroundColor:kDarkBrownColor];
        [[[[[[[[tableView subviews] objectAtIndex:0] subviews] objectAtIndex:indexPath.row] contentView] subviews] objectAtIndex:5] setBackgroundColor:kDarkBrownColor];
    }
    else
    {
        [[[[[[[[tableView subviews] objectAtIndex:0] subviews] objectAtIndex:indexPath.row] contentView] subviews] objectAtIndex:3] setBackgroundColor:kOrangeColor];
        [[[[[[[[tableView subviews] objectAtIndex:0] subviews] objectAtIndex:indexPath.row] contentView] subviews] objectAtIndex:5] setBackgroundColor:kOrangeColor];
    }
    
    NSMutableDictionary *dic_itemDetail=[[NSMutableDictionary alloc] init];
    [dic_itemDetail setObject:[(Category_ItemDetail*)[self.cartItems objectAtIndex:indexPath.row] specialInstruction] forKey:ksplInstr];
    [dic_itemDetail setObject:[(Category_ItemDetail*)[self.cartItems objectAtIndex:indexPath.row] modifier1] forKey:kmodifier1];
    [dic_itemDetail setObject:[(Category_ItemDetail*)[self.cartItems objectAtIndex:indexPath.row] modifier2] forKey:kmodifier2];
    [dic_itemDetail setObject:[(Category_ItemDetail*)[self.cartItems objectAtIndex:indexPath.row] modifier3] forKey:kmodifier3];
    [dic_itemDetail setObject:[(Category_ItemDetail*)[self.cartItems objectAtIndex:indexPath.row] modifier4] forKey:kmodifier4];
    [dic_itemDetail setObject:[(Category_ItemDetail*)[self.cartItems objectAtIndex:indexPath.row] managerID] forKey:kmanagerID];
    [dic_itemDetail setObject:[(Category_ItemDetail*)[self.cartItems objectAtIndex:indexPath.row] complimentry] forKey:kComplimentry];
    [dic_itemDetail setObject:[NSString stringWithFormat:@"%d",indexPath.row] forKey:@"row"];

    NSLog(@"dic_itemDetail %@",dic_itemDetail);
    NSString *notificationName = @"OrderItemUpdateNotification";
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dic_itemDetail];
    [dic_itemDetail release];
}
-(void) increaseQty:(id) sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.tblList1];
    NSIndexPath *clickedIndPth = [self.tblList1 indexPathForRowAtPoint:buttonPosition];
    
    UILabel *qtyLbl=(UILabel *)[[[[self.tblList1 cellForRowAtIndexPath:clickedIndPth] contentView] subviews] objectAtIndex:4];
    int qty=[[qtyLbl text] intValue];
    qty++;
    qtyLbl.text=[NSString stringWithFormat:@"%d",qty];
    UILabel *priceLbl=(UILabel *)[[[[self.tblList1 cellForRowAtIndexPath:clickedIndPth] contentView] subviews] objectAtIndex:6];

    priceLbl.text=[NSString stringWithFormat:@"$%.2f",([[(Category_ItemDetail*)[self.cartItems objectAtIndex:[sender tag]] price] floatValue] * qty)];
}
-(void) decreaseQty:(id) sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.tblList1];
    NSIndexPath *clickedIndPth = [self.tblList1 indexPathForRowAtPoint:buttonPosition];
    
    UILabel *qtyLbl=(UILabel *)[[[[self.tblList1 cellForRowAtIndexPath:clickedIndPth] contentView] subviews] objectAtIndex:4];
    int qty=[ qtyLbl.text intValue];
    if (qty>1) {
        qty--;
    }
    else
    {
        qty=1;
    }
    qtyLbl.text=[NSString stringWithFormat:@"%d",qty];
    UILabel *priceLbl=(UILabel *)[[[[self.tblList1 cellForRowAtIndexPath:clickedIndPth] contentView] subviews] objectAtIndex:6];
   priceLbl.text=[NSString stringWithFormat:@"$%.2f",([[(Category_ItemDetail*)[self.cartItems objectAtIndex:[sender tag]] price] floatValue] * qty)];
}

-(void)rowDeleteBtnClicked:(id)sender
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                    message:@"Are you sure want to delete it ?"
                                                   delegate:self
                                          cancelButtonTitle:@"NO"
                                          otherButtonTitles:@"YES", nil];
    [alert show];
    
    
     buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.tblList1];
       }

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // This method is invoked in response to the user's action. The altert view is about to disappear (or has been disappeard already - I am not sure)
    
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"NO"])
    {
        NSLog(@"Nothing to do here");
    }
    else if([title isEqualToString:@"YES"])
    {
        NSLog(@"Delete the cell");
        NSIndexPath *clickedIP = [self.tblList1 indexPathForRowAtPoint:buttonPosition];
        self.rowCount--;
        [self.tblList1 deleteRowsAtIndexPaths:[NSArray arrayWithObjects:clickedIP, nil] withRowAnimation:UITableViewRowAnimationFade];
        if (self.rowCount==0) {
            [self.tblList1 setSeparatorColor:[UIColor clearColor]];
        }
        NSMutableDictionary *dic_itemDetail=[[NSMutableDictionary alloc] init];
        [dic_itemDetail setObject:[NSString stringWithFormat:@"%d",clickedIP.row] forKey:@"row_deleted"];
        NSString *notificationName_delete = @"OrderItemDeleteNotification";
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationName_delete object:nil userInfo:dic_itemDetail];
        [dic_itemDetail release];
        

      
        }
}




@end
