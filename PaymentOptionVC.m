//
//  PaymentOptionVC.m
//  iROS
//
//  Created by iMac Apple on 18/12/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "PaymentOptionVC.h"
#import "iROSAppDelegate.h"
#import "ReciptInvoiceVC.h"
#import "iROS/iRestaurantsGlobalDataClass.h"

@interface PaymentOptionVC ()
{
#define kBaseUrlTsysSanbox @"http://stagegw.transnox.com:9300/servlets/TransNox_API_Server"
#define kBaseUrlTsysOriginal @"https://stagegw.transnox.com/servlets/TransNox_API_Server"
}

@end

@implementation PaymentOptionVC

@synthesize  webData, soapResults, xmlParser;


- (void)viewDidLoad {
    [super viewDidLoad];
  
    iROSAppDelegate *appDelegate = (iROSAppDelegate*)[[UIApplication sharedApplication] delegate];
    EncryptedSwipeData *esd = appDelegate.encryptedSwipeData;
    
    NSLog(@"ESD  :%@ \n %@ \n %@ \n%@ \n%@ \n%@",esd.track1Masked,
          esd.track2Encrypted,
          esd.CardLast4,
          esd.CardPANLength,
          esd.CapMagStripeEncryption,
          esd.CardStatus);
    
}

- (IBAction)backBtnClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)CardVarification:(id)sender
{
    /*
    btnActionType = 1;
    iROSAppDelegate *appDelegate = (iROSAppDelegate*)[[UIApplication sharedApplication] delegate];
    EncryptedSwipeData *esd = appDelegate.encryptedSwipeData;
    
    ServerConnectionClass* network = [[ServerConnectionClass alloc]init] ;
    self.connObject = network;
    self.connObject.delegate = self;
    
    NSString* ReqStr=[[NSString alloc]initWithFormat:@"{\"deviceID\":\"%@\" ,\"transactionKey\":\"%@\",\"cardDataSource\":\"%@\",\"currencyCode\":\"%@\",\"track1Data\":\"%@\" ,\"track2Data\":\"%@\" ,\"cardHolderName\":\"%@\",\"ksn\":\"%@\" ,\"tokenRequired\":\"%@\" ,\"firstName\":\"%@\" ,\"lastName\":\"%@\" }", @"66600000456701", @"39NFVH8G8EOYTFIR6X9PSH369Z38F8W1", @"SWIPE", @"INR", esd.track1Masked, esd.track2Masked ,esd.CardName, esd.ksn, @"Y", @"Harendra", @"Sharma"];
    
    NSString* jsonRequest=[[NSString alloc]initWithFormat:@"{\"CardAuthentication\":\%@ }",ReqStr];
    [self.connObject CallWebApiPostData:[NSURL URLWithString:kBaseUrlTsysOriginal] PayLoadStr:jsonRequest];
    */
    
}


-(IBAction)MakeSaleRequestSwipe:(id)sender
{
    
    btnActionType = 3;
    UIView * emailView = (UIView*)[self.view viewWithTag:100];
    [emailView setHidden:false];
    
    UITextField * emailTxt = (UITextField*)[emailView viewWithTag:101];
    
    iROSAppDelegate *appDelegate = (iROSAppDelegate*)[[UIApplication sharedApplication] delegate];
    EncryptedSwipeData *esd = appDelegate.encryptedSwipeData;
    
    ServerConnectionClass* network = [[ServerConnectionClass alloc]init] ;
    self.connObject = network;
    self.connObject.delegate = self;
    
    NSString  *Amt = [NSString stringWithFormat:@"%.2f",[iRestaurantsGlobalDataClass getInstance].amtToPay];
    
    NSString* ReqStr;
    if ([emailTxt.text isEqualToString:@""])
    {
         ReqStr=[[NSString alloc]initWithFormat:@"{\"deviceID\":\"%@\" ,\"transactionKey\":\"%@\",\"cardDataSource\":\"%@\",\"transactionAmount\":\"%@\",\"currencyCode\":\"%@\",\"track1Data\":\"%@\" ,\"track2Data\":\"%@\" ,\"ksn\":\"%@\" }", @"66600000456701", @"39NFVH8G8EOYTFIR6X9PSH369Z38F8W1", @"SWIPE", Amt, @"USD", esd.track1Masked, esd.track2Masked, esd.ksn];
    }
    else
    {
        ReqStr=[[NSString alloc]initWithFormat:@"{\"deviceID\":\"%@\" ,\"transactionKey\":\"%@\",\"cardDataSource\":\"%@\",\"transactionAmount\":\"%@\",\"currencyCode\":\"%@\",\"track1Data\":\"%@\" ,\"track2Data\":\"%@\" ,\"ksn\":\"%@\" , \"notifyEmailID\":\"%@\" }", @"66600000456701", @"39NFVH8G8EOYTFIR6X9PSH369Z38F8W1", @"SWIPE", Amt, @"INR", esd.track1Masked, esd.track2Masked, esd.ksn,[emailTxt text]];

    }
    
  /*  NSString* jsonRequest=[[NSString alloc]initWithFormat:@"{\"Sale\":\%@ }",ReqStr];
    [self.connObject CallWebApiPostData:[NSURL URLWithString:kBaseUrlTsysOriginal] PayLoadStr:jsonRequest];
   */
    
}


-(IBAction)ProcessCS10:(id)sender
{
    buttonClicked = 1;

    iROSAppDelegate *appDelegate = (iROSAppDelegate*)[[UIApplication sharedApplication] delegate];
    EncryptedSwipeData *esd = appDelegate.encryptedSwipeData;
    
 NSString *   soapMessage = [NSString stringWithFormat:
                             @"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                             "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                             "<soap:Body>\n"
                             "<ProcessCS10 xmlns=\"http://www.magensa.net/\">"
                             "<ProcessCS10_Input>"
                             "<TransactionType>A</TransactionType>"
                             "<HostID>MPH596289521</HostID>"
                             "<HostPW>M@dgTi3$a8tV7A</HostPW>"
                             "<MerchantID>MPM953468091</MerchantID>"
                             "<MerchantPW>Webt4mppg!T3$7</MerchantPW>"
                             "<EncryptedTrack1>%@</EncryptedTrack1>"
                             "<EncryptedTrack2>%@</EncryptedTrack2>"
                             "<EncryptedTrack3></EncryptedTrack3>"
                             "<EncryptedMP>%@</EncryptedMP>"
                             "<KSN>%@</KSN>"
                             "<MPStatus>%@</MPStatus>"
                             "<Amt>%.2f</Amt>"
                             "<TaxAmt></TaxAmt>"
                             "<PurchaseOrder></PurchaseOrder>"
                             "<InvoiceNumber></InvoiceNumber>"
                             "<CVV></CVV>"
                             "<ZIP></ZIP>"
                             "<AuthCode></AuthCode>"
                             "</ProcessCS10_Input>"
                             "</ProcessCS10>"
                             "</soap:Body>"
                             "</soap:Envelope>",esd.track1Encrypted,esd.track2Encrypted, esd.getMagnePrintData,esd.ksn, esd.getMagnePrintDataStatus,  [iRestaurantsGlobalDataClass getInstance].amtToPay];
    NSLog(@"Soap message - %@",soapMessage);
    message = [[NSString alloc]initWithString:soapMessage];
    
    
    NSURL *url = [NSURL URLWithString:@"https://mppg.magensa.net/MPPG/service.asmx"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%d", [soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://www.magensa.net/ProcessCS10" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if( theConnection )
    {
        webData = [[NSMutableData data] retain];
    }
    else
    {
        NSLog(@"theConnection is NULL");
    }
}


-(IBAction)ProcessTI10 :(id)sender
{
    buttonClicked = 2;
    
    NSString *   soapMessage = [NSString stringWithFormat:
                                @"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                                "<soap:Body>\n"
                                "<ProcessTI10 xmlns=\"http://www.magensa.net/\">"
                                "<ProcessTI10_Input>"
                                "<TransactionType>D</TransactionType>"
                                "<HostID>MPH596289521</HostID>"
                                "<HostPW>M@dgTi3$a8tV7A</HostPW>"
                                "<MerchantID>MPM953468091</MerchantID>"
                                "<MerchantPW>Webt4mppg!T3$7</MerchantPW>"
                                "<TransactionID>%@</TransactionID>"
                                "<Amt></Amt>"
                                "<Phone></Phone>"
                                "<PurchaseOrder></PurchaseOrder>"
                                "<InvoiceNumber></InvoiceNumber>"
                                "<FirstName></FirstName>"
                                "<LastName></LastName>"
                                "<BTName></BTName>"
                                "<Email></Email>"
                                "<CustomerAddress1></CustomerAddress1>"
                                "<CustomerAddress2></CustomerAddress2>"
                                "<CustomerCity></CustomerCity>"
                                "<CustomerState></CustomerState>"
                                "<CustomerZip></CustomerZip>"
                                "<Comments></Comments>"
                                "<TerminalName></TerminalName>"
                                "</ProcessTI10_Input>"
                                "</ProcessTI10>"
                                "</soap:Body>"
                                "</soap:Envelope>",[[iRestaurantsGlobalDataClass getInstance].DicProcessCS10 objectForKey:@"TransactionID"]];
    NSLog(@"Soap message - %@",soapMessage);
    message = [[NSString alloc]initWithString:soapMessage];
    
    
    NSURL *url = [NSURL URLWithString:@"https://mppg.magensa.net/MPPG/service.asmx"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%d", [soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://www.magensa.net/ProcessTI10" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if( theConnection )
    {
        webData = [[NSMutableData data] retain];
    }
    else
    {
        NSLog(@"theConnection is NULL");
    }
}


-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [webData setLength: 0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [webData appendData:data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERROR with theConenction");
    [connection release];
    [webData release];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"DONE. Received Bytes: %d", [webData length]);
  NSString *  theXMLResponse = [[NSString alloc] initWithBytes: [webData mutableBytes] length:[webData length] encoding:NSUTF8StringEncoding];
    
    Response = [[NSString alloc]initWithString:theXMLResponse];


    NSLog(theXMLResponse);
    [self sendMail];
    [theXMLResponse release];
    
    if( xmlParser )
    {
        [xmlParser release];
    }
    
    xmlParser = [[NSXMLParser alloc] initWithData: webData];
    [xmlParser setDelegate: self];
    [xmlParser setShouldResolveExternalEntities: YES];
    [xmlParser parse];
    
    [connection release];
    [webData release];
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *) namespaceURI qualifiedName:(NSString *)qName
   attributes: (NSDictionary *)attributeDict
{
    if( [elementName isEqualToString:@"TransactionID"])
    {
          if(!soapResults)
        {
            soapResults = [[NSMutableString alloc] init];
        }
//        recordResults = TRUE;
    }
}
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
//    if( recordResults )
//    {
        soapResults = [[NSMutableString alloc] init];

        [soapResults appendString: string];
//    }
}
-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    switch (buttonClicked) {
        case 1:
            if( [elementName isEqualToString:@"TransactionID"])
            {
                [[iRestaurantsGlobalDataClass getInstance].DicProcessCS10 setObject:soapResults forKey:@"TransactionID"];
            }
            
            else  if( [elementName isEqualToString:@"TransactionStatus"])
            {
                [[iRestaurantsGlobalDataClass getInstance].DicProcessCS10 setObject:soapResults forKey:@"TransactionStatus"];
            }
            else  if( [elementName isEqualToString:@"StatusCode"])
            {
                [[iRestaurantsGlobalDataClass getInstance].DicProcessCS10 setObject:soapResults forKey:@"StatusCode"];
            }
            else  if( [elementName isEqualToString:@"StatusMsg"])
            {
                [[iRestaurantsGlobalDataClass getInstance].DicProcessCS10 setObject:soapResults forKey:@"StatusMsg"];
            }
            NSLog(@"Trans ProcessCS10 Dic %@ ",[iRestaurantsGlobalDataClass getInstance].DicProcessCS10);
            break;
            
        case 2:
            break;
        default:
            break;
    }


}


/*
#pragma mark- MyCabuUserNetwork delegate methods

- (void) connectionEndWithError:(NSError*)error
{
    [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [self removeConnectionClassObject];
}

- (void) connectionEndedSuccefullyWithData:(NSData*)data
{
    NSError *localError = nil;
    RecieptdataResponseDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    NSLog(@"MY dictionary====%@",RecieptdataResponseDic);
    if (RecieptdataResponseDic != nil)
    {
        [self performSegueWithIdentifier:@"InvoiceReciept" sender:self]; // done by harry

    }
    else
    {
        [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    
    [self removeConnectionClassObject];
}

- (void) removeConnectionClassObject
{
    self.connObject.delegate = nil;
    self.connObject = nil;
}

*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ReciptInvoiceVC *vc = [segue destinationViewController];
    vc.ReciptDataDic  = RecieptdataResponseDic;
    vc.btnActionType = btnActionType;
}

-(void)sendMail
{
    NSString * msg = [NSString stringWithFormat:@"Request message - %@\n\n\n\n Response-  %@",message, Response];
    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setSubject:@"Megensa Request/Response"];
    [mailComposer setMessageBody:msg isHTML:NO];
    [self presentViewController:mailComposer animated:NO completion:nil];
     }
     
#pragma mark - mail compose delegate
     -(void)mailComposeController:(MFMailComposeViewController *)controller
             didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
                 if (result) {
                     NSLog(@"Result : %d",result);
                 }
                 if (error) {
                     NSLog(@"Error : %@",error);
                 }
         [self dismissViewControllerAnimated:NO completion:nil];
         
             }
@end
