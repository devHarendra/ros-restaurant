//
//  MarkAttendanceClass.h
//  iROS
//
//  Created by Dex on 15/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarkAttendanceClass : UIView<ServerConnectionDelegate>
{
    UITextField *fareFld;
    int EntryType;
}
@property (nonatomic, retain) UIView *bgVw;
@property (nonatomic, retain) UIButton *clockInBtn;
@property (nonatomic, retain) UIButton *clockOutBtn;

@property (nonatomic, retain) UIButton* closeBtn;
@property (retain, nonatomic) ServerConnectionClass*  connObject;

@end
