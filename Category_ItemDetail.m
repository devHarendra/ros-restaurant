//
//  Category_ItemDetail.m
//  iROS
//
//  Created by Dex on 29/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "Category_ItemDetail.h"

@implementation Category_ItemDetail
@synthesize categoryID,itemID,complimentry,managerID,modifier1,modifier2,modifier3,modifier4,price,quantity,specialInstruction,itemName,imgVw,itemType;

- (id) init
{
    self = [super init];
    
    if (self) {
        
        itemID=nil;
        complimentry=@"";
        managerID=nil;
        modifier1=nil;
        modifier2=nil;
        modifier3=nil;
        modifier4=nil;
        price=nil;
        quantity=nil;
        specialInstruction=@"";
        categoryID=nil;
        itemName=nil;
        imgVw=nil;
        itemType=@"";
    }
    
    return self;
}
    
@end
