//
//  Category.h
//  iROS
//
//  Created by Dex on 25/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Category_menu : NSObject

@property (nonatomic, strong) NSString *categoryID;
@property (nonatomic, strong) NSString *categoryName;
@end

