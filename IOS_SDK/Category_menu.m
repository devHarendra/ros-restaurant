//
//  Category.m
//  iROS
//
//  Created by Dex on 25/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "Category_menu.h"

@implementation Category_menu
@synthesize categoryID,categoryName;



- (id) init
{
self = [super init];

if (self) {
    
    categoryName=nil;
    categoryID=nil;
    
}

return self;
}
@end
