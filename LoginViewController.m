//
//  LoginViewController.m
//  iROS
//
//  Created by Dex on 31/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"

@interface LoginViewController ()
{
     UIView *bgVw_login;
    UIImageView *mainbg_img;
    UITextField *paswrdFld;
    UITextField *usernameFld;
}
@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    UIView *bgVw=[[UIView alloc] init];
    bgVw.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [bgVw setBackgroundColor:kOrangeColor];
    [self.view addSubview:bgVw];
    [bgVw release];
    
    mainbg_img = [[UIImageView alloc] init];
    mainbg_img.frame=CGRectMake((self.view.frame.size.width/2)-100, 150, 200, 95);
    mainbg_img.userInteractionEnabled=TRUE;
    mainbg_img.opaque = YES;
    [mainbg_img setImage:[UIImage imageNamed:@"logo2.png"]];
    [bgVw addSubview:mainbg_img];
    [mainbg_img release];
    
    bgVw_login=[[UIView alloc] init];
    bgVw_login.frame=CGRectMake((self.view.frame.size.width/2)-260, (self.view.frame.size.height/2)-100, 520, 300);
    [bgVw_login setBackgroundColor:kLightOrangeColor];
    [self.view addSubview:bgVw_login];
    [bgVw_login release];
    
    usernameFld=[[UITextField alloc] init];
    [usernameFld setFrame:CGRectMake(20,40,480, 60)];
    usernameFld.placeholder=@"Username";
    usernameFld.text=@"shruti.mittal@search-value.com";
    usernameFld.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    usernameFld.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
    [usernameFld setBackgroundColor:[UIColor whiteColor]];
    [usernameFld setTextColor:[UIColor darkGrayColor]];
    [usernameFld setTag:10];
    [usernameFld setDelegate:self];
    [bgVw_login addSubview:usernameFld];
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 30)];// view added for padding
    usernameFld.leftView = paddingView3;
    [paddingView3 release];
    usernameFld.leftViewMode = UITextFieldViewModeAlways;
    [usernameFld release];
    [usernameFld setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    
    paswrdFld=[[UITextField alloc] init];
    [paswrdFld setFrame:CGRectMake(20,110,480, 60)];
    paswrdFld.placeholder=@"Password";
    paswrdFld.text=@"qwerty";
    paswrdFld.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    paswrdFld.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
    [paswrdFld setBackgroundColor:[UIColor whiteColor]];
    [paswrdFld setTextColor:[UIColor darkGrayColor]];
    [bgVw_login addSubview:paswrdFld];
    [paswrdFld setDelegate:self];
    [paswrdFld setSecureTextEntry:YES];
    [paswrdFld setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 30)];// view added for padding
    paswrdFld.leftView = paddingView4;
    [paddingView4 release];
    paswrdFld.leftViewMode = UITextFieldViewModeAlways;
    [paswrdFld release];
   [paswrdFld setTag:20];
    
    UIButton *submitResponseBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [submitResponseBtn setFrame:CGRectMake(20, 200, 480, 50)];
    [submitResponseBtn setTitle:@"Login" forState:UIControlStateNormal];
    [submitResponseBtn setBackgroundColor:[UIColor whiteColor]];
    [submitResponseBtn setTitleColor:kOrangeColor forState:UIControlStateNormal];
    [submitResponseBtn setExclusiveTouch:YES];
    [bgVw_login addSubview:submitResponseBtn];
    [submitResponseBtn addTarget:self action:@selector(submitResponse) forControlEvents:UIControlEventTouchUpInside];
    

    [super viewDidLoad];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





-(void)textFieldDidBeginEditing:(UITextField *)textField
{
   
    [bgVw_login setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.height-520)/2, ([UIScreen mainScreen].bounds.size.width-300)/2-100,520 , 300)];
    mainbg_img.frame=CGRectMake((self.view.frame.size.width/2)-100, 30, 200, 95);
       
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [bgVw_login setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.height-520)/2, (self.view.frame.size.height/2)-100,520 , 300)];
    mainbg_img.frame=CGRectMake((self.view.frame.size.width/2)-100, 150, 200, 95);
    
}

- (void) submitResponse
{
    if ([usernameFld.text length] == 0)
    {
        [Utils showAlertView:@"Alert" message:@"Please enter username." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    else if ([paswrdFld.text length] == 0)
    {
        [Utils showAlertView:@"Alert" message:@"Please enter your Password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    else if (![Utils connected])
    {
        [Utils showAlertView:@"Network Error" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    else
    {
        
        
        ServerConnectionClass* network = [[ServerConnectionClass alloc]init] ;
        self.connObject = network;
        [network release];
        self.connObject.delegate = self;
        NSString* urlString=[[NSString alloc]initWithFormat:@"%@{\"user_login\":\"%@\",\"user_pass\":\"%@\",\"device_token\":\"%@\",\"device_id\":\"%@\",\"mobile_os\":\"%@\" }",kLoginUrl,usernameFld.text,paswrdFld.text,[Utils valueForKey:kDeviceToken], [Utils deviceid],kMobileOs];
        [self.connObject makeRequestForUrl:urlString];
        [urlString release];
    }
}





- (void) removeConnectionClassObject
{
    self.connObject.delegate = nil;
    self.connObject = nil;
}
#pragma mark- MyCabuUserNetwork delegate methods

- (void) connectionEndWithError:(NSError*)error
{
    [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [self removeConnectionClassObject];
}

- (void) connectionEndedSuccefullyWithData:(NSData*)data
{
    NSError *localError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    NSLog(@"MY dictionary====%@",dictionary);
    if (dictionary != nil)
    {
        NSString* result = [dictionary objectForKey:@"result"];
        //
        
        if ([result isEqualToString:@"Success"])
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:@"Yes" forKey:kUserLoginStatus];
            [defaults synchronize];
            [defaults setObject:[[dictionary objectForKey:@"data"] objectForKey:@"user_id"]        forKey:kuserId];
            [defaults setObject:[[dictionary objectForKey:@"data"] objectForKey:@"user_type"]      forKey:kuserType];
            
            HomeViewController *newVC=[[HomeViewController alloc] init];
            [self.navigationController pushViewController:newVC animated:YES];
            [newVC release];
           
        }
        else
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:@"No" forKey:kUserLoginStatus];
            [defaults synchronize];
            
            [Utils showAlertView:((result) ? result : @"")  message:(([dictionary objectForKey:@"msg"]) ? [dictionary objectForKey:@"msg"] : @"Login failed.") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        }
    }
    else
    {
        [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    
    [self removeConnectionClassObject];
}


@end
