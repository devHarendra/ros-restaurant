//
//  LayoutViewController.m
//  Restaurant Menu
//
//  Created by Dex on 16/07/14.
//  Copyright (c) 2014 Dex. All rights reserved.
//
#import "HomeViewController.h"
#import "LayoutViewController.h"
#import "MenuViewController.h"
#import "SwipeViewController.h"

@interface LayoutViewController ()
{
}
@end

@implementation LayoutViewController
@synthesize userType;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)refreshView:(NSNotification *) notification {
    
    
    NSLog(@"refreshingggggg");
   
    userType=[[notification userInfo] objectForKey:kuserType];
    
    [self viewDidLoad];
    
}
-(void)viewDidAppear:(BOOL)animated
{
//    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//    userType=[defaults objectForKey:kuserType];
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshView:)
                                                 name:@"refreshView" object:nil];
    [super viewDidAppear:YES];
    
}

    
    
   
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];

    
    UIView *sectionView=[[UIView alloc] init];
    sectionView.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 70);
    

    if ([userType isEqualToString:@"Bar Tender"]) {
        [sectionView setBackgroundColor:kDarkBrownColor];
    }
    else
    {
        [sectionView setBackgroundColor:kOrangeColor];
    }
    [self.view  addSubview:sectionView];
    [sectionView release];
    
    UIImageView *layout_img = [[UIImageView alloc] init];
    layout_img.frame=CGRectMake(30, 25, 40, 40);
    layout_img.userInteractionEnabled=TRUE;
    layout_img.opaque = YES;
    [layout_img setImage:[UIImage imageNamed:@"layout_icon.png"]];
    [sectionView addSubview:layout_img];
    [layout_img release];
    
    UILabel *userTypeLbl=[[UILabel alloc] init];
    userTypeLbl.frame=CGRectMake(80, 20, 350, 50);
    [userTypeLbl setText:@"Table Layout"];
    [userTypeLbl setTextColor:[UIColor whiteColor]];
    [userTypeLbl setTextAlignment:NSTextAlignmentLeft];
    [userTypeLbl setBackgroundColor:[UIColor clearColor]];
    [sectionView addSubview:userTypeLbl];
    [userTypeLbl release];
    
    UIView *footerVw=[[UIView alloc] init];
    footerVw.frame=CGRectMake(0, 699, [UIScreen mainScreen].bounds.size.width, 100);
    if ([userType isEqualToString:@"Bar Tender"]) {
        [footerVw setBackgroundColor:kDarkBrownColor];
    }
    else
    {
        [footerVw setBackgroundColor:kOrangeColor];
    }
    //[footerVw setBackgroundColor:kOrangeColor];
    [self.view  addSubview:footerVw];
    [footerVw release];
    
    UIButton * newOrderBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [newOrderBtn setFrame:CGRectMake(30, 15, 120, 40)];
    [newOrderBtn setBackgroundColor:[UIColor whiteColor]];
    [newOrderBtn setTitle:@"Back" forState:UIControlStateNormal];
    [newOrderBtn.titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
    [newOrderBtn addTarget:self action:@selector(btnBack:) forControlEvents:UIControlEventTouchUpInside];
    [footerVw addSubview:newOrderBtn];
    if ([userType isEqualToString:@"Bar Tender"]) {
        [newOrderBtn setTitleColor:kDarkBrownColor forState:UIControlStateNormal];
    }
    else
    {
        [newOrderBtn setTitleColor:kOrangeColor forState:UIControlStateNormal];
    }
    
    UILabel *poweredLbl=[[UILabel alloc] init];
    poweredLbl.frame=CGRectMake(850, 35, 190, 40);
    [poweredLbl setText:@"Powered By:Webtye"];
    [poweredLbl setTextAlignment:NSTextAlignmentCenter];
    [poweredLbl setBackgroundColor:[UIColor clearColor]];
    [poweredLbl setFont:[UIFont systemFontOfSize:12.0f]];
    [poweredLbl setTextColor:[UIColor whiteColor]];
    [footerVw addSubview:poweredLbl];
    [poweredLbl release];
    
    [scrollView_layout removeFromSuperview];
    scrollView_layout = [[UIScrollView alloc] init];
    scrollView_layout.frame=CGRectMake(0, 70, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-160);
    scrollView_layout.backgroundColor = [UIColor clearColor];
    scrollView_layout.showsHorizontalScrollIndicator = NO;
    [scrollView_layout setCanCancelContentTouches:NO];
    scrollView_layout.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView_layout.clipsToBounds = YES;
    scrollView_layout.scrollEnabled = YES;
    scrollView_layout.pagingEnabled=YES;
    scrollView_layout.delegate=self;
    scrollView_layout.tag=222;
    scrollView_layout.contentSize = CGSizeMake(300, 200);
    [self.view addSubview:scrollView_layout];
    [scrollView_layout release];
    
    scrollView_layout.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.height, scrollView_layout.frame.size.height*3);
    int scrolViewHeight=scrollView_layout.frame.size.height;
    int scrolViewWidth=scrollView_layout.frame.size.width;
    
    int row,column,rowCount=10;
    
    iRestaurantsGlobalDataClass *obj=[iRestaurantsGlobalDataClass getInstance];
    for (row=0; row<rowCount; row++)
    {
        for (column=0; column<5; column ++)
        {
            NSLog(@"COL: %d, ROW: %d",column,row);
       
            UIButton *tableBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            UILabel *tableNumLbl = [[UILabel alloc]init];
            [tableNumLbl setBackgroundColor:[UIColor clearColor]];
            [tableNumLbl setTextAlignment:NSTextAlignmentCenter];
           
            if ([userType isEqualToString:@"Bar Tender"])
            {
                [tableBtn setFrame:CGRectMake((column *(scrolViewWidth/5))+40, 10+(row*(scrolViewHeight/4))+10, 140, 140)];
                [tableBtn setImage:[UIImage imageNamed:@"table_bar.png"] forState:UIControlStateNormal];
                [tableBtn setImage:[UIImage imageNamed:@"table_active_bar.png"] forState:UIControlStateSelected];
                [tableBtn setTag:[[NSString stringWithFormat:@"%d%d",row,column] integerValue]];
                if ([obj.tableBookedArr_bar containsObject:[NSString stringWithFormat:@"%d",tableBtn.tag+1]])
                {
                    [tableBtn setSelected:YES];
                }
                
                [tableNumLbl setFrame:CGRectMake(tableBtn.frame.origin.x-2, tableBtn.frame.origin.y, tableBtn.frame.size.width, 120)];
                [tableNumLbl setText:[NSString stringWithFormat:@"%d",tableBtn.tag+1]];
                
            }
            else
            {
                 [tableBtn setFrame:CGRectMake((column *(scrolViewWidth/5))+40, 10+(row*(scrolViewHeight/4))+10, 123.5, 135)];
                [tableBtn setImage:[UIImage imageNamed:@"table.png"] forState:UIControlStateNormal];
                [tableBtn setImage:[UIImage imageNamed:@"table_active.png"] forState:UIControlStateSelected];
                [tableBtn setTag:[[NSString stringWithFormat:@"%d%d",row,column] integerValue]];
                if ([obj.tableBookedArr_food containsObject:[NSString stringWithFormat:@"%d",tableBtn.tag+1]]) {
                    [tableBtn setSelected:YES];
                }
                [tableNumLbl setFrame:CGRectMake(tableBtn.frame.origin.x-2, tableBtn.frame.origin.y, tableBtn.frame.size.width, 112)];
                [tableNumLbl setText:[NSString stringWithFormat:@"%d",tableBtn.tag+1]];
            }
            [tableBtn addTarget:self action:@selector(selectTable:) forControlEvents:UIControlEventTouchUpInside];
            [tableBtn setBackgroundColor:[UIColor clearColor]];
            [scrollView_layout addSubview:tableBtn];
            [tableBtn setExclusiveTouch:YES];
            [scrollView_layout addSubview:tableNumLbl];
        }
    }
}

-(void) selectTable:(id)sender
{
    if ([sender isSelected])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Booked" message:@"You cannot allot this table .It is already booked" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] ;
        [alert show];
        [alert release];
    }
    else
    {
        if ([userType isEqualToString:@"Bar Tender"])
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Open Tab?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil] ;
             [alert setTag:10];
            [alert show];
            [alert release];
        }
        else
        {
            [sender setSelected:YES];
             [self ShowPartyInfoPopup:self];
            
//             [sender setSelected:YES];   //
//            MenuViewController *menuVC=[[MenuViewController alloc] init];
            tableIdfrom =[sender tag]+1;
            NSLog(@"%d",tableIdfrom);
//            [menuVC setFromClass:@"LayoutClass"];
//            [self.navigationController pushViewController:menuVC animated:YES];
//            [menuVC release];
        }
      
    }
}


-(void)EmptyTableClicked
{
    
    if (0) {
          UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Number of People in Party" message:@"10" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert setTag:40];
    [alert show];
    [alert release];
    }
}

-(void)GoTomenuLayout
{
    MenuViewController *menuVC=[[MenuViewController alloc] init];
    [menuVC setTableID:[NSString stringWithFormat:@"%d",tableIdfrom]]; // tag is alredy seting up earlier method
    [menuVC setFromClass:@"LayoutClass"];
    [self.navigationController pushViewController:menuVC animated:YES];
    [menuVC release];

}


-(void)btnBack:(id)sender
{
    for (UIViewController *controller in self.navigationController.viewControllers)
    {
        
        
        if ([controller isKindOfClass:[HomeViewController class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            break;
            
        }
    }

}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==10) {
        if (buttonIndex==0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Which mode:" delegate:self cancelButtonTitle:@"Prepaid" otherButtonTitles:@"Postpaid", nil];
            
            [alertView setTag:20];
            [alertView show];
            [alertView release];
        }
    }
    else if (alertView.tag==20)
    {
         if (buttonIndex==0)
         {
             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter Amount:" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
             [alertView setTag:30];
             [alertView show];
             [alertView release];
         }
        else
        {
            //todo:Read credit card and save it on server
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"iPad" bundle:nil];
            
           
            UIViewController *controller = (UIViewController *)
            [storyboard instantiateViewControllerWithIdentifier:@"1"];
            
            [self.navigationController  pushViewController:controller animated:YES ];
          
        }
    }
    else if (alertView.tag==30)
    {
        //todo: Read credit card and save it on server
    }
    
    else if (alertView.tag==40)
    {
         if (buttonIndex==0) {
             [self GoTomenuLayout];
         }
    }

}


- (void)ShowPartyInfoPopup:(id)sender
{
    if (!_uiasView_partyDetailClass) {
        _uiasView_partyDetailClass = [[partyDetailClass alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height , self.view.frame.size.width, self.view.frame.size.height)];
        [_uiasView_partyDetailClass.closeBtn addTarget:self action:@selector(ClosePartyInfoPopup:) forControlEvents:UIControlEventTouchDragInside];
         [_uiasView_partyDetailClass.DoneBtn addTarget:self action:@selector(GoTomenuLayout) forControlEvents:UIControlEventTouchDragInside];
           }
    [self.view addSubview:_uiasView_partyDetailClass];
    [UIView animateWithDuration:0.60f animations:^
    {
              CGRect uiasViewFrame   = _uiasView_partyDetailClass.frame;
        uiasViewFrame.origin.y =0;
        _uiasView_partyDetailClass.frame = uiasViewFrame;
        [self.view addSubview:_uiasView_partyDetailClass];
        
        
    } completion:^(BOOL finished) {
        
        
    }];
 
}

- (void)ClosePartyInfoPopup:(id)sender
{
    [UIView animateWithDuration:0.75f animations:^{
        
        
        CGRect uiasViewFrame   = _uiasView_partyDetailClass.frame;
        uiasViewFrame.origin.y = self.view.frame.size.height ;
        _uiasView_partyDetailClass.frame = uiasViewFrame;
    
    } completion:^(BOOL finished)
     {
         // completion block is important in each case. Use it
         [_uiasView_partyDetailClass removeFromSuperview];
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

@end
