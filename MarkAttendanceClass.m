//
//  MarkAttendanceClass.m
//  iROS
//
//  Created by Dex on 15/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "MarkAttendanceClass.h"
#import "Define.h"

@implementation MarkAttendanceClass

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:0.8];
        
        
        UIView *bgVw=[[UIView alloc] init];
        self.bgVw=bgVw;
        [bgVw release];
        [self.bgVw setBackgroundColor:[UIColor whiteColor]];
        [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-500)/2, ([UIScreen mainScreen].bounds.size.height-560)/2,500 , 535)];
        [self addSubview:self.bgVw];
        [self.bgVw.layer setBorderColor:[UIColor colorWithRed:77.0f/255.0f green:77.0f/255.0f blue:77.0f/255.0f alpha:1].CGColor];
        [self.bgVw.layer setBorderWidth:6.0f];
        [self.bgVw.layer setCornerRadius:4.0f];
        
        
        
        
        self.closeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeBtn setFrame:CGRectMake( self.bgVw.frame.size.width-60,1, 60, 60)];
        [self.closeBtn setBackgroundColor:[UIColor clearColor]];
        [self.closeBtn setExclusiveTouch:YES];
//        [self.closeBtn setHidden:YES];  // to prevent closing without entering emp id
        [self.bgVw addSubview:self.closeBtn];
        [self.closeBtn setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        
        
        UILabel *popupHeader=[[UILabel alloc] init];
        [popupHeader setFrame:CGRectMake(0, 0, self.bgVw.frame.size.width, 60)];
        [popupHeader setBackgroundColor:[UIColor clearColor]];
        [popupHeader setTextAlignment:NSTextAlignmentCenter];
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
             [popupHeader setTextColor:kDarkBrownColor];
        }
        else
        {
             [popupHeader setTextColor:kOrangeColor];
        }
       
        [popupHeader setText:@"Attendance"];
        [popupHeader setFont:[UIFont systemFontOfSize:25]];
        [self.bgVw addSubview:popupHeader];
        [popupHeader release];
        
        UILabel *popupHeaderlbl=[[UILabel alloc] init];
        [popupHeaderlbl setFrame:CGRectMake(30, popupHeader.frame.origin.y+50, self.bgVw.frame.size.width-60, 40)];
        [popupHeaderlbl setBackgroundColor:[UIColor clearColor]];
        [popupHeaderlbl setTextAlignment:NSTextAlignmentLeft];
        [popupHeaderlbl setTextColor:[UIColor darkGrayColor]];
        [popupHeaderlbl setText:@"Please mark your attendance"];
        [popupHeaderlbl setFont:[UIFont systemFontOfSize:18]];
        [self.bgVw addSubview:popupHeaderlbl];
        [popupHeaderlbl release];
        
        UIView *viewLine=[[UIView alloc] init];
        [viewLine setBackgroundColor:[UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1]];
        [viewLine setFrame:CGRectMake(30, popupHeaderlbl.frame.origin.y+40, self.bgVw.frame.size.width-60, 1)];
        [self.bgVw addSubview:viewLine];
        [viewLine release];
        
        
        UILabel *employeeIDlbl=[[UILabel alloc] init];
        [employeeIDlbl setFrame:CGRectMake(30, viewLine.frame.origin.y+18, 160, 40)];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
              [employeeIDlbl setBackgroundColor:kDarkBrownColor];
        }
        else
        {
              [employeeIDlbl setBackgroundColor:kOrangeColor];
        }

      
        [employeeIDlbl setTextAlignment:NSTextAlignmentCenter];
        [employeeIDlbl setTextColor:[UIColor whiteColor]];
        [employeeIDlbl setText:@"Employee ID"];
        [employeeIDlbl setFont:[UIFont systemFontOfSize:18]];
        [self.bgVw addSubview:employeeIDlbl];
        [employeeIDlbl release];
        
        fareFld=[[UITextField alloc] init];
        [fareFld setFrame:CGRectMake(192,viewLine.frame.origin.y+18,278, 40)];
        fareFld.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
        fareFld.keyboardType = UIKeyboardTypeNumberPad;
        fareFld.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
        [fareFld setBackgroundColor:[UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1]];
        [fareFld setTextColor:[UIColor darkGrayColor]];
        [self.bgVw addSubview:fareFld];
        UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 30)];// view added for padding
        fareFld.leftView = paddingView3;
        [fareFld setUserInteractionEnabled:false]; // prevent apparing of default keyboard.
        [paddingView3 release];
        fareFld.leftViewMode = UITextFieldViewModeAlways;
        [fareFld release];
        
        self.clockInBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.clockInBtn setFrame:CGRectMake(150, fareFld.frame.origin.y+55, 210, 50)];
        [self.clockInBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, -5.0f, 0.0f, 0.0f)];
        [self.clockInBtn setTitle:@"Entry" forState:UIControlStateNormal];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
             [self.clockInBtn setBackgroundColor:kDarkBrownColor];
        }
        else
        {
             [self.clockInBtn setBackgroundColor:kOrangeColor];
        }

        [self.clockInBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.clockInBtn setExclusiveTouch:YES];
        [self.clockInBtn setSelected:NO];
        [self.bgVw addSubview:self.clockInBtn];

        UIImageView *imgvw2=[[UIImageView alloc] init];
        [imgvw2 setFrame:CGRectMake(-17, -23, 100, 100)];
        [imgvw2 setImage:[UIImage imageNamed:@"radio_off"]];
        [self.clockInBtn addSubview:imgvw2];
        [imgvw2 release];
        
        self.clockOutBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.clockOutBtn setFrame:_clockInBtn.frame];
        [self.clockOutBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, -5.0f, 0.0f, -10.0f)];
        [self.clockOutBtn setTitle:@"Exit" forState:UIControlStateNormal];
        [self.clockOutBtn setBackgroundColor:[UIColor darkGrayColor]];
        [self.clockOutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.clockOutBtn setExclusiveTouch:YES];
        [self.clockOutBtn setSelected:NO];
        [self.bgVw addSubview:self.clockOutBtn];
        UIImageView *imgvw3=[[UIImageView alloc] init];
        [imgvw3 setFrame:CGRectMake(-17, -23, 100, 100)];
        [imgvw3 setImage:[UIImage imageNamed:@"radio_off"]];
        [self.clockOutBtn addSubview:imgvw3];
        [imgvw3 release];
        
        UIButton *submitResponseBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [submitResponseBtn setFrame:CGRectMake((self.bgVw.frame.size.width-100)/2, self.clockOutBtn.frame.origin.y+70, 100, 40)];
        [submitResponseBtn setTitle:@"Submit" forState:UIControlStateNormal];
        [submitResponseBtn setBackgroundColor:[UIColor clearColor]];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
             [submitResponseBtn setTitleColor:kDarkBrownColor forState:UIControlStateNormal];
             [submitResponseBtn.layer setBorderColor:kDarkBrownColor.CGColor];
        }
        else
        {
             [submitResponseBtn.layer setBorderColor:kOrangeColor.CGColor];
             [submitResponseBtn setTitleColor:kOrangeColor forState:UIControlStateNormal];
        }
       
        [submitResponseBtn setExclusiveTouch:YES];
        [submitResponseBtn setHidden:YES]; // hidden, i do want romeve it .
       
        [submitResponseBtn.layer setBorderWidth:1.0f];
        [self.bgVw addSubview:submitResponseBtn];
        [submitResponseBtn addTarget:self action:@selector(submitResponse) forControlEvents:UIControlEventTouchUpInside];
       
        [self.clockOutBtn addTarget:self action:@selector(exitClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.clockInBtn addTarget:self action:@selector(entryClicked:) forControlEvents:UIControlEventTouchUpInside];

        UIView *grayV = [[UIView alloc]initWithFrame:CGRectMake(0, 225.0, _bgVw.frame.size.width, 305.0)];
        [_bgVw addSubview:grayV];
        [grayV setBackgroundColor:[UIColor lightGrayColor]];
        
        UIButton *keyBtn;
        int tag = 0;
        float x = 1.0*1 ; float y = 229.0;

        for (int i = 1; i<=3; i++)  // creting buttons 1 to 9 digit
        {
            for (int j = 1; j<=3; j++)
            {
                tag++;
                keyBtn = [[UIButton alloc]initWithFrame:CGRectMake(x, y,162.0 , 70)];
                [keyBtn setBackgroundColor:[UIColor whiteColor]];
                [keyBtn setTitle:[NSString stringWithFormat:@"%d",tag] forState:UIControlStateNormal];
                [keyBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                keyBtn.titleLabel.font  = [UIFont systemFontOfSize:26.0];
                [keyBtn addTarget:self action:@selector(SetCustomKeyValues:) forControlEvents:UIControlEventTouchUpInside];
                [_bgVw addSubview:keyBtn];
                x=x+1.5+165.5; // 1.5 spacing b'/w btns
            }
            y=y+75.0;
            x = 0.0*1+1;
        }
        
        for (int j = 1; j<=3; j++) // creting buttons after 9 digit
        {
            tag++;
            keyBtn = [[UIButton alloc]initWithFrame:CGRectMake(x, y,162.0 , 70)];
            [keyBtn setBackgroundColor:[UIColor whiteColor]];
            
            if (tag == 10) {
                [keyBtn setTitle:@"0" forState:UIControlStateNormal];
                [keyBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                [keyBtn addTarget:self action:@selector(SetCustomKeyValues:) forControlEvents:UIControlEventTouchUpInside];

                keyBtn.titleLabel.font  = [UIFont systemFontOfSize:26.0];
            }
            else if (tag == 11)
            {
                [keyBtn setTitle:@"Login"forState:UIControlStateNormal];
                keyBtn.titleLabel.font  = [UIFont systemFontOfSize:26.0];
                [keyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [keyBtn setBackgroundColor:[UIColor colorWithRed:47.0/255.0 green:103.0/255.0 blue:156.0/255.0 alpha:1.0]];
                  [keyBtn addTarget:self action:@selector(LoginClicked) forControlEvents:UIControlEventTouchUpInside];
            }
            else
            {
                [keyBtn setBackgroundImage:[UIImage imageNamed:@"backspace.png"] forState:UIControlStateNormal];
                [keyBtn addTarget:self action:@selector(BackSpacePressed:) forControlEvents:UIControlEventTouchUpInside];
            }
            [_bgVw addSubview:keyBtn];
            x=x+1.5+165.5; // 1.5 spacing b'/w btns
        }
    }
    return self;
}

-(void)LoginClicked
{
    NSString *oStr = [fareFld text];
    if ([oStr length]<=0)
    {
     [Utils showAlertView:@"Alert" message:@"Emp-Id cannot be blank" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    else if (EntryType == 0)
    {
        [Utils showAlertView:@"Alert" message:@"Please select Entry or Exit Type" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    
    else
    {
//    [UIView animateWithDuration:0.75f animations:^{
//        
//        
//        CGRect uiasViewFrame   = self.frame;
//        uiasViewFrame.origin.y = self.frame.size.height ;
//        
//        self.frame = uiasViewFrame;
//        
        [self MarkAttandance];
//
//    } completion:^(BOOL finished)
//     {
//         // completion block is important in each case. Use it
//         [fareFld setText:@""];
//         [self removeFromSuperview];
//     }];
}
}
-(void)entryClicked:(id)sender
{
    UIImageView *imgvw=(UIImageView*)[[sender subviews] objectAtIndex:0];
    EntryType = 1;
    if (![sender isSelected])
    {
        [sender setSelected:YES];
        [imgvw setImage:[UIImage imageNamed:@"radio_on"]];
        
        [self.clockOutBtn setSelected:NO];
        UIImageView *imgvw_split=(UIImageView*)[[self.clockOutBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_off"]];
        
    }
    else
    {
        [sender setSelected:NO];
        [imgvw setImage:[UIImage imageNamed:@"radio_off"]];
        
        [self.clockOutBtn setSelected:YES];
        UIImageView *imgvw_split=(UIImageView*)[[self.clockOutBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_on"]];
        
    }
}
-(void)exitClicked:(id)sender
{
    UIImageView *imgvw=(UIImageView*)[[sender subviews] objectAtIndex:0];
    EntryType = 2;

    if (![sender isSelected])
    {
        [sender setSelected:YES];
        [imgvw setImage:[UIImage imageNamed:@"radio_on"]];
        
        [self.clockInBtn setSelected:NO];
        UIImageView *imgvw_split=(UIImageView*)[[self.clockInBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_off"]];
        
    }
    else
    {
        [sender setSelected:NO];
        [imgvw setImage:[UIImage imageNamed:@"radio_off"]];
        
        [self.clockInBtn setSelected:YES];
        UIImageView *imgvw_split=(UIImageView*)[[self.clockInBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_on"]];
        
    }

}

-(IBAction)SetCustomKeyValues:(UIButton*)Btn
{
    [fareFld setText:[NSString stringWithFormat:@"%@%@",[fareFld text], [Btn.titleLabel text]]];
}

-(IBAction)BackSpacePressed:(id)sender
{
    NSString *oStr = [fareFld text];
    if ([oStr length]>0)
    {
        [fareFld setText:[oStr substringToIndex:[oStr length]-1]];
    }
}



- (void) MarkAttandance
{
        ServerConnectionClass* network = [[ServerConnectionClass alloc]init] ;
        self.connObject = network;
        [network release];
        self.connObject.delegate = self;
    
    NSString* urlString;
    if (EntryType == 1)
    {
        urlString=[[NSString alloc]initWithFormat:@"%@{\"user_id\":\"%@\" }",kwaiterEntry,[fareFld text]];
    }
    else
    {
        urlString=[[NSString alloc]initWithFormat:@"%@{\"user_id\":\"%@\" }",kwaiterExit,[fareFld text]];
    }
        [self.connObject makeRequestForUrl:urlString];
        [urlString release];

}

- (void) removeConnectionClassObject
{
    self.connObject.delegate = nil;
    self.connObject = nil;
}
#pragma mark- MyCabuUserNetwork delegate methods

- (void) connectionEndWithError:(NSError*)error
{
    [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [self removeConnectionClassObject];
}

- (void) connectionEndedSuccefullyWithData:(NSData*)data
{
    NSError *localError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    NSLog(@"MY dictionary====%@",dictionary);
    if (dictionary != nil)
    {
        NSString* result = [dictionary objectForKey:@"result"];
        //
        
        if ([result isEqualToString:@"Success"])
        {
           
            [Utils showAlertView:[dictionary objectForKey:@"cmd"] message:[dictionary objectForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [UIView animateWithDuration:0.75f animations:^{
                             CGRect uiasViewFrame   = self.frame;
                uiasViewFrame.origin.y = self.frame.size.height ;
                
                self.frame = uiasViewFrame;
                
            } completion:^(BOOL finished)
             {
                 // completion block is important in each case. Use it
                 [fareFld setText:@""];
                 [self removeFromSuperview];
             }];
        }
        else
        {
            [Utils showAlertView:@"Alert" message:@"Wrong Emp-Id" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];

        }
    }
    else
    {
        [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    
    [self removeConnectionClassObject];
}


@end
