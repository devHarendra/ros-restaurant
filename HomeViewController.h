//
//  HomeViewController.h
//  iROS
//
//  Created by iMac Apple on 04/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//
@class ParseXML;

#import <UIKit/UIKit.h>
#import "OrderViewController.h"
#import "PaymentTypeClass.h"
#import "OrderTypeClass.h"
#import "MarkAttendanceClass.h"
#import "ReturnSelectedCellText.h"
#import "SearchPrinterViewController.h"
#import "SearchPrinterClass.h"
#import <StarIO/SMPort.h>
#import <MessageUI/MessageUI.h>

@interface HomeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,ReturnSelectedCellTextDelegate,ServerConnectionDelegate,MFMailComposeViewControllerDelegate>

{
    

    int popUp;
    int popUp_orderType;
    int popUp_markAttendance;
    int popUp_printerList;
    BOOL IsGotoPayment;
    
    
    SearchPrinterViewController* searchView;
    MFMailComposeViewController *mailComposer;
    NSString *message ;


    NSString *selectedPortName ;
    NSString *tableID;

}

@property(retain,nonatomic)   UITableView *tblList;
@property (nonatomic, retain) PaymentTypeClass *uiasView;
@property (nonatomic, retain) MarkAttendanceClass *uiasView_markAttendance;
@property (nonatomic, retain) OrderTypeClass *uiasView_orderType;
@property (nonatomic, retain) SearchPrinterClass *uiasView_printerList;
@property (retain, nonatomic) ServerConnectionClass*  connObject;
@property (retain, nonatomic) NSArray *allOrdersArr;
@property (nonatomic,retain) NSString *empname;
- (void)setPortInfo;
-(void) FetchingUserOrderList;
-(void) UserOrderHandler :(NSDictionary*)dictionary;


@end
