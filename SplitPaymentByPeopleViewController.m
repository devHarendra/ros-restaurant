//
//  SplitPaymentByPeopleViewController.m
//  iROS
//
//  Created by Dex on 15/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "SplitPaymentByPeopleViewController.h"

@interface SplitPaymentByPeopleViewController ()

@end

@implementation SplitPaymentByPeopleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIView *headerVw=[[UIView alloc] init];
    headerVw.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    [headerVw setBackgroundColor:kOrangeColor];
    [self.view addSubview:headerVw];
    [headerVw release];
    
    UIImageView *mainbg_img = [[UIImageView alloc] init];
    mainbg_img.frame=CGRectMake(30, 17.5, 200, 95);
    mainbg_img.userInteractionEnabled=TRUE;
    mainbg_img.opaque = YES;
    [mainbg_img setImage:[UIImage imageNamed:@"logo2.png"]];
    [headerVw addSubview:mainbg_img];
    [mainbg_img release];
    
    UIView *sectionView=[[UIView alloc] init];
    sectionView.frame=CGRectMake(0, 120, self.view.frame.size.width, 60);
    [sectionView setBackgroundColor:[UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1]];
    [headerVw addSubview:sectionView];
    [sectionView release];
    
    UILabel *userTypeLbl=[[UILabel alloc] init]; 
    userTypeLbl.frame=CGRectMake(30, 0, 350, 60);
    [userTypeLbl setText:@"Payment split by number of people : 3"];
    [userTypeLbl setTextAlignment:NSTextAlignmentLeft];
    [userTypeLbl setBackgroundColor:[UIColor clearColor]];
    [sectionView addSubview:userTypeLbl];
    [userTypeLbl release];

    
    UIView *headerVw1=[[UIView alloc] init];
    headerVw1.frame=CGRectMake(0, 180, [UIScreen mainScreen].bounds.size.width, 60);
    [headerVw1 setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:headerVw1];
    [headerVw1 release];
    
    UILabel *tblList=[[UILabel alloc] init];
    tblList.frame=CGRectMake(30, 5, 650, 50);
    [tblList setText:@"Total Cost"];
    [tblList setTextAlignment:NSTextAlignmentLeft];
    [tblList setBackgroundColor:[UIColor clearColor]];
    [headerVw1 addSubview:tblList];
    [tblList release];
    
    UILabel *costLbl=[[UILabel alloc] init];
    costLbl.frame=CGRectMake(690, 5, 100, 50);
    [costLbl setText:@"$50.00"];
    [costLbl setTextAlignment:NSTextAlignmentLeft];
    [costLbl setBackgroundColor:[UIColor clearColor]];
    [headerVw1 addSubview:costLbl];
    [costLbl release];
    
    
    
    UIView *headerVw2=[[UIView alloc] init];
    headerVw2.frame=CGRectMake(0, 240, [UIScreen mainScreen].bounds.size.width, 60);
    [headerVw2 setBackgroundColor:[UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1]];
    [self.view addSubview:headerVw2];
    [headerVw2 release];
    
    UILabel *discountLbl=[[UILabel alloc] init];
    discountLbl.frame=CGRectMake(30, 5, 650, 50);
    [discountLbl setText:@"Total Discount"];
    [discountLbl setTextAlignment:NSTextAlignmentLeft];
    [discountLbl setBackgroundColor:[UIColor clearColor]];
    [headerVw2 addSubview:discountLbl];
    [discountLbl release];
    
    UITextField *discountTxtFld=[[UITextField alloc] init];
    [discountTxtFld setBackgroundColor:[UIColor whiteColor ]];
    discountTxtFld.frame=CGRectMake(690, 10, 100, 40);
    [headerVw2 addSubview:discountTxtFld];
    [discountTxtFld.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [discountTxtFld.layer setBorderWidth:1.0f];
    [discountTxtFld setKeyboardType:UIKeyboardTypeDecimalPad];
    [discountTxtFld setTextAlignment:NSTextAlignmentCenter];
    [discountTxtFld release];
    
    

    UIView *headerVw3=[[UIView alloc] init];
    headerVw3.frame=CGRectMake(0, 300, [UIScreen mainScreen].bounds.size.width, 396);
    [headerVw3 setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:headerVw3];
    [headerVw3 release];
    
    UILabel *tipLbl=[[UILabel alloc] init];
    tipLbl.frame=CGRectMake(30, 5, 650, 50);
    [tipLbl setText:@"Tip given by payee:"];
    [tipLbl setTextAlignment:NSTextAlignmentLeft];
    [tipLbl setBackgroundColor:[UIColor clearColor]];
    [headerVw3 addSubview:tipLbl];
    [tipLbl release];
    
    UIScrollView* optionScrollView = [[UIScrollView alloc] init];
    [optionScrollView setScrollEnabled:YES];
    [optionScrollView setUserInteractionEnabled:YES];
    optionScrollView.frame = CGRectMake(30, 55, [UIScreen mainScreen].bounds.size.width, 225);
    [optionScrollView setBackgroundColor:[UIColor clearColor]];
    optionScrollView.contentSize = CGSizeMake(650, 270);
    [headerVw3 addSubview:optionScrollView];
    [optionScrollView setTag:111];
    [optionScrollView release];
    
    UILabel *payee1Lbl=[[UILabel alloc] init];
    payee1Lbl.frame=CGRectMake(0, 10, 350, 65);
    [payee1Lbl setText:@"Payee 1:"];
    [payee1Lbl setTextAlignment:NSTextAlignmentLeft];
    [payee1Lbl setBackgroundColor:[UIColor clearColor]];
    [optionScrollView addSubview:payee1Lbl];
    [payee1Lbl release];
    
    UITextField *payee1TxtFld=[[UITextField alloc] init];
    [payee1TxtFld setBackgroundColor:[UIColor whiteColor ]];
    payee1TxtFld.frame=CGRectMake(355, 20, 120, 45);
    [optionScrollView addSubview:payee1TxtFld];
    [payee1TxtFld.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [payee1TxtFld.layer setBorderWidth:1.0f];
    [payee1TxtFld setKeyboardType:UIKeyboardTypeDecimalPad];
    [payee1TxtFld setTextAlignment:NSTextAlignmentCenter];
//    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 30)];// view added for padding
//    payee1TxtFld.leftView = paddingView2;
//    payee1TxtFld.leftViewMode = UITextFieldViewModeAlways;
    [payee1TxtFld release];
    
    
    UILabel *payee2Lbl=[[UILabel alloc] init];
    payee2Lbl.frame=CGRectMake(0, 80, 350, 65);
    [payee2Lbl setText:@"Payee 2:"];
    [payee2Lbl setTextAlignment:NSTextAlignmentLeft];
    [payee2Lbl setBackgroundColor:[UIColor clearColor]];
    [optionScrollView addSubview:payee2Lbl];
    [payee2Lbl release];
    
    UITextField *payee2TxtFld=[[UITextField alloc] init];
    [payee2TxtFld setBackgroundColor:[UIColor whiteColor ]];
    payee2TxtFld.frame=CGRectMake(355, 90, 120, 45);
    [optionScrollView addSubview:payee2TxtFld];
    [payee2TxtFld.layer setBorderColor:[UIColor grayColor].CGColor];
    [payee2TxtFld setKeyboardType:UIKeyboardTypeDecimalPad];
    [payee2TxtFld.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [payee2TxtFld.layer setBorderWidth:1.0f];
    [payee2TxtFld setDelegate:self];
    [payee2TxtFld setTag:10];
    [payee2TxtFld setTextAlignment:NSTextAlignmentCenter];
    [payee2TxtFld release];
    
    UILabel *payee3Lbl=[[UILabel alloc] init];
    payee3Lbl.frame=CGRectMake(0, 150, 350, 65);
    [payee3Lbl setText:@"Payee 3:"];
    [payee3Lbl setTextAlignment:NSTextAlignmentLeft];
    [payee3Lbl setBackgroundColor:[UIColor clearColor]];
    [optionScrollView addSubview:payee3Lbl];
    [payee3Lbl release];
    
    UITextField *payee3TxtFld=[[UITextField alloc] init];
    [payee3TxtFld setBackgroundColor:[UIColor whiteColor ]];
    payee3TxtFld.frame=CGRectMake(355, 160, 120, 45);
    [optionScrollView addSubview:payee3TxtFld];
    [payee3TxtFld release];
    [payee3TxtFld.layer setBorderColor:[UIColor grayColor].CGColor];
    [payee3TxtFld setKeyboardType:UIKeyboardTypeDecimalPad];
    [payee3TxtFld.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [payee3TxtFld.layer setBorderWidth:1.0f];
    [payee3TxtFld setDelegate:self];
    [payee3TxtFld setTag:20];
    [payee3TxtFld setTextAlignment:NSTextAlignmentCenter];
    
    
    UILabel *totalTipLbl=[[UILabel alloc] init];
    totalTipLbl.frame=CGRectMake(30, 280, 650, 50);
    [totalTipLbl setText:@"Total tip given by payee:"];
    [totalTipLbl setTextAlignment:NSTextAlignmentLeft];
    [totalTipLbl setBackgroundColor:[UIColor clearColor]];
    [headerVw3 addSubview:totalTipLbl];
    [totalTipLbl release];
    
    UILabel *totalTip=[[UILabel alloc] init];
    totalTip.frame=CGRectMake(690, 280, 100, 50);
    [totalTip setText:@"$15"];
    [totalTip setTextAlignment:NSTextAlignmentLeft];
    [totalTip setBackgroundColor:[UIColor clearColor]];
    [headerVw3 addSubview:totalTip];
    [totalTip release];
    
    UIView *headerVw4=[[UIView alloc] init];
    headerVw4.frame=CGRectMake(0, 636, [UIScreen mainScreen].bounds.size.width, 60);
    [headerVw4 setBackgroundColor:[UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:245.0f/255.0f alpha:1]];
    [self.view addSubview:headerVw4];
    [headerVw4 release];
    
    UILabel *payableAmtLbl=[[UILabel alloc] init];
    payableAmtLbl.frame=CGRectMake(30, 5, 650, 50);
    [payableAmtLbl setText:@"Total Payable Amount :"];
    [payableAmtLbl setTextColor:kOrangeColor];
    [payableAmtLbl setTextAlignment:NSTextAlignmentLeft];
    [payableAmtLbl setBackgroundColor:[UIColor clearColor]];
    [headerVw4 addSubview:payableAmtLbl];
    [payableAmtLbl release];
    
    UILabel *payableAmtLbl1=[[UILabel alloc] init];
    payableAmtLbl1.frame=CGRectMake(690, 5, 100, 50);
    [payableAmtLbl1 setText:@"$60"];
    [payableAmtLbl1 setTextColor:kOrangeColor];
    [payableAmtLbl1 setTextAlignment:NSTextAlignmentLeft];
    [payableAmtLbl1 setBackgroundColor:[UIColor clearColor]];
    [headerVw4 addSubview:payableAmtLbl1];
    [payableAmtLbl1 release];
    
    UIView *footerVw=[[UIView alloc] init];
    footerVw.frame=CGRectMake(0, 699, self.view.frame.size.width, 100);
    [footerVw setBackgroundColor:[UIColor whiteColor]];
    [headerVw addSubview:footerVw];
    [footerVw release];
    NSLog(@"FOOTER HEIGHT : %f",tblList.frame.origin.y+tblList.frame.size.height+3);
    
    UIButton * newOrderBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [newOrderBtn setFrame:CGRectMake(30, 15, 120, 40)];
    [newOrderBtn setBackgroundColor:kOrangeColor];
    [newOrderBtn setTitle:@"Back" forState:UIControlStateNormal];
    [newOrderBtn.titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
    [newOrderBtn addTarget:self action:@selector(btnBack:) forControlEvents:UIControlEventTouchUpInside];
    [footerVw addSubview:newOrderBtn];
    
      
    UILabel *poweredLbl=[[UILabel alloc] init];
    poweredLbl.frame=CGRectMake(850, 35, 190, 40);
    [poweredLbl setText:@"Powered By:Webtye"];
    [poweredLbl setTextAlignment:NSTextAlignmentCenter];
    [poweredLbl setBackgroundColor:[UIColor clearColor]];
    [poweredLbl setFont:[UIFont systemFontOfSize:12.0f]];
    [poweredLbl setTextColor:kOrangeColor];
    [footerVw addSubview:poweredLbl];
    [poweredLbl release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIScrollView *scrollview=(UIScrollView *)[self.view viewWithTag:111];
    scrollview.scrollEnabled=YES;
    
    if (textField.tag==10)
    {
        [scrollview setContentOffset:CGPointMake(0, 120) animated:YES];
    }
    else if (textField.tag==20)
    {
        [scrollview setContentOffset:CGPointMake(0, 180) animated:YES];
    }

}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    UIScrollView *scrollview=(UIScrollView *)[self.view viewWithTag:111];
    scrollview.scrollEnabled=YES;
    [scrollview setContentOffset:CGPointMake(0, 0) animated:YES];

}

@end
