//
//  SearchPrinterClass.h
//  iROS
//
//  Created by Dex on 19/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReturnSelectedCellText.h"
#import <MessageUI/MessageUI.h>


@interface SearchPrinterClass : UIView<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate>
{
    NSIndexPath *indxPth;
    int rowCount;
    NSString *message ;
    MFMailComposeViewController *mailComposer;


}
@property (nonatomic, retain) UIView *bgVw;
@property (nonatomic, retain) UIButton* closeBtn;
@property(retain,nonatomic)   UITableView *printerList;
@property(readonly) NSString *lastSelectedPortName;
@property(retain, nonatomic) NSArray* foundPrinters;
@property(nonatomic, assign) id <ReturnSelectedCellTextDelegate> delegate;
@end
