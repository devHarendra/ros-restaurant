//
//  OpenFoodClass.h
//  iROS
//
//  Created by iMac Apple on 19/01/15.
//  Copyright (c) 2015 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenFoodClass : UIView<UITextViewDelegate, UITextFieldDelegate>
{

}
@property (nonatomic, retain) UIView   *bgVw;
@property (nonatomic, retain) UIButton *closeBtn;
@property (nonatomic, retain) UITextView *instructionTxt;
@property (nonatomic, retain) UIButton *UpdateBtn;
@property (nonatomic, retain) UITextField *amtTxt;




@end
