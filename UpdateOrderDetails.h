//
//  UpdateOrderDetails.h
//  iROS
//
//  Created by Dex on 31/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdateOrderDetails : UIView<UITextFieldDelegate,UITextViewDelegate>
{
    
    UIView *slideVw;
    UITextField *managerIdTxtFld;
    UIButton *complimentaryBtn;
    UIButton *modifier4Btn;
    UIButton *modifier3Btn;
    UIButton *modifier2Btn;
    UIButton *modifier1Btn;
}
@property (nonatomic, retain) UIView   *bgVw;
@property (nonatomic, retain) UIButton *closeBtn;
@property (nonatomic, retain) UIButton *UpdateBtn;

@property (nonatomic, retain) UITextView *instructionTxt;

@property (nonatomic, retain) NSString *managerID;
@property (nonatomic, retain) NSString *isComplimentary;
@property (nonatomic, retain) NSString *isModifier1;
@property (nonatomic, retain) NSString *isModifier2;
@property (nonatomic, retain) NSString *isModifier3;
@property (nonatomic, retain) NSString *isModifier4;

-(void) setUI;
@end
