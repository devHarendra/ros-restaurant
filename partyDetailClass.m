//
//  partyDetailClass.m
//  iROS
//
//  Created by iMac Apple on 02/02/15.
//  Copyright (c) 2015 Dex Consulting. All rights reserved.
//

#import "partyDetailClass.h"
#import "Define.h"
@implementation partyDetailClass

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:0.8];

        UIView *bgVw=[[UIView alloc] init];
        self.bgVw=bgVw;
        [bgVw release];
        [self.bgVw setBackgroundColor:[UIColor whiteColor]];
        [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-300)/2, ([UIScreen mainScreen].bounds.size.height-300)/2-50,300 , 300)];
        [self addSubview:self.bgVw];
        [self.bgVw.layer setBorderColor:[UIColor colorWithRed:77.0f/255.0f green:77.0f/255.0f blue:77.0f/255.0f alpha:1].CGColor];
        [self.bgVw.layer setBorderWidth:6.0f];
        [self.bgVw.layer setCornerRadius:4.0f];

        
        UILabel *header  = [[UILabel alloc]initWithFrame:CGRectMake(0, 10 , self.bgVw.frame.size.width, 20)];
        [header setTextAlignment:NSTextAlignmentCenter];
        [header setText:@"Party details"];
        [_bgVw addSubview:header];
        
        self.closeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeBtn setFrame:CGRectMake( self.bgVw.frame.size.width-40,1, 40, 40)];
        [self.closeBtn setBackgroundColor:[UIColor clearColor]];
        [self.closeBtn setExclusiveTouch:YES];
        //        [self.closeBtn setHidden:YES];  // to prevent closing without entering emp id
        [self.bgVw addSubview:self.closeBtn];
        [self.closeBtn setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        
        _noPeopleTF = [[UITextField alloc] initWithFrame:CGRectMake(10, 60, self.bgVw.frame.size.width-20, 50)];
        _noPeopleTF.borderStyle = UITextBorderStyleRoundedRect;
        _noPeopleTF.font = [UIFont systemFontOfSize:15];
        _noPeopleTF.placeholder = @"Enter No. of people in party";
        _noPeopleTF.autocorrectionType = UITextAutocorrectionTypeNo;
        _noPeopleTF.keyboardType = UIKeyboardTypeDefault;
        _noPeopleTF.returnKeyType = UIReturnKeyDone;
        _noPeopleTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _noPeopleTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _noPeopleTF.delegate = self;
        [self.bgVw addSubview:_noPeopleTF];
        
        
        _partyNameTF = [[UITextField alloc] initWithFrame:CGRectMake(10, 120, self.bgVw.frame.size.width-20, 50)];
        _partyNameTF.borderStyle = UITextBorderStyleRoundedRect;
        _partyNameTF.font = [UIFont systemFontOfSize:15];
        _partyNameTF.placeholder = @"Enter Name of party";
        _partyNameTF.autocorrectionType = UITextAutocorrectionTypeNo;
        _partyNameTF.keyboardType = UIKeyboardTypeDefault;
        _partyNameTF.returnKeyType = UIReturnKeyDone;
        _partyNameTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        _partyNameTF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _partyNameTF.delegate = self;
        [self.bgVw addSubview:_partyNameTF];

        
        _DoneBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.bgVw.frame.size.width/2-50, 190, 100, 40)];
//        _DoneBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [_DoneBtn setTitle:@"Done" forState:UIControlStateNormal];
        [_DoneBtn setBackgroundColor:[UIColor clearColor]];
        [_DoneBtn setExclusiveTouch:YES];
        [_DoneBtn setHidden:NO]; // hidden, i do want romeve it .
        [_DoneBtn.layer setBorderWidth:1.0f];
        [self.bgVw addSubview:_DoneBtn];
//        [submitResponseBtn addTarget:self action:@selector(submitResponse) forControlEvents:UIControlEventTouchUpInside];

            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [header setTextColor:kDarkBrownColor];
            [_DoneBtn setTitleColor:kDarkBrownColor forState:UIControlStateNormal];
            [_DoneBtn.layer setBorderColor:kDarkBrownColor.CGColor];
        }
        else
        {
            [header setTextColor:kOrangeColor];
            [_DoneBtn.layer setBorderColor:kOrangeColor.CGColor];
            [_DoneBtn setTitleColor:kOrangeColor forState:UIControlStateNormal];
            
        }
        [header release];
    }
    return self;
}


@end
