//
//  LayoutRestoCell.h
//  iROS
//
//  Created by iMac Apple on 05/02/15.
//  Copyright (c) 2015 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LayoutRestoCell : UICollectionViewCell

@property(nonatomic,retain) IBOutlet UILabel *tbleIdlbl;
@property(nonatomic,retain) IBOutlet UIButton *seatselBtn;

@end
