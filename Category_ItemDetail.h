//
//  Category_ItemDetail.h
//  iROS
//
//  Created by Dex on 29/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Category_ItemDetail : NSObject

@property (nonatomic, strong) NSString *categoryID;
@property (nonatomic, strong) NSString *itemName;
@property (nonatomic, strong) NSString *itemID;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *quantity;
@property (nonatomic, strong) NSString *modifier1;
@property (nonatomic, strong) NSString *modifier2;
@property (nonatomic, strong) NSString *modifier3;
@property (nonatomic, strong) NSString *modifier4;
@property (nonatomic, strong) NSString *complimentry;
@property (nonatomic, strong) NSString *managerID;
@property (nonatomic, strong) NSString *specialInstruction;
@property (nonatomic, strong) NSString *itemType;
@property (nonatomic, strong) UIImageView* imgVw;
@end
