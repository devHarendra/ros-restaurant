//
//  OrderDetailsClass.h
//  Restaurant Menu
//
//  Created by Dex on 16/07/14.
//  Copyright (c) 2014 Dex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailsClass : UIView<UITextViewDelegate,UITextFieldDelegate>
{
   
    UIView *slideVw;
    int popUp;
}
@property (nonatomic, retain) UIView   *bgVw;
@property (nonatomic, retain) UIButton *closeBtn;
@property (nonatomic, retain) UILabel  *popupHeader;
@property (nonatomic, retain) UIWebView *itemDetails;
@property (nonatomic, retain) UITextField *totalAmtTxtFld;
@property (nonatomic, retain) UITextView *instructionTxt;
@property (nonatomic, retain) UIButton    *AddItemBtn;
@property (nonatomic, retain) UIImageView *imgView;
@property (nonatomic, retain) UILabel     *qtyNumLbl;

@property (nonatomic, retain) NSString *managerID;


@property (nonatomic, retain) NSString *isComplimentary;
@property (nonatomic, retain) NSString *isModifier1;
@property (nonatomic, retain) NSString *isModifier2;
@property (nonatomic, retain) NSString *isModifier3;
@property (nonatomic, retain) NSString *isModifier4;
@end
