//
//  SplitPaymentByPeopleViewController.h
//  iROS
//
//  Created by Dex on 15/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplitPaymentByPeopleViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,NSXMLParserDelegate,UITextFieldDelegate>


@end
