//
//  SearchPrinterClass.m
//  iROS
//
//  Created by Dex on 19/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "SearchPrinterClass.h"
#import "StarIO/SMPort.h"
@implementation SearchPrinterClass
@synthesize lastSelectedPortName;
@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:0.8];
        
        
        UIView *bgVw=[[UIView alloc] init];
        self.bgVw =bgVw;
        [bgVw release];
        [self.bgVw setBackgroundColor:[UIColor whiteColor]];
        [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-600)/2, ([UIScreen mainScreen].bounds.size.height-500)/2,600 , 500)];
        [self addSubview:self.bgVw];
        [self.bgVw.layer setBorderColor:[UIColor colorWithRed:77.0f/255.0f green:77.0f/255.0f blue:77.0f/255.0f alpha:1].CGColor];
        [self.bgVw.layer setBorderWidth:6.0f];
        [self.bgVw.layer setCornerRadius:4.0f];
        
        
        
        
        self.closeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeBtn setFrame:CGRectMake( self.bgVw.frame.size.width-60,1, 60, 60)];
        [self.closeBtn setBackgroundColor:[UIColor clearColor]];
        [self.closeBtn setExclusiveTouch:YES];
        [self.bgVw addSubview:self.closeBtn];
        [self.closeBtn setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        
        
        UILabel *popupHeader=[[UILabel alloc] init];
        [popupHeader setFrame:CGRectMake(0, 0, self.bgVw.frame.size.width, 60)];
        [popupHeader setBackgroundColor:[UIColor clearColor]];
        [popupHeader setTextAlignment:NSTextAlignmentCenter];
        [popupHeader setTextColor:kOrangeColor];
        [popupHeader setText:@"Printers Available on LAN"];
        [popupHeader setFont:[UIFont systemFontOfSize:21]];
        [self.bgVw addSubview:popupHeader];
        [popupHeader release];
        
        UILabel *popupHeaderlbl=[[UILabel alloc] init];
        [popupHeaderlbl setFrame:CGRectMake(30, popupHeader.frame.origin.y+50, self.bgVw.frame.size.width-60, 40)];
        [popupHeaderlbl setBackgroundColor:[UIColor clearColor]];
        [popupHeaderlbl setTextAlignment:NSTextAlignmentLeft];
        [popupHeaderlbl setTextColor:[UIColor darkGrayColor]];
        [popupHeaderlbl setText:@"Please select your preference:"];
        [popupHeaderlbl setFont:[UIFont systemFontOfSize:18]];
        [self.bgVw addSubview:popupHeaderlbl];
        [popupHeaderlbl release];
        
        UIView *viewLine=[[UIView alloc] init];
        [viewLine setBackgroundColor:[UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1]];
        [viewLine setFrame:CGRectMake(20, popupHeaderlbl.frame.origin.y+40, self.bgVw.frame.size.width-60, 1)];
        [self.bgVw addSubview:viewLine];
        [viewLine release];
        
       UITableView *printerLst=[[UITableView alloc] init];
        self.printerList =printerLst;
        [printerLst release];
        self.printerList.frame=CGRectMake(15, 100, 540, 400);
        [self.printerList setDelegate:self];
        [self.printerList setDataSource:self];
        [self.printerList setBackgroundColor:[UIColor clearColor]];
        [self.printerList  setSeparatorColor:kOrangeColor];
        [self.bgVw addSubview:self.printerList];

    }
    return self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.foundPrinters.count ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    
    if (indexPath.row < self.foundPrinters.count)
    {
        PortInfo *port = self.foundPrinters[indexPath.row];
        cell.textLabel.text = port.modelName;
        NSString *detailText = [NSString stringWithFormat:@"%@(%@)", port.portName, port.macAddress];
        cell.detailTextLabel.text = detailText;
        
          NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
         [prefs setObject:port.portName forKey:@"PrinterPortName"];
          [prefs synchronize];
    }
//    else if (indexPath.row == self.foundPrinters.count)
//    {
//        cell.textLabel.text = @"Back";
//    }
    cell.textLabel.textColor=[UIColor darkGrayColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.foundPrinters.count)
    {
        PortInfo *portInfo = self.foundPrinters[indexPath.row];
        lastSelectedPortName = portInfo.portName;
    }
    
    [self.delegate returnSelectedCellText];
   
}



@end
