//
//  OrderDecriptionForPay.m
//  iROS
//
//  Created by iMac Apple on 03/01/15.
//  Copyright (c) 2015 Dex Consulting. All rights reserved.
//

#import "OrderDecriptionForPay.h"
#import "iROS/iRestaurantsGlobalDataClass.h"
#import "rasterPrinting.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface OrderDecriptionForPay ()

@end

@implementation OrderDecriptionForPay

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    scroll.contentSize = CGSizeMake(scroll.frame.size.width, scroll.frame.size.height);

    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
    {
        [self.view setBackgroundColor:kDarkBrownColor];
    }
    else
    {
        [self.view setBackgroundColor:kOrangeColor];
    }
    
    UIButton *taxCheckMark = (UIButton*)[scroll viewWithTag:7888];
    [taxCheckMark setSelected:YES];
    
    
    currentCartPriceWithoutTax = [iRestaurantsGlobalDataClass getInstance].amtToPay;

//    Including 8% Tax in cart price by below line -
    
//    NSString *amount =[_currentCartPriceWithoutTax stringByReplacingOccurrencesOfString:@"$" withString:@""];
    currentCartPriceWithTax= currentCartPriceWithoutTax + [self CalculateTax:[iRestaurantsGlobalDataClass getInstance].amtToPay Taxpercent:8.875] ;

    
    
    UILabel *totalPaylbl = (UILabel*)[scroll viewWithTag:125];
    [totalPaylbl setText:[NSString stringWithFormat:@"$%.2f",currentCartPriceWithTax]];

    NSLog(@"order %@",_orderListArr);
    
    UILabel *amtTobePaylbl = (UILabel*)[scroll viewWithTag:126];
    [amtTobePaylbl setText:[NSString stringWithFormat:@"$%.2f",currentCartPriceWithTax]];
 
    [iRestaurantsGlobalDataClass getInstance].amtToPay  = currentCartPriceWithTax; // Now payble amt. with tax added this will be ussed for megensa api payment (Discount is not added).
    
    SearchPrinterClass *printersList = [[SearchPrinterClass alloc] init];
    [self setUiasView_printerList:printersList];
    [self.uiasView_printerList setFrame:CGRectMake(0, self.view.frame.size.height , self.view.frame.size.width, self.view.frame.size.height)];
    [self.uiasView_printerList.closeBtn addTarget:self action:@selector(printerListCloseBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self registerForKeyboardNotifications];
    
    [printersList release];
  
    UIView *DiscountPopUp = (UIView*)[self.view viewWithTag:300];
    [DiscountPopUp setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    
    [self GetDiscountType:self];

}


// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+15, 0.0);
    scroll.contentInset = contentInsets;
    scroll.scrollIndicatorInsets = contentInsets;
   }

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scroll.contentInset = contentInsets;
    scroll.scrollIndicatorInsets = contentInsets;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField.tag == 127) {
        
    }
    
    
    activeField = nil;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}



-(IBAction)payBtnClicked:(id)sender
{
    [NumpadV setHidden:YES]; // this is now using,  now this functionality removed.
    UILabel *amtTobePaylbl = (UILabel*)[scroll viewWithTag:126];
    UITextField *txtPaid = (UITextField*)[self.view viewWithTag:7586];
    NSString *aStr = [amtTobePaylbl.text stringByReplacingOccurrencesOfString:@"$" withString:@""];


        UILabel *BalLbl = (UILabel*)[self.view viewWithTag:7585];
        [BalLbl setText:[NSString stringWithFormat:@"$%.2f",txtPaid.text.floatValue-aStr.floatValue]];
        [BalReturnV setHidden:false];
 }

-(IBAction)cashClicked:(id)sender
{
    
    [BalReturnV setHidden:true];
    [NumpadV setHidden:false];

    UIButton * printbtn = (UIButton*)[scroll viewWithTag:75];
    [printbtn setHidden:true];
    
    UITextField *txt = (UITextField*)[self.view viewWithTag:7584];
    [txt setText:nil];
    
    [sender setBackgroundColor:[UIColor colorWithRed:37.0/255.0 green:104.0/255.0 blue:156.0/255.0 alpha:1.0]];
}





-(IBAction)HandleKeyBoard:(UIButton*)sender
{
    UITextField *txt = (UITextField*)[self.view viewWithTag:7584];
    NSString *aStr = [NSString stringWithFormat:@"%@%@",[txt text], [sender.titleLabel text]];
    

    
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    [formatter setMaximumFractionDigits:2];
    [formatter setMinimumFractionDigits:2];
    NSString *formatted = [formatter stringFromNumber:[NSNumber numberWithFloat:aStr.floatValue]];
    [txt setText:[NSString stringWithFormat:@"%@",formatted]];

    }


-(IBAction)BackSpacePressed:(id)sender
{
//    UITextField *fareFld = (UITextField*)[self.view viewWithTag:7584];
//
//    NSString *oStr = [fareFld text];
//    if ([oStr length]>0)
//    {
//        [fareFld setText:[oStr substringToIndex:[oStr length]-1]];
//    }
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    
    if (textField.tag == 7586)
    {
        NSInteger MAX_DIGITS = 7; // 99999.99
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [numberFormatter setMaximumFractionDigits:2];
        [numberFormatter setMinimumFractionDigits:2];
        
        NSString *stringMaybeChanged = [NSString stringWithString:string];
        if (stringMaybeChanged.length > 1)
        {
            NSMutableString *stringPasted = [NSMutableString stringWithString:stringMaybeChanged];
            
            [stringPasted replaceOccurrencesOfString:numberFormatter.currencySymbol
                                          withString:@""
                                             options:NSLiteralSearch
                                               range:NSMakeRange(0, [stringPasted length])];
            
            [stringPasted replaceOccurrencesOfString:numberFormatter.groupingSeparator
                                          withString:@""
                                             options:NSLiteralSearch
                                               range:NSMakeRange(0, [stringPasted length])];
            
            NSDecimalNumber *numberPasted = [NSDecimalNumber decimalNumberWithString:stringPasted];
            stringMaybeChanged = [numberFormatter stringFromNumber:numberPasted];
        }
        
        UITextRange *selectedRange = [textField selectedTextRange];
        UITextPosition *start = textField.beginningOfDocument;
        NSInteger cursorOffset = [textField offsetFromPosition:start toPosition:selectedRange.start];
        NSMutableString *textFieldTextStr = [NSMutableString stringWithString:textField.text];
        NSUInteger textFieldTextStrLength = textFieldTextStr.length;
        
        [textFieldTextStr replaceCharactersInRange:range withString:stringMaybeChanged];
        
        [textFieldTextStr replaceOccurrencesOfString:numberFormatter.currencySymbol
                                          withString:@""
                                             options:NSLiteralSearch
                                               range:NSMakeRange(0, [textFieldTextStr length])];
        
        [textFieldTextStr replaceOccurrencesOfString:numberFormatter.groupingSeparator
                                          withString:@""
                                             options:NSLiteralSearch
                                               range:NSMakeRange(0, [textFieldTextStr length])];
        
        [textFieldTextStr replaceOccurrencesOfString:numberFormatter.decimalSeparator
                                          withString:@""
                                             options:NSLiteralSearch
                                               range:NSMakeRange(0, [textFieldTextStr length])];
        
        if (textFieldTextStr.length <= MAX_DIGITS)
        {
            NSDecimalNumber *textFieldTextNum = [NSDecimalNumber decimalNumberWithString:textFieldTextStr];
            NSDecimalNumber *divideByNum = [[[NSDecimalNumber alloc] initWithInt:10] decimalNumberByRaisingToPower:numberFormatter.maximumFractionDigits];
            NSDecimalNumber *textFieldTextNewNum = [textFieldTextNum decimalNumberByDividingBy:divideByNum];
            NSString *textFieldTextNewStr = [numberFormatter stringFromNumber:textFieldTextNewNum];
            //  [divideByNum release];
            
            textField.text = textFieldTextNewStr;
            
            if (cursorOffset != textFieldTextStrLength)
            {
                NSInteger lengthDelta = textFieldTextNewStr.length - textFieldTextStrLength;
                NSInteger newCursorOffset = MAX(0, MIN(textFieldTextNewStr.length, cursorOffset + lengthDelta));
                UITextPosition* newPosition = [textField positionFromPosition:textField.beginningOfDocument offset:newCursorOffset];
                UITextRange* newRange = [textField textRangeFromPosition:newPosition toPosition:newPosition];
                [textField setSelectedTextRange:newRange];
            }
        }
        return NO;
    }
    
    return YES;
    
}
    

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (Alerttag) {
        case 70:
            if (buttonIndex == 0)
            {
                Alerttag = 71;
                [Utils showAlertView:@"Payment recieved" message:@"Do you want take Reciept " delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No"];
            }
            break;
        
        case 71:
            if (buttonIndex == 0)
            {
                UIButton * printbtn = (UIButton*)[scroll viewWithTag:75];
                [printbtn setHidden:false];
                
                UITextField *txtbalAmt = (UITextField*)[scroll viewWithTag:7586];
                [txtbalAmt resignFirstResponder];
            }
            else{
                UITextField *txtbalAmt = (UITextField*)[scroll viewWithTag:7586];
                [txtbalAmt resignFirstResponder];
            }
            break;
            
        default:
            break;
    }
   
}

-(IBAction)CancelBtnClicked:(id)sender
{
    UILabel *BalLbl = (UILabel*)[self.view viewWithTag:7585];
    [BalLbl setText:nil];


}

-(IBAction)ResignKeyBoard:(id)sender
{
    [sender resignFirstResponder];
}



-(IBAction)Goback:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table View Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return [_orderListArr count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        }
  UILabel *namelbl=(UILabel*)[cell viewWithTag:10];
  [namelbl setText:[[_orderListArr objectAtIndex:indexPath.row] objectForKey:@"item_name"]];
    
    UILabel *qtylbl=(UILabel*)[cell viewWithTag:20];
    [qtylbl setText:[[_orderListArr objectAtIndex:indexPath.row] objectForKey:@"quantity"]];
    
    UILabel *pricelbl=(UILabel*)[cell viewWithTag:30];
    [pricelbl setText:[[_orderListArr objectAtIndex:indexPath.row] objectForKey:@"price"]];

    UIImageView *imgVw = (UIImageView*)[cell viewWithTag:9];
 
    /*  // harry crashing because url is nill
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
          [imgVw setImageWithURL:[NSURL URLWithString:[[_orderListArr objectAtIndex:indexPath.row] objectForKey:@"photo_url"]]
                            placeholderImage:[UIImage imageNamed:@"logo2.png"]
                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {  NSLog(@"image setting up ! ");
                                   }];
                   });
    */
        return cell;
}

#pragma Collection view code starts from here - 

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [DiscountsArr count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    NSMutableDictionary * dic = [DiscountsArr objectAtIndex:indexPath.item];
    
    
    UILabel * amtTxt = (UILabel*)[cell viewWithTag:1004];
    UILabel * reason = (UILabel*)[cell viewWithTag:1003];
    [reason setText:[NSString stringWithFormat:@"%@",[dic objectForKey:@"reason"]]];
 
    float amt = [[dic objectForKey:@"dis_amount"] floatValue];
    float per = [[dic objectForKey:@"dis_percentage"] floatValue];
  
    if (amt !=0 && per == 0)
    {
        [amtTxt setText:[NSString stringWithFormat:@"$ %.02f",amt]];
    }
    else  if (amt ==0 && per != 0)
    {
        [amtTxt setText:[NSString stringWithFormat:@"%.02f %@",per,@"%"]];
    }
    
    else  if (amt !=0 && per != 0)
    {
        [amtTxt setText:[NSString stringWithFormat:@"%.02f %@",per,@"%"]];
    }

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary * dic = [DiscountsArr objectAtIndex:indexPath.item];
    UITextField * amtTF = (UITextField*)[self.view viewWithTag:120];
    UILabel *amtTobePaylbl = (UILabel*)[scroll viewWithTag:126];

    float amt = [[dic objectForKey:@"dis_amount"] floatValue];
    float per = [[dic objectForKey:@"dis_percentage"] floatValue];
    if (amt !=0 && per == 0)
    {
        amtTobePaylbl.text = [NSString stringWithFormat:@"%.02f",[amtTF.text floatValue]-amt];
    }
    else  if (amt ==0 && per != 0)
    {
            amtTobePaylbl.text = [NSString stringWithFormat:@"%.02f",[self CalculateDiscountPercent:[amtTF.text floatValue] DispercentOrAmt:per]];
    }

    else  if (amt !=0 && per != 0)
    {
        amtTobePaylbl.text = [NSString stringWithFormat:@"%.02f",[self CalculateDiscountPercent:[amtTF.text floatValue] DispercentOrAmt:per]];
    }
    
}

-(void)DisBYper
{
    NSLog(@"percent");
    
}

-(void)DisBYAmt
{
    NSLog(@"Amount");

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (IBAction)printerSettings:(id)sender
{
    //    ViewController *viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] ;
    //    [self.navigationController pushViewController:viewController animated:YES];
    //    [viewController release];
    
    
    NSArray *array = [[SMPort searchPrinter:@"TCP:"] retain];
    
    self.uiasView_printerList.foundPrinters=array;
    self.uiasView_printerList.delegate=self;
    popUp_printerList=0;
    // show new order screen
    [UIView animateWithDuration:0.60f animations:^{
        
        
        CGRect uiasViewFrame   = self.uiasView_printerList.frame;
        uiasViewFrame.origin.y =0;
        self.uiasView_printerList.frame = uiasViewFrame;
        [self.view addSubview:self.uiasView_printerList];
        
        
    } completion:^(BOOL finished) {
        
        
    }];
    
    //        searchView = [[SearchPrinterViewController alloc] initWithNibName:@"SearchPrinterViewController" bundle:nil];
    //        searchView.foundPrinters = array;
    //        searchView.delegate = self;
    //        [self presentViewController:searchView animated:YES completion:nil];
    //        [searchView release];
            [array release];
    
    
}


-(void) printerListCloseBtnClicked
{
    popUp_printerList=1;
    [UIView animateWithDuration:0.75f animations:^
    {
        CGRect uiasViewFrame   = self.uiasView_printerList.frame;
        uiasViewFrame.origin.y = self.view.frame.size.width ;
        
        self.uiasView_printerList.frame = uiasViewFrame;
       } completion:^(BOOL finished)
     {
         // completion block is important in each case. Use it
         [self.uiasView_printerList removeFromSuperview];
     }];
}

#pragma Mark - Tax and Discount Handlers

-(float)CalculateTax :(float)Amount  Taxpercent :(float)taxPerc
{
//    float taxPerc = 8.0;
    
    float FinalPayWithTax = (taxPerc/100)*Amount;
    
    return FinalPayWithTax;
}

-(float)CalculateDiscountPercent :(float)Amount  DispercentOrAmt :(float)disPercAmt
{
    //    float taxPerc = 8.0;
    float dis = (disPercAmt/100)*Amount;
    return Amount-dis;
}


-(IBAction)ToggleTaxBtn:(UIButton*)sender
{
    UILabel *totalPaylbl = (UILabel*)[scroll viewWithTag:125];  // subtotal label
    UILabel *amtTobePaylbl = (UILabel*)[scroll viewWithTag:126];


    if (!sender.selected)
    {
        [sender setSelected:true];
        [totalPaylbl setText:[NSString stringWithFormat:@"$%.2f",currentCartPriceWithTax]];
        [amtTobePaylbl setText:[NSString stringWithFormat:@"$%.2f",currentCartPriceWithTax]];
    }
    else if (sender.selected)
    {
        [sender setSelected:false];
        [totalPaylbl setText:[NSString stringWithFormat:@"$%.2f",currentCartPriceWithoutTax]];
        [amtTobePaylbl setText:[NSString stringWithFormat:@"$%.2f",currentCartPriceWithoutTax]];
    }
}

-(IBAction)ShowDiscountPopUp:(UIButton*)btn
{
    
    UIView *DiscountPopUp = (UIView*)[self.view viewWithTag:300];
    
    if (DiscountPopUp.frame.origin.y==self.view.frame.size.height)
    {
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             
                             [DiscountPopUp setFrame:CGRectMake(0, 0, DiscountPopUp.frame.size.width, DiscountPopUp.frame.size.height)];
                             
                         }
                         completion:^(BOOL finished){
                             [DiscountPopUp setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f  green:0.0f/255.0f blue:0.0f/255.0f alpha:0.5]];
                         }];
    }
    
    else  if (DiscountPopUp.frame.origin.y==0)
    {
        if ([mangerId.text length]<=0 && btn.tag !=301)
        {
            [Utils showAlertView:@"Alert" message:@"Manager Id cannot be blank" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            return;
        }
        
        [UIView animateWithDuration:0.2
                              delay:0.0
                            options: UIViewAnimationCurveEaseIn
                         animations:^{
                             
                             [DiscountPopUp setFrame:CGRectMake(0, self.view.frame.size.height, DiscountPopUp.frame.size.width, self.view.frame.size.height)];
                             
                         }
                         completion:^(BOOL finished){
                             [self.view endEditing:YES];
                             [DiscountPopUp setBackgroundColor:[UIColor clearColor]];
                         }];
    }
}

- (IBAction)GetDiscountType:(id)sender
{
    ServerConnectionClass* network = [[ServerConnectionClass alloc]init] ;
    self.connObject = network;
    [network release];
    self.connObject.delegate = self;
    
    NSString* urlString = [[NSString alloc]initWithFormat:@"%@{\"status\":\"1\" }",kDiscountType];
    [self.connObject makeRequestForUrl:urlString];
    [urlString release];
}

- (void) removeConnectionClassObject
{
    self.connObject.delegate = nil;
    self.connObject = nil;
}

#pragma mark- MyCabuUserNetwork delegate methods
- (void) connectionEndWithError:(NSError*)error
{
    [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [self removeConnectionClassObject];
}

- (void) connectionEndedSuccefullyWithData:(NSData*)data
{
    NSError *localError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    NSLog(@"MY dictionary====%@",dictionary);
    if (dictionary != nil)
    {
        NSString* result = [dictionary objectForKey:@"result"];
        NSString* cmd = [dictionary objectForKey:@"cmd"];

        
        if ([result isEqualToString:@"Success"] && [cmd isEqualToString:@"promo"]  )
        {
            DiscountsArr = [[NSMutableArray alloc]initWithArray:[dictionary objectForKey:@"promocode"]];
            [(UICollectionView *)[self.view viewWithTag:1005] reloadData];
         
        }
    }
    else
    {
        [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    [self removeConnectionClassObject];
}









@end
