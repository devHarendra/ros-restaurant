//
//  ServerConnectionClass.h
//  iROS
//
//  Created by Dex on 25/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol ServerConnectionDelegate

- (void) connectionEndWithError:(NSError*)error;
- (void) connectionEndedSuccefullyWithData:(NSData*)data;


@end

@interface ServerConnectionClass : NSObject<NSURLConnectionDelegate, NSURLConnectionDataDelegate>
{
    NSMutableData*  _responseData;
}

- (void) makeJsonRequestFor:(NSString*) xmlString url:(NSString*)urlStr;

- (void) makeRequestForUrl:(NSString*)urlString;

-(void)CallWebApiPostData:(NSURL*)url PayLoadStr:(NSString*)jsonRequest;
-(void)SoapWebCall:(NSString*)url SoapMessage:(NSString*)msgStr SoapAction:(NSString*)SoapAct;


@property (retain, nonatomic)id <ServerConnectionDelegate> delegate;


@end


