//
//  ServerConnectionClass.m
//  iROS
//
//  Created by Dex on 25/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "ServerConnectionClass.h"
#import "Define.h"
@implementation ServerConnectionClass
@synthesize delegate;

- (void) dealloc
{
    [super dealloc];
}

#pragma mark - Class Methods

- (void) makeJsonRequestFor:(NSString*) xmlString url:(NSString*)urlStr
{
    if ([Utils connected])
    {
        [Utils startActivityIndicator];
       
        NSString* webStringURL = [xmlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURL *url = [NSURL URLWithString:urlStr];
        NSLog(@"%@",urlStr);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        [request addValue: @"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setHTTPMethod:@"POST"];
        [request setValue:[NSString stringWithFormat:@"%lu",(unsigned long)[webStringURL length]] forHTTPHeaderField:@"Content-length"] ;
        [request setHTTPBody:[webStringURL dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSURLConnection *connection =[[[NSURLConnection alloc] initWithRequest:request delegate:self] autorelease];
        [connection start];
    }
    else
    {
        [Utils showAlertView:@"Network Error" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
}

- (void) makeRequestForUrl:(NSString*)urlString
{
    if ([Utils connected])
    {
        [Utils startActivityIndicator];
        urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        
        NSLog(@"url ---------%@",urlString);
        NSURLConnection* connection =  [[[NSURLConnection alloc] initWithRequest:request  delegate:self] autorelease];
        [request release];
        [connection start];
    }
    else
    {
        [Utils showAlertView:@"Network Error" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
}

#pragma mark - NSURLConnection class methods

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [Utils stopActivityIndicator];
    [self.delegate connectionEndWithError:error];
    NSLog(@"%@",error);
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _responseData = [[NSMutableData alloc] init];
    _responseData.length = 0;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [Utils stopActivityIndicator];
    [self.delegate connectionEndedSuccefullyWithData:_responseData];
    [_responseData autorelease];
    [connection cancel];
}


-(void)CallWebApiPostData:(NSURL*)url PayLoadStr:(NSString*)jsonRequest
{
    
    if ([Utils connected])
    {
        [Utils startActivityIndicator];
        NSLog(@"jsonRequest is %@", jsonRequest);
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        
        NSData *requestData = [jsonRequest dataUsingEncoding:NSUTF8StringEncoding];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        //    NSError *localError=nil;
        //    NSData *resData;
        
        __block NSMutableDictionary *dictionarydata;
        __block NSHTTPURLResponse *httpResponse = nil;
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:
         ^(NSURLResponse *response, NSData *returnData, NSError *error) {
             
             // This will get the NSURLResponse into NSHTTPURLResponse format
             httpResponse = (NSHTTPURLResponse*)response;
             
             // This will Fetch the status code from NSHTTPURLResponse object
             long responseStatusCode = [httpResponse statusCode];
             
             //Just to make sure, it works or not
             NSLog(@"Status Code  :: %ld", responseStatusCode);
             dictionarydata=[NSJSONSerialization JSONObjectWithData:returnData options:0 error:nil];
             
             if ([httpResponse statusCode] == 200)
             {
                 //             NSLog(@"response data - is %@", dictionarydata);
                 [Utils stopActivityIndicator];
                 [self.delegate connectionEndedSuccefullyWithData:returnData];
                 [_responseData autorelease];
             }
         }];

    }
    else
    {
        [Utils showAlertView:@"Network Error" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
}

#pragma  Soap request starts from here -----
-(void)SoapWebCall:(NSString*)url SoapMessage:(NSString*)msgStr SoapAction:(NSString*)SoapAct
{
    if ([Utils connected])
    {
        [Utils startActivityIndicator];
      NSURL *aUrl = [NSURL URLWithString:url];

        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:aUrl];
        NSString *msgLength = [NSString stringWithFormat:@"%d", [msgStr length]];
        
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: SoapAct forHTTPHeaderField:@"SOAPAction"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody: [msgStr dataUsingEncoding:NSUTF8StringEncoding]];

        __block NSHTTPURLResponse *httpResponse = nil;
        
        [NSURLConnection sendAsynchronousRequest:theRequest
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:
         ^(NSURLResponse *response, NSData *returnData, NSError *error) {
             
             // This will get the NSURLResponse into NSHTTPURLResponse format
             httpResponse = (NSHTTPURLResponse*)response;
             
             // This will Fetch the status code from NSHTTPURLResponse object
             long responseStatusCode = [httpResponse statusCode];
             
             //Just to make sure, it works or not
             NSLog(@"Status Code  :: %ld", responseStatusCode);
             
             if ([httpResponse statusCode] == 200)
             {
                 [Utils stopActivityIndicator];
                 [self.delegate connectionEndedSuccefullyWithData:returnData];
                 [_responseData autorelease];
                 
             }
         }];
        
        
        

    }
    else
    {
        [Utils showAlertView:@"Network Error" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }




}







@end
