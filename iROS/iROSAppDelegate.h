//
//  iROSAppDelegate.h
//  iROS
//
//  Created by iMac Apple on 04/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UAirship.h"
#import "UAConfig.h"
#import "UAPush.h"
#import "UAInboxPushHandler.h"
#import "LoginViewController.h"
#import "EncryptedSwipeData.h"
#import "iROSiROSNewLoginVC.h"

@interface iROSAppDelegate : UIResponder <UIApplicationDelegate,UARegistrationDelegate,UAPushNotificationDelegate>
{
    LoginViewController *viewHome;
    iROSiROSNewLoginVC * viewHomeNew;

    UINavigationController *navController;
}
@property (strong, nonatomic) UIView*   activityView;
@property (strong, nonatomic) UIView*   customBackgroundView;
@property (strong, nonatomic) UIImageView*  backgroundmageView;
@property (strong, nonatomic) UIWindow *window;


@property (strong, nonatomic) NSString *pid;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSString *responseCode;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *tranType;
@property (strong, nonatomic) NSString *authCode;
@property (strong, nonatomic) NSString *authAmount;
@property (strong, nonatomic) NSString *amount;
@property (strong, nonatomic) NSString *taxAmt;
@property (strong, nonatomic) NSString *acqRefData;
@property (strong, nonatomic) NSString *cardType;
@property (strong, nonatomic) NSString *cvvResult;
@property (strong, nonatomic) NSString *displayMessage;
@property (strong, nonatomic) NSString *expDate;
@property (strong, nonatomic) NSString *invoice;
@property (strong, nonatomic) NSString *maskedAccount;
@property (strong, nonatomic) NSString *refNo;
@property (strong, nonatomic) NSString *statusMessage;
@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic) NSString *transPostTime;
@property (strong, nonatomic) NSString *tranactionID;
@property (strong, nonatomic) EncryptedSwipeData *encryptedSwipeData;


+ (NSString *)getPortName;
+ (void)setPortName:(NSString *)m_portName;
+ (NSString*)getPortSettings;
+ (void)setPortSettings:(NSString *)m_portSettings;


@end
