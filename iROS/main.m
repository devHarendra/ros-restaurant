//
//  main.m
//  iROS
//
//  Created by iMac Apple on 04/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "iROSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([iROSAppDelegate class]));
    }
}
