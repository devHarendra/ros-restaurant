//
//  iRestaurantsGlobalDataClass.h
//  iRestaurants Universal
//
//  Created by Dex on 04/01/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iRestaurantsGlobalDataClass : NSObject


@property (assign, nonatomic) float amtToPay;
@property (nonatomic, retain) NSMutableDictionary *DicProcessCS10;
@property (assign, nonatomic) int loggedUserId;
@property (nonatomic, retain) NSString *orderidForpay;
@property (nonatomic, retain) NSString *orderType;
@property (nonatomic, retain) NSString  *waiterName;
@property (nonatomic, retain) NSString  *cateringQuantity;
@property (nonatomic, retain) NSString  *orderPlaced_new;
@property (nonatomic, retain) NSArray *itemNameArray;
@property (nonatomic, retain) NSArray *itemQuantityArray;
@property (nonatomic, retain) NSArray *itemPriceArray;
@property (nonatomic, retain) NSMutableArray *tableBookedArr_bar;
@property (nonatomic, retain) NSMutableArray *tableBookedArr_food;
@property (nonatomic, assign) int attendanceFlag;
+(iRestaurantsGlobalDataClass*)getInstance;
@end
