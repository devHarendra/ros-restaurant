//
//  iROSAppDelegate.m
//  iROS
//
//  Created by iMac Apple on 04/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "iROSAppDelegate.h"
#import "UAirship.h"
#import "UAConfig.h"
#import "UAPush.h"
#import "UAInboxPushHandler.h"

static NSString *portName = nil;
static NSString *portSettings = nil;
//static NSString *drawerPortName = nil;

@implementation iROSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UIWindow *window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window=window;
    [window release];
    
    UAConfig *config = [UAConfig defaultConfig];
    [UAirship takeOff:config];
    
    [UAPush shared].notificationTypes = (UIRemoteNotificationTypeBadge |
                                         UIRemoteNotificationTypeSound |
                                         UIRemoteNotificationTypeAlert |
                                         UIRemoteNotificationTypeNewsstandContentAvailability);
    
    
    [[UAPush shared] registerForRemoteNotifications];
    [UAPush shared].pushNotificationDelegate = self;
    [UIApplication sharedApplication].applicationIconBadgeNumber =0;
    
    viewHomeNew=[[iROSiROSNewLoginVC alloc]init];
    navController=[[UINavigationController alloc]initWithRootViewController:viewHomeNew];
    [viewHome release];
    [navController setNavigationBarHidden:TRUE];
    
    self.window.rootViewController=navController;
    self.window.backgroundColor = [UIColor whiteColor];
    
    
    
    UIView* activityV = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.activityView = activityV;
    self.activityView.backgroundColor = [UIColor blackColor];
    self.activityView.alpha = 0.5;
    UIActivityIndicatorView* activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = self.activityView.center;
    [activityIndicator setColor:[UIColor orangeColor]];
    [activityIndicator startAnimating];
    [self.activityView addSubview:activityIndicator];
    [activityIndicator release];
    [activityV release];
    
    UIView* backgrounV = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.customBackgroundView = backgrounV;
    self.customBackgroundView.backgroundColor = [UIColor blackColor];
    self.customBackgroundView.alpha = 0.5;
    [backgrounV release];
    
    
    
    
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma mark getter/setter

+ (NSString*)getPortName
{
    return portName;
}

+ (void)setPortName:(NSString *)m_portName
{
    if (portName != m_portName) {
        [portName release];
        portName = [m_portName copy];
    }
}

+ (NSString *)getPortSettings
{
    return portSettings;
}

+ (void)setPortSettings:(NSString *)m_portSettings
{
    if (portSettings != m_portSettings) {
        [portSettings release];
        portSettings = [m_portSettings copy];
    }
}
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [[UAPush shared] registerDeviceToken:deviceToken];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[[UAPush shared] deviceToken] forKey:kDeviceToken];
    [defaults synchronize];
   
}




-(void)displayNotificationAlert:(NSString *)alertMessage
{
    NSLog(@"sssssss: %@",alertMessage);
    UIAlertView* cancelTripReasonAlert = [[UIAlertView alloc] initWithTitle:nil message:alertMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [cancelTripReasonAlert show];
    [cancelTripReasonAlert release];
}

-(void)launchedFromNotification:(NSDictionary *)notification fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    [UIApplication sharedApplication].applicationIconBadgeNumber =0;
    NSLog(@"NOTIFICATION IN BG :%@",[[notification objectForKey:@"aps"] objectForKey:@"alert"]);
    if (notification !=nil && [notification objectForKey:@"aps"] !=[NSNull null]  )
    {
        
       [Utils showAlertView:@"Notification" message:[[notification objectForKey:@"aps"] objectForKey:@"alert"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
    }
}
-(void)receivedForegroundNotification:(NSDictionary *)notification fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    [UIApplication sharedApplication].applicationIconBadgeNumber =0;
    if (notification !=nil && [notification objectForKey:@"aps"] !=[NSNull null]  )
    {
//         [Utils showAlertView:@"Notification" message:[[notification objectForKey:@"aps"] objectForKey:@"alert"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
}
- (void) reset {
    self.pid = @"";
    self.message = @"";
    self.responseCode = @"";
    self.status = @"";
    self.tranType = @"";
    self.authCode = @"";
    self.authAmount = @"";
    self.amount = @"";
    self.taxAmt = @"";
    self.acqRefData = @"";
    self.cardType = @"";
    self.cvvResult = @"";
    self.displayMessage = @"";
    self.expDate = @"";
    self.invoice = @"";
    self.maskedAccount = @"";
    self.refNo = @"";
    self.statusMessage = @"";
    self.token = @"";
    self.transPostTime = @"";
    self.tranactionID = @"";
    self.encryptedSwipeData = [[EncryptedSwipeData alloc] init];
}
@end
