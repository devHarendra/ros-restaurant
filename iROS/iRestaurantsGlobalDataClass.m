//
//  iRestaurantsGlobalDataClass.m
//  iRestaurants Universal
//
//  Created by Dex on 04/01/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "iRestaurantsGlobalDataClass.h"

@implementation iRestaurantsGlobalDataClass

@synthesize orderType,waiterName,cateringQuantity,orderPlaced_new;
@synthesize itemNameArray,itemPriceArray,itemQuantityArray,tableBookedArr_bar,tableBookedArr_food;
static iRestaurantsGlobalDataClass *instance =nil;
+(iRestaurantsGlobalDataClass *) getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [iRestaurantsGlobalDataClass new];
        }
    }
    return instance;
}

-(id)init
{
    if(self = [super init]){
        
        NSLog(@"creating array");
        tableBookedArr_food=[[NSMutableArray alloc] init];
          tableBookedArr_bar=[[NSMutableArray alloc] init];
        _DicProcessCS10 = [[NSMutableDictionary alloc]init];

    }
    
    return self;
    
}
@end
