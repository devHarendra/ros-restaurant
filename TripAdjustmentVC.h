//
//  TripAdjustmentVC.h
//  iROS
//
//  Created by iMac Apple on 16/02/15.
//  Copyright (c) 2015 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TripAdjustmentVC : UIViewController<UITableViewDelegate, UITableViewDataSource,ServerConnectionDelegate>
{
    NSArray  *orderList;
}

@property (retain, nonatomic) ServerConnectionClass*  connObject;
@end
