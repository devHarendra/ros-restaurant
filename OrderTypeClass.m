//
//  OrderTypeClass.m
//  iROS
//
//  Created by Dex on 15/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "OrderTypeClass.h"

@implementation OrderTypeClass

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:0.8];
        
        UIView *bgVw=[[UIView alloc] init];
        self.bgVw=bgVw;
        [bgVw release];
        [self.bgVw setBackgroundColor:[UIColor whiteColor]];
        [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-500)/2, ([UIScreen mainScreen].bounds.size.height-200)/2,500 , 200)];
        [self addSubview:self.bgVw];
        [self.bgVw.layer setBorderColor:[UIColor colorWithRed:77.0f/255.0f green:77.0f/255.0f blue:77.0f/255.0f alpha:1].CGColor];
        [self.bgVw.layer setBorderWidth:6.0f];
        [self.bgVw.layer setCornerRadius:4.0f];
        
        self.closeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeBtn setFrame:CGRectMake( self.bgVw.frame.size.width-60,1, 60, 60)];
        [self.closeBtn setBackgroundColor:[UIColor clearColor]];
        [self.closeBtn setExclusiveTouch:YES];
        [self.bgVw addSubview:self.closeBtn];
        [self.closeBtn setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        
        
        UILabel *popupHeader=[[UILabel alloc] init];
        [popupHeader setFrame:CGRectMake(0, 0, self.bgVw.frame.size.width, 60)];
        [popupHeader setBackgroundColor:[UIColor clearColor]];
        [popupHeader setTextAlignment:NSTextAlignmentCenter];
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [popupHeader setTextColor:kDarkBrownColor];
        }
        else
        {
            [popupHeader setTextColor:kOrangeColor];
        }
        [popupHeader setText:@"Order Type"];
        [popupHeader setFont:[UIFont systemFontOfSize:25]];
        [self.bgVw addSubview:popupHeader];
        [popupHeader release];
        
        UILabel *popupHeaderlbl=[[UILabel alloc] init];
        [popupHeaderlbl setFrame:CGRectMake(30, popupHeader.frame.origin.y+50, self.bgVw.frame.size.width-60, 40)];
        [popupHeaderlbl setBackgroundColor:[UIColor clearColor]];
        [popupHeaderlbl setTextAlignment:NSTextAlignmentLeft];
        [popupHeaderlbl setTextColor:[UIColor darkGrayColor]];
        [popupHeaderlbl setText:@"Please select your preference:"];
        [popupHeaderlbl setFont:[UIFont systemFontOfSize:18]];
        [self.bgVw addSubview:popupHeaderlbl];
        [popupHeaderlbl release];
        
        UIView *viewLine=[[UIView alloc] init];
        [viewLine setBackgroundColor:[UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1]];
        [viewLine setFrame:CGRectMake(30, popupHeaderlbl.frame.origin.y+40, self.bgVw.frame.size.width-60, 1)];
        [self.bgVw addSubview:viewLine];
        [viewLine release];
        
        self.takeAwayBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.takeAwayBtn setFrame:CGRectMake(30, viewLine.frame.origin.y+30, 160, 40)];
        [self.takeAwayBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 0.0f, 0.0f)];
        [self.takeAwayBtn setTitle:@"Take Away" forState:UIControlStateNormal];
        [self.takeAwayBtn setBackgroundColor:[UIColor clearColor]];
        [self.takeAwayBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [self.takeAwayBtn setExclusiveTouch:YES];
        [self.takeAwayBtn setSelected:NO];
        [self.bgVw addSubview:self.takeAwayBtn];
        UIImageView *imgvw2=[[UIImageView alloc] init];
        [imgvw2 setFrame:CGRectMake(-17, -14, 70, 70)];
        [imgvw2 setImage:[UIImage imageNamed:@"radio_off"]];
        [self.takeAwayBtn addSubview:imgvw2];
        [imgvw2 release];
        
        self.dineInBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.dineInBtn setFrame:CGRectMake(190, viewLine.frame.origin.y+30, 140, 40)];
        [self.dineInBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 0.0f, 0.0f)];
        [self.dineInBtn setTitle:@"Dine In" forState:UIControlStateNormal];
        [self.dineInBtn setBackgroundColor:[UIColor clearColor]];
        [self.dineInBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [self.dineInBtn setExclusiveTouch:YES];
        [self.dineInBtn setSelected:NO];
        [self.bgVw addSubview:self.dineInBtn];
        UIImageView *imgvw3=[[UIImageView alloc] init];
        [imgvw3 setFrame:CGRectMake(-17, -14, 70, 70)];
        [imgvw3 setImage:[UIImage imageNamed:@"radio_off"]];
        [self.dineInBtn addSubview:imgvw3];
        [imgvw3 release];
        
        self.barBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.barBtn setFrame:CGRectMake(325, viewLine.frame.origin.y+30, 115, 40)];
        [self.barBtn setTitleEdgeInsets:UIEdgeInsetsMake(5.0f, 5.0f, 0.0f, 0.0f)];
        [self.barBtn setTitle:@"Bar" forState:UIControlStateNormal];
        [self.barBtn setBackgroundColor:[UIColor clearColor]];
        [self.barBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [self.barBtn setExclusiveTouch:YES];
        [self.barBtn setSelected:NO];
        [self.bgVw addSubview:self.barBtn];
        UIImageView *imgvw4=[[UIImageView alloc] init];
        [imgvw4 setFrame:CGRectMake(-17, -14, 70, 70)];
        [imgvw4 setImage:[UIImage imageNamed:@"radio_off"]];
        [self.barBtn addSubview:imgvw4];
        [imgvw4 release];
    }
    return self;
}



@end
