//
//  MenuViewController.h
//  iROS
//
//  Created by Dex on 25/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailsClass.h"
#import "CurrentOrderList.h"
#import "Category_ItemDetail.h"
#import "UpdateOrderDetails.h"
#import "OrderTypeClass.h"
#import "OpenFoodClass.h"

@interface MenuViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate,ServerConnectionDelegate,NSURLConnectionDelegate, NSURLConnectionDataDelegate,UIAlertViewDelegate>
{
    UITableView         *tableViewOptions;
    
   // NSMutableDictionary *sectionContentDict;
   // NSMutableArray      *arrayForBool;
    UIScrollView        *scrollView_MenuItems;
    
    NSInteger numberofNewsArticle;
    NSArray* numberofSubNewsArticle;
    UILabel *pageNumber;
  
    UILabel *navbartitle;
    //NSMutableArray *allItems;
    UISlider *mySlider;
    UILabel *popView;
    UIImageView *imageView;
    
    NSArray *arrSectionHeader;
   // NSMutableArray *arrWine;
    //NSMutableArray *arrRowsTitle;
    
    int popUp_itemDetails;
    int popUp_scrollItems;
    int popUp_currentOrderList;
    int popUp_updateOrderList;
    BOOL popUp_openFoodOrAlcohalShown;
    int popUp_orderType;
    
    
    UIView *footerVw1;
    UIView *footerVw;
    UIView *leftVw;
    UIButton * homeBtn;
    UIButton * newOrderBtn;
    UISearchBar * theSearchBar;
    
    NSMutableArray *categoryArray_food;
    NSMutableArray *categoryArray_bar;
  
    
    NSMutableArray *cartItemsArr;
    
    Category_ItemDetail *itemDetailCls;
    OrderDetailsClass *itemDetails ;
    UpdateOrderDetails *updateItem;
    NSString *color;

    NSMutableData*  _responseData;
    NSMutableDictionary *ofoodAlcoDic;
    
    
    UIImageView *img;
    UIImageView *img1;
}
@property (nonatomic, retain) OrderDetailsClass *uiasView_itemDetails;
@property (nonatomic, retain) CurrentOrderList *uiasView_currentOrderList;
@property (nonatomic, retain) UpdateOrderDetails *uiasView_updateOrderList;
@property (nonatomic, retain) OpenFoodClass *uiasView_OpenFoodClass;
@property (strong,nonatomic)  NSMutableArray *filteredArray_bar;
@property (strong,nonatomic)  NSMutableArray *filteredArray_food;
@property (strong,nonatomic)  NSMutableArray *categoryArray_food1;
@property (strong,nonatomic)  NSMutableArray *categoryArray_bar1;
@property (nonatomic,retain)  NSString* tableID;
@property (nonatomic,retain)  NSString *fromClass;
@property (strong,nonatomic)  NSMutableArray *cartItemUpdateArr;
@property (retain, nonatomic) ServerConnectionClass*  connObject;
@property (nonatomic, retain) NSDictionary *itemDetailDict;
@property (nonatomic, assign) NSString *currentCartPrice;
@property (nonatomic, assign) NSString *currentCartPriceWithTax;

@property (nonatomic, retain) OrderTypeClass *uiasView_orderType;
@property (strong,nonatomic)  NSMutableArray *cartItemsArr_toupdate;

@property (strong,nonatomic) UIRefreshControl *refreshControl ;
@end
