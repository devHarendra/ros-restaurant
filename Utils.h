//
//  Utils.h
//  MyCabuUserIPhone
//
//  Created by iMac Apple on 29/01/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (void) showAlertView :(NSString*)title message:(NSString*)msg delegate:(id)delegate cancelButtonTitle:(NSString*)CbtnTitle otherButtonTitles:(NSString*)otherBtnTitles;

+ (BOOL) validateEmail: (NSString *) candidate;


//// Method on App delegate objects/////
+ (void)startActivityIndicator;
+ (void)stopActivityIndicator;


+ (NSString* )valueForKey:(NSString*)key;
+ (NSString *)deviceid;

+ (BOOL) connected;

+ (UIImage *)makeRoundedImage:(UIImage *) image radius: (float) radius;
+ (UIImage*) buttonImageColor:(UIColor*)color frame:(CGRect)frame;

@end
