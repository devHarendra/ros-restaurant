//
//  UpdateOrderDetails.m
//  iROS
//
//  Created by Dex on 31/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "UpdateOrderDetails.h"
#import "Define.h"

@implementation UpdateOrderDetails

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:0.8];
        
        UIView *dummyView=[[UIView alloc] init];
        [dummyView setBackgroundColor:[UIColor whiteColor]];
        [dummyView setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-600)/2, ([UIScreen mainScreen].bounds.size.height-380)/2,600 , 380)];
        self.bgVw=dummyView;
        [dummyView release];
        [self.bgVw.layer setBorderColor:[UIColor colorWithRed:77.0f/255.0f green:77.0f/255.0f blue:77.0f/255.0f alpha:1].CGColor];
        [self.bgVw.layer setBorderWidth:6.0f];
        [self.bgVw.layer setCornerRadius:4.0f];
        [self addSubview:self.bgVw];
        
        
        
        self.closeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeBtn setFrame:CGRectMake( self.bgVw.frame.size.width-60,1, 60, 60)];
        [self.closeBtn setBackgroundColor:[UIColor clearColor]];
        [self.closeBtn setExclusiveTouch:YES];
        [self.bgVw addSubview:self.closeBtn];
        [self.closeBtn setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        
        
        UILabel *modifierLbl=[[UILabel alloc] init];
        [modifierLbl setFrame:CGRectMake(20, 30, 100, 50)];
        [modifierLbl setBackgroundColor:[UIColor clearColor]];
        [modifierLbl setTextAlignment:NSTextAlignmentLeft];
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [modifierLbl setTextColor:kDarkBrownColor];
        }
        else
        {
            [modifierLbl setTextColor:kOrangeColor];
        }

        
        [modifierLbl setText:@"Modifier:"];
        [modifierLbl setFont:[UIFont systemFontOfSize:18]];
        [self.bgVw addSubview:modifierLbl];
        [modifierLbl release];
        
         modifier1Btn=[UIButton buttonWithType:UIButtonTypeCustom];
        [modifier1Btn setFrame:CGRectMake(158, 35, 40, 40)];
        [modifier1Btn setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
        [modifier1Btn setBackgroundImage:[UIImage imageNamed:@"check_box_active"] forState:UIControlStateSelected];
        [modifier1Btn addTarget:self action:@selector(setModifier1:) forControlEvents:UIControlEventTouchUpInside];
        [modifier1Btn setTag:10];
        [self.bgVw addSubview:modifier1Btn];
      

        UILabel *modifier1=[[UILabel alloc] init];
        [modifier1 setFrame:CGRectMake(198, 35, 100, 40)];
        [modifier1 setBackgroundColor:[UIColor clearColor]];
        [modifier1 setTextAlignment:NSTextAlignmentCenter];
        [modifier1 setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
        [modifier1 setText:@"Modifier1"];
        [modifier1 setFont:[UIFont systemFontOfSize:15]];
        [self.bgVw addSubview:modifier1];
        [modifier1 release];
        
        modifier2Btn=[UIButton buttonWithType:UIButtonTypeCustom];
        [modifier2Btn setFrame:CGRectMake(370, 35, 40, 40)];
        [modifier2Btn setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
        [modifier2Btn setBackgroundImage:[UIImage imageNamed:@"check_box_active"] forState:UIControlStateSelected];
        [modifier2Btn addTarget:self action:@selector(setModifier1:) forControlEvents:UIControlEventTouchUpInside];
        [self.bgVw addSubview:modifier2Btn];
        [modifier2Btn setTag:20];
      
        
        
        
        UILabel *modifier2=[[UILabel alloc] init];
        [modifier2 setFrame:CGRectMake(415, 35, 100, 40)];
        [modifier2 setBackgroundColor:[UIColor clearColor]];
        [modifier2 setTextAlignment:NSTextAlignmentCenter];
        [modifier2 setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
        [modifier2 setText:@"Modifier2"];
        [modifier2 setFont:[UIFont systemFontOfSize:15]];
        [self.bgVw addSubview:modifier2];
        [modifier2 release];
        
        
        
        
         modifier3Btn=[UIButton buttonWithType:UIButtonTypeCustom];
        [modifier3Btn setFrame:CGRectMake(158, 95, 40, 40)];
        [modifier3Btn setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
        [modifier3Btn setBackgroundImage:[UIImage imageNamed:@"check_box_active"] forState:UIControlStateSelected];
        [modifier3Btn addTarget:self action:@selector(setModifier1:) forControlEvents:UIControlEventTouchUpInside];
        [self.bgVw addSubview:modifier3Btn];
        [modifier3Btn setTag:30];
       
        
        UILabel *modifier3=[[UILabel alloc] init];
        [modifier3 setFrame:CGRectMake(198, 95, 100, 40)];
        [modifier3 setBackgroundColor:[UIColor clearColor]];
        [modifier3 setTextAlignment:NSTextAlignmentCenter];
        [modifier3 setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
        [modifier3 setText:@"Modifier3"];
        [modifier3 setFont:[UIFont systemFontOfSize:15]];
        [self.bgVw addSubview:modifier3];
        [modifier3 release];
        
        modifier4Btn=[UIButton buttonWithType:UIButtonTypeCustom];
        [modifier4Btn setFrame:CGRectMake(370, 95, 40, 40)];
        [modifier4Btn setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
        [modifier4Btn setBackgroundImage:[UIImage imageNamed:@"check_box_active"] forState:UIControlStateSelected];
        [modifier4Btn addTarget:self action:@selector(setModifier1:) forControlEvents:UIControlEventTouchUpInside];
        [self.bgVw addSubview:modifier4Btn];
        [modifier4Btn setTag:40];
        
        
        UILabel *modifier4=[[UILabel alloc] init];
        [modifier4 setFrame:CGRectMake(415, 95, 100, 40)];
        [modifier4 setBackgroundColor:[UIColor clearColor]];
        [modifier4 setTextAlignment:NSTextAlignmentCenter];
        [modifier4 setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
        [modifier4 setText:@"Modifier4"];
        [modifier4 setFont:[UIFont systemFontOfSize:15]];
        [self.bgVw addSubview:modifier4];
        [modifier4 release];
        
        
        UILabel *complimentaryLbl=[[UILabel alloc] init];
        [complimentaryLbl setFrame:CGRectMake(20, 165, 130, 40)];
        [complimentaryLbl setBackgroundColor:[UIColor clearColor]];
        [complimentaryLbl setNumberOfLines:1];
        [complimentaryLbl setTextAlignment:NSTextAlignmentLeft];
      
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [complimentaryLbl setTextColor:kDarkBrownColor];
        }
        else
        {
             [complimentaryLbl setTextColor:kOrangeColor];
        }
        [complimentaryLbl setText:@"Complimentary:"];
        [complimentaryLbl setFont:[UIFont systemFontOfSize:15]];
        [self.bgVw addSubview:complimentaryLbl];
        [complimentaryLbl release];
        
        complimentaryBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [complimentaryBtn setFrame:CGRectMake(158, 165, 40, 40)];
        [complimentaryBtn setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
        [complimentaryBtn setBackgroundImage:[UIImage imageNamed:@"check_box_active"] forState:UIControlStateSelected];
        [complimentaryBtn addTarget:self action:@selector(setModifier2:) forControlEvents:UIControlEventTouchUpInside];
        [self.bgVw addSubview:complimentaryBtn];
        
        slideVw=[[UIView alloc] init];
        [slideVw setFrame:CGRectMake(222, 165, 300, 40)];
        [slideVw setBackgroundColor:[UIColor clearColor]];
        [self.bgVw addSubview:slideVw];
        [slideVw release];
       
        
        UILabel *managerLbl=[[UILabel alloc] init];
        [managerLbl setFrame:CGRectMake(0, 0, 160, 40)];
        [managerLbl setBackgroundColor:[UIColor clearColor]];
        [managerLbl setTextAlignment:NSTextAlignmentLeft];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [managerLbl setTextColor:kDarkBrownColor];
        }
        else
        {
           [managerLbl setTextColor:kOrangeColor];
        }
        
        [managerLbl setText:@"Enter Manager's ID:"];
        [managerLbl setFont:[UIFont systemFontOfSize:16]];
        [slideVw addSubview:managerLbl];
        [managerLbl release];
        
        managerIdTxtFld=[[UITextField alloc] init];
        [managerIdTxtFld setDelegate:self];
        [managerIdTxtFld setFrame:CGRectMake(160, 0, 130, 40)];
        [managerIdTxtFld.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [managerIdTxtFld.layer setBorderWidth:1.0f];
        [managerIdTxtFld setFont:[UIFont systemFontOfSize:18]];
        [managerIdTxtFld setTag:20];
        [slideVw addSubview:managerIdTxtFld];
        [managerIdTxtFld release];
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 30)];// view added for padding
        managerIdTxtFld.leftView = paddingView;
        [paddingView release];
        
        UILabel *instructionLbl=[[UILabel alloc] init];
        [instructionLbl setFrame:CGRectMake(20, 230, 250, 70)];
        [instructionLbl setBackgroundColor:[UIColor clearColor]];
        [instructionLbl setTextAlignment:NSTextAlignmentLeft];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [instructionLbl setTextColor:kDarkBrownColor];
        }
        else
        {
            [instructionLbl setTextColor:kOrangeColor];
        }
        
        [instructionLbl setText:@"Instructions:"];
        [complimentaryLbl setFont:[UIFont systemFontOfSize:18]];
        [self.bgVw addSubview:instructionLbl];
        [instructionLbl release];
        
        
        UITextView *instructionTxt=[[UITextView alloc] init];
        self.instructionTxt=instructionTxt;
        [instructionTxt release];
        [self.instructionTxt setFrame:CGRectMake(160, 230, 355, 70)];
        [self.bgVw addSubview:self.instructionTxt];
        [self.instructionTxt.layer setBorderWidth:1.0f];
        [self.instructionTxt.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [self.instructionTxt setFont:[UIFont systemFontOfSize:18]];
        [self.instructionTxt setDelegate:self];
        [self.instructionTxt setTextColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1]];
        
        
        
        
        self.UpdateBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [self.UpdateBtn setFrame:CGRectMake(self.bgVw.frame.size.width/2-60, self.bgVw.frame.size.height-60, 120, 40)];
        [self.UpdateBtn setBackgroundColor:[UIColor clearColor]];
        [self.UpdateBtn setTitle:@"Update" forState:UIControlStateNormal];
        
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [self.UpdateBtn setTitleColor:kDarkBrownColor forState:UIControlStateNormal];
            [self.UpdateBtn.layer setBorderColor:kDarkBrownColor.CGColor];
        }
        else
        {
            [self.UpdateBtn setTitleColor:kOrangeColor forState:UIControlStateNormal];
            [self.UpdateBtn.layer setBorderColor:kOrangeColor.CGColor];
            
        }
        [self.UpdateBtn.layer setBorderWidth:1.0f];
        [self.UpdateBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
        [self.bgVw addSubview:self.UpdateBtn];

        

    }
    return self;
}
-(void) setUI
{
    if ([self.isComplimentary isEqualToString:@"Yes"]) {
        [complimentaryBtn setSelected:YES];
    }
    else
    {
        [complimentaryBtn setSelected:NO];
    }
    

    if ([self.isComplimentary isEqualToString:@"Yes"]) {
        [slideVw setHidden:NO];
        managerIdTxtFld.text= self.managerID;
    }
    else
    {
        [slideVw setHidden:YES];
        self.managerID=@"";
    }
    if ([self.isModifier4 isEqualToString:@"Yes"]) {
        [modifier4Btn setSelected:YES];
    }
    else
    {
        [modifier4Btn setSelected:NO];
    }
    if ([self.isModifier3 isEqualToString:@"Yes"]) {
        [modifier3Btn setSelected:YES];
    }
    else
    {
        [modifier3Btn setSelected:NO];
    }
    if ([self.isModifier1 isEqualToString:@"Yes"]) {
        [modifier1Btn setSelected:YES];
    }
    else
    {
        [modifier1Btn setSelected:NO];
    }
    if ([self.isModifier2 isEqualToString:@"Yes"]) {
        [modifier2Btn setSelected:YES];
    }
    else
    {
        [modifier2Btn setSelected:NO];
    }
}
-(void) setModifier1:(id) sender
{
    
    if ([sender isSelected]) {
        [sender setSelected:NO];
        if ([sender tag]==10) {
            self.isModifier1=@"No";
        }
        else  if ([sender tag]==20) {
            self.isModifier2=@"No";
        }
        else  if ([sender tag]==30) {
            self.isModifier3=@"No";
        }
        else  if ([sender tag]==40) {
            self.isModifier4=@"No";
        }
    }
    else
    {
        [sender setSelected:YES];
        if ([sender tag]==10) {
            self.isModifier1=@"Yes";
        }
        else  if ([sender tag]==20) {
            self.isModifier2=@"Yes";
        }
        else  if ([sender tag]==30) {
            self.isModifier3=@"Yes";
        }
        else  if ([sender tag]==40) {
            self.isModifier4=@"Yes";
        }
        
    }
    
}
-(void) setModifier2:(id) sender
{
    
    if ([sender isSelected]) {
        [sender setSelected:NO];
        self.isComplimentary=@"No";
        [slideVw setHidden:YES];
    }
    else
    {
        [sender setSelected:YES];
        self.isComplimentary=@"Yes";
       [slideVw setHidden:NO];
    }
    
}


-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.height-600)/2, ([UIScreen mainScreen].bounds.size.width-350)/2-100,600 , 350)];
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.height-600)/2, ([UIScreen mainScreen].bounds.size.width-350)/2,600 , 350)];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag==10) {
        [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.height-600)/2, ([UIScreen mainScreen].bounds.size.width-350)/2-190,600 , 350)];
    } else {
        [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.height-600)/2, ([UIScreen mainScreen].bounds.size.width-350)/2-100,600 , 350)];
    }
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.bgVw setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.height-600)/2, ([UIScreen mainScreen].bounds.size.width-350)/2,600 , 350)];
    self.managerID=textField.text;
}

@end
