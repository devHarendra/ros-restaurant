//
//  MenuViewController.m
//  iROS
//
//  Created by Dex on 25/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "MenuViewController.h"
#import "HomeViewController.h"
#import "LayoutViewController.h"
#import "Category_menu.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "OpenFoodClass.h"
@interface MenuViewController ()

@end

@implementation MenuViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *notificationName = @"OrderItemUpdateNotification";
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(useNotificationWithString:) name:notificationName object:nil];
    
    NSString *notificationName_delete = @"OrderItemDeleteNotification";
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(deleteItemNotification:) name:notificationName_delete object:nil];
    
	[self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationController setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    cartItemsArr=[[NSMutableArray alloc] init];
  
    
    
    arrSectionHeader=[[NSArray arrayWithObjects:@"THE FOOD",@"THE DRINKS" ,nil] retain];
    if (![Utils connected])
    {
        [Utils showAlertView:@"Network Error" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    else
    {
        ServerConnectionClass* network = [[[ServerConnectionClass alloc]init] autorelease];
        self.connObject = network;
        self.connObject.delegate = self;
        NSString* urlString=[[NSString alloc]initWithFormat:@"%@{\"status\":\"1\" }",kCategoryUrl];
        [self.connObject makeRequestForUrl:urlString];
        [urlString release];
    }
    
    
    
    leftVw=[[UIView alloc] init];
    [leftVw setFrame:CGRectMake(0, 0, 300, self.view.frame.size.height-50)];
    [self.view addSubview:leftVw];
    [leftVw setBackgroundColor:kOrangeColor];
    [leftVw release];
    
    UIImageView *imgView=[[UIImageView alloc] init];
    [imgView setFrame:CGRectMake(50, 0, 200, 95)];
    [imgView setBackgroundColor:[UIColor clearColor]];
    [imgView setImage:[UIImage imageNamed:@"logo2.png"]];
    [leftVw addSubview:imgView];
    [imgView release];
    
    theSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(1,115,298,40)]; // frame has no effect.
    theSearchBar.delegate = self;
    [theSearchBar setPlaceholder:@"Search"];
    [theSearchBar setBarStyle:UIBarStyleDefault];
    [theSearchBar.layer setBorderColor:[UIColor colorWithRed:234/255.0f  green:122/255.0f blue:77/255.0f alpha:1.0].CGColor];
    [theSearchBar.layer setBorderWidth:1.0f];
    theSearchBar.barTintColor=[UIColor colorWithRed:234/255.0f  green:122/255.0f blue:77/255.0f alpha:1.0];
    [theSearchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@" "] forState:UIControlStateNormal];
    theSearchBar.showsCancelButton = YES;
    [leftVw addSubview:theSearchBar];
    [theSearchBar release];
    
    
    UIView *view = [theSearchBar.subviews objectAtIndex:0];
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[UIButton class]])
        {
            UIButton *cancelButton = (UIButton *)subView;
            [cancelButton.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        }
    }
    
    
    
    
    tableViewOptions=[[UITableView alloc]init];
    tableViewOptions.frame=CGRectMake(0, 170,300,leftVw.frame.size.height-170);
    tableViewOptions.dataSource=self;
    tableViewOptions.delegate=self;
    tableViewOptions.separatorColor=[UIColor colorWithRed:230/255.0f  green:112/255.0f blue:65/255.0f alpha:1];
    tableViewOptions.backgroundColor=[UIColor clearColor];
    [leftVw addSubview:tableViewOptions];
    [tableViewOptions release];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        init];
    refreshControl.tintColor = [UIColor whiteColor];
    [refreshControl addTarget:self action:@selector(refreshMenu) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    [tableViewOptions addSubview:self.refreshControl];
    [refreshControl release];
    
    homeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [homeBtn setFrame:CGRectMake(5, leftVw.frame.size.height+10, 135, 40)];
    [homeBtn setBackgroundColor:kOrangeColor];
    [homeBtn setTitle:@"Home" forState:UIControlStateNormal];
    [homeBtn.titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
    [homeBtn addTarget:self action:@selector(homeBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:homeBtn];
    
    
    newOrderBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [newOrderBtn setFrame:CGRectMake(160, leftVw.frame.size.height+10, 140, 40)];
    [newOrderBtn setBackgroundColor:kOrangeColor];
    [newOrderBtn setTitle:@"New order" forState:UIControlStateNormal];
    [newOrderBtn.titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
    [newOrderBtn addTarget:self action:@selector(newOrderClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:newOrderBtn];
    
    footerVw=[[UIView alloc] init];
    [footerVw setFrame:CGRectMake(311, leftVw.frame.size.height+10, self.view.frame.size.width-331, 40)];
    [self.view addSubview:footerVw];
    [footerVw setBackgroundColor:kOrangeColor];
    [footerVw release];
    
    UIImageView *imgVw=[[UIImageView alloc] init];
    [imgVw setImage:[UIImage imageNamed:@"dine_icon"]];
    [imgVw setFrame:CGRectMake(10 , 0, 40, 40)];
    [footerVw addSubview:imgVw];
    [imgVw release];
    
    
    UILabel *orderlistLbl=[[UILabel alloc] init];
    orderlistLbl.frame=CGRectMake(60, 0, 180, 40);
    [orderlistLbl setText:@"Current order list"];
    [orderlistLbl setTextAlignment:NSTextAlignmentLeft];
    [orderlistLbl setBackgroundColor:[UIColor clearColor]];
    [orderlistLbl setFont:[UIFont systemFontOfSize:16.0f]];
    [orderlistLbl setTextColor:[UIColor whiteColor]];
    [footerVw addSubview:orderlistLbl];
    [orderlistLbl release];
    
    
    UILabel *currentPriceLbl=[[UILabel alloc] init];
    currentPriceLbl.frame=CGRectMake(250, 5, 100, 30);
    [currentPriceLbl setText:@"$0.00"];
    [currentPriceLbl setTextAlignment:NSTextAlignmentCenter];
    [currentPriceLbl setBackgroundColor:[UIColor clearColor]];
    [currentPriceLbl setFont:[UIFont boldSystemFontOfSize:16.0f]];
    [currentPriceLbl setTextColor:[UIColor whiteColor]];
    [footerVw addSubview:currentPriceLbl];
    [currentPriceLbl.layer setBorderWidth:1.0f];
    [currentPriceLbl.layer setBorderColor:[UIColor whiteColor].CGColor];
    [currentPriceLbl release];
    
    UIButton *currentOrderBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [currentOrderBtn setFrame:CGRectMake(0, 0, footerVw.frame.size.width, 40)];
    [currentOrderBtn setBackgroundColor:[UIColor clearColor]];
    [currentOrderBtn addTarget:self action:@selector(currentOrderList:) forControlEvents:UIControlEventTouchUpInside];
    [footerVw addSubview:currentOrderBtn];
    
    
    footerVw1=[[UIView alloc] init];
    [footerVw1 setFrame:CGRectMake(311, leftVw.frame.size.height+10, self.view.frame.size.width-331, 40)];
    [self.view addSubview:footerVw1];
    [footerVw1 setBackgroundColor:kOrangeColor];
    [footerVw1 setHidden:YES];
    [footerVw1 release];
    
    UIImageView *imgVw1=[[UIImageView alloc] init];
    [imgVw1 setImage:[UIImage imageNamed:@"dine_icon"]];
    [imgVw1 setFrame:CGRectMake(10 , 0, 40, 40)];
    [footerVw1 addSubview:imgVw1];
    [imgVw1 release];
    
    UILabel *orderlistLbl1=[[UILabel alloc] init];
    orderlistLbl1.frame=CGRectMake(60, 0, 180, 40);
    [orderlistLbl1 setText:@"Items list"];
    [orderlistLbl1 setTextAlignment:NSTextAlignmentLeft];
    [orderlistLbl1 setBackgroundColor:[UIColor clearColor]];
    [orderlistLbl1 setFont:[UIFont systemFontOfSize:16.0f]];
    [orderlistLbl1 setTextColor:[UIColor whiteColor]];
    [footerVw1 addSubview:orderlistLbl1];
    [orderlistLbl1 release];
    
    
    UILabel *currentPriceLbl1=[[UILabel alloc] init];
    currentPriceLbl1.frame=CGRectMake(650, 5, 30, 30);
    [currentPriceLbl1 setText:@"+"];
    [currentPriceLbl1 setTextAlignment:NSTextAlignmentCenter];
    [currentPriceLbl1 setBackgroundColor:[UIColor clearColor]];
    [currentPriceLbl1 setFont:[UIFont boldSystemFontOfSize:20.0f]];
    [currentPriceLbl1 setTextColor:[UIColor whiteColor]];
    [footerVw1 addSubview:currentPriceLbl1];
    [currentPriceLbl1 release];
    
    UIButton *currentOrderBtn1=[UIButton buttonWithType:UIButtonTypeCustom];
    [currentOrderBtn1 setFrame:CGRectMake(0, 0, footerVw1.frame.size.width, 40)];
    [currentOrderBtn1 setBackgroundColor:[UIColor clearColor]];
    [currentOrderBtn1 addTarget:self action:@selector(currentOrderList1:) forControlEvents:UIControlEventTouchUpInside];
    [footerVw1 addSubview:currentOrderBtn1];
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    scrollView_MenuItems = [[UIScrollView alloc] init];
    scrollView_MenuItems.frame=CGRectMake(301, 0, self.view.frame.size.width-301, self.view.frame.size.height-50);
    scrollView_MenuItems.backgroundColor = [UIColor clearColor];
    scrollView_MenuItems.showsHorizontalScrollIndicator = NO;
    [scrollView_MenuItems setCanCancelContentTouches:NO];
    scrollView_MenuItems.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView_MenuItems.clipsToBounds = YES;
    scrollView_MenuItems.scrollEnabled = YES;
    scrollView_MenuItems.pagingEnabled=YES;
    scrollView_MenuItems.delegate=self;
    scrollView_MenuItems.tag=222;
    scrollView_MenuItems.contentSize = CGSizeMake(300, 200);
    [self.view addSubview:scrollView_MenuItems];
    [scrollView_MenuItems release];
    
    img=[[UIImageView alloc] init];
    [img setImage:[UIImage imageNamed:@"dinner-menu.jpg"]];
    [img setFrame:CGRectMake(360, (self.view.frame.size.width-50-279)/2, 307, 279)];
    [self.view addSubview:img];
    img1=[[UIImageView alloc] init];
    [img1 setImage:[UIImage imageNamed:@"lunch-menu.jpg"]];
    [img1 setFrame:CGRectMake(670, (self.view.frame.size.width-50-279)/2, 307, 279)];
    [self.view addSubview:img1];
    
    if ([self.fromClass isEqualToString:@"HomeClass"]) {
        CGRect uiasViewFrame   = scrollView_MenuItems.frame;
        uiasViewFrame.origin.x = self.view.frame.size.height+300;
        scrollView_MenuItems.frame = uiasViewFrame;
        
        popUp_currentOrderList=0;
        [UIView animateWithDuration:0.60f animations:^{
            
            CurrentOrderList *currentOrderList ;
            if (!currentOrderList) {
                currentOrderList = [[CurrentOrderList alloc] init];
            }
            
            [self setUiasView_currentOrderList:currentOrderList];
            [currentOrderList release];
            [self.uiasView_currentOrderList setFrame:CGRectMake(311, self.view.frame.size.width , self.view.frame.size.width-332, self.view.frame.size.height-60)];
            CGRect uiasViewFrame   = self.uiasView_currentOrderList.frame;
            uiasViewFrame.origin.y =10;
            uiasViewFrame.origin.x =311;
            self.uiasView_currentOrderList.frame = uiasViewFrame;
            self.uiasView_currentOrderList.cartItems=(NSArray*)self.cartItemsArr_toupdate;
            cartItemsArr=self.cartItemsArr_toupdate;
            self.uiasView_currentOrderList.rowCount=[self.uiasView_currentOrderList.cartItems count];
            [self.uiasView_currentOrderList.tblList1 reloadData];
            [self.uiasView_currentOrderList.updateOrderBtn addTarget:self action:@selector(submitOrderBtn:) forControlEvents:UIControlEventTouchUpInside];
            for (int num=0; num<[self.uiasView_currentOrderList.cartItems count]; num++) {
                NSLog(@"$%@",[[self.uiasView_currentOrderList.cartItems objectAtIndex:num] price]);
                self.currentCartPrice=[NSString stringWithFormat:@"%.2f",[self.currentCartPrice floatValue]+[[[self.uiasView_currentOrderList.cartItems objectAtIndex:num] price] floatValue]];
            }
           
            
   

            self.uiasView_currentOrderList.currentPriceLbl.text=self.currentCartPrice;
            [self.uiasView_currentOrderList.updateOrderBtn setTitle:@"Update Order" forState:UIControlStateNormal];
            [self.view addSubview:self.uiasView_currentOrderList];
            
            
        } completion:^(BOOL finished)
        {
            
            [footerVw1 setHidden:NO];
            [footerVw setHidden:YES];
        }];

        
         [homeBtn setFrame:CGRectMake(0, leftVw.frame.size.height+10, 300, 40)];
         [newOrderBtn setHidden:YES];
    }
    else if ([self.fromClass isEqualToString:@"LayoutClass"])
    {
          self.cartItemUpdateArr=[[NSMutableArray alloc] init];
         [self.uiasView_currentOrderList.updateOrderBtn setTitle:@"Submit Order" forState:UIControlStateNormal];
    }
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    if (![[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
    {
       
            
            [theSearchBar.layer setBorderColor:[UIColor colorWithRed:234/255.0f  green:122/255.0f blue:77/255.0f alpha:1.0].CGColor] ;
            [tableViewOptions setSeparatorColor:[UIColor colorWithRed:230/255.0f  green:112/255.0f blue:65/255.0f alpha:1]];
            theSearchBar.barTintColor=kLightOrangeColor;
            [leftVw setBackgroundColor:kOrangeColor];
            [footerVw setBackgroundColor:kOrangeColor];
            [footerVw1 setBackgroundColor:kOrangeColor];
            [homeBtn setBackgroundColor:kOrangeColor];
            [newOrderBtn setBackgroundColor:kOrangeColor];
            [self.uiasView_currentOrderList.footerVw setBackgroundColor:kOrangeColor];
            color=@"orange";
            
        
    }
    else
    {
        
            
            color=@"brown";
            [self.uiasView_currentOrderList.footerVw setBackgroundColor:[UIColor colorWithRed:121/255.0f green:50/255.0f blue:4/255.0f alpha:1.0]];
            [theSearchBar.layer setBorderColor:[UIColor colorWithRed:142.0/255.0 green:84.0/255.0 blue:46.0/255.0 alpha:1.0].CGColor] ;
            [tableViewOptions setSeparatorColor:[UIColor colorWithRed:142.0/255.0 green:84.0/255.0 blue:46.0/255.0 alpha:1.0]];
            theSearchBar.barTintColor=[UIColor colorWithRed:142.0/255.0 green:84.0/255.0 blue:46.0/255.0 alpha:1.0];
            [leftVw setBackgroundColor:kDarkBrownColor];
            [footerVw setBackgroundColor:kDarkBrownColor];
            [footerVw1 setBackgroundColor:kDarkBrownColor];
            [homeBtn setBackgroundColor:kDarkBrownColor];
            [newOrderBtn setBackgroundColor:kDarkBrownColor];
       
        
    }
}
- (void)updateTable
{
    
    //[tableViewOptions reloadData];
    [self.refreshControl endRefreshing];
}
-(void) refreshMenu
{
    if ([Utils connected])
    {
         NSString* urlString=[[NSString alloc]initWithFormat:@"%@{\"status\":\"1\" }",kCategoryUrl];
        urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        
        NSLog(@"url ---------%@",urlString);
        NSURLConnection* connection =  [[[NSURLConnection alloc] initWithRequest:request  delegate:self] autorelease];
        [request release];
        [connection start];
    }
    else
    {
        [Utils showAlertView:@"Network Error" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _responseData = [[NSMutableData alloc] init];
    _responseData.length = 0;
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	
	[_responseData appendData:data]; //when connection start than  append data to webdata
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"%@",error);
    NSLog(@"Error Occured");
    [self.refreshControl endRefreshing];
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"DONE. Received Bytes: %lu", (unsigned long)[_responseData length]);
    [self.refreshControl endRefreshing];
    NSError *localError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&localError];
    
    NSLog(@"MY dictionary====%@",dictionary);
    if (dictionary != nil)
    {
        if ([[dictionary objectForKey:@"cmd"] isEqualToString:@"items"])
        {
            self.itemDetailDict=[NSDictionary dictionaryWithDictionary:dictionary];
            NSString* result = [dictionary objectForKey:@"result"];
            
            if ([result isEqualToString:@"Success"])
            {
                
                
                categoryArray_food=[[NSMutableArray alloc] init];
                categoryArray_bar=[[NSMutableArray alloc] init];
                self.categoryArray_food1=[[NSMutableArray alloc] init];
                self.categoryArray_bar1=[[NSMutableArray alloc] init];
                self.filteredArray_food =[[NSMutableArray alloc] init];
                self.filteredArray_bar =[[NSMutableArray alloc] init];
                
                
                
                int count_foodCategory=(int)[[dictionary objectForKey:@"food"] count];
                int count_barCategory=(int)[[dictionary objectForKey:@"bar"] count];
                
                
                for (int i=0; i<count_foodCategory; i++)
                {
                    Category_menu *category=[[Category_menu alloc] init];
                    [category setCategoryID:[[[dictionary objectForKey:@"food"] objectAtIndex:i] objectForKey:@"id"]];
                    [category setCategoryName:[[[dictionary objectForKey:@"food"] objectAtIndex:i] objectForKey:@"category_name"]];
                    [categoryArray_food addObject:category];
                    [self.categoryArray_food1 addObject:category];
                    [self.filteredArray_food addObject:category];
                    [category release];
                }

                
                for (int i=0; i<count_barCategory; i++) {
                    Category_menu *category=[[Category_menu alloc] init];
                    [category setCategoryID:[[[dictionary objectForKey:@"bar"] objectAtIndex:i] objectForKey:@"id"]];
                    [category setCategoryName:[[[dictionary objectForKey:@"bar"] objectAtIndex:i] objectForKey:@"category_name"]];
                    [categoryArray_bar addObject:category];
                    [self.categoryArray_bar1 addObject:category];
                    [self.filteredArray_bar addObject:category];
                    [category release];
                }
                
               
                [tableViewOptions reloadData];
                
                
            }
            else
            {
                
                
                [Utils showAlertView:result  message: [dictionary objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
               
            }
            
            
            
        }
    }

    [_responseData release];
    
    
}

#pragma mark  - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [arrSectionHeader count];
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(13, 0, tableView.frame.size.width-20, 55)];
    [label setFont:[UIFont boldSystemFontOfSize:14]];
    label.textColor=[UIColor whiteColor];
    NSString *string =[arrSectionHeader objectAtIndex:section];
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:151/255.0f  green:60/255.0f blue:25/255.0f alpha:1]]; //your background color...
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0)
    {
        return [categoryArray_food count];
    }
    else
    {
        return [categoryArray_bar count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier] ;
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        [cell setBackgroundColor:[UIColor clearColor]];
        cell.textLabel.font=[UIFont systemFontOfSize:15.0];
        [cell.textLabel setTextColor:[UIColor whiteColor]];
        UIView *bgColorView = [[UIView alloc] init];
        [bgColorView setBackgroundColor:[UIColor colorWithRed:234/255.0f  green:122/255.0f blue:77/255.0f alpha:1.0]];
        [cell setSelectedBackgroundView:bgColorView];
        [bgColorView release];
    }
    if(indexPath.section==0)
    {
               cell.textLabel.text=[[categoryArray_food objectAtIndex:indexPath.row] categoryName];
    }
    else
    {
                   cell.textLabel.text=[[categoryArray_bar objectAtIndex:indexPath.row] categoryName];
    }
    
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setBackGroundColor:tableView forIndexPath:indexPath];
    [img setHidden:YES];
    [img1 setHidden:YES];
    
    if (![Utils connected])
    {
        [Utils showAlertView:@"Network Error" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    else
    {
//        [self currentOrderList1:self];
        ServerConnectionClass* network = [[[ServerConnectionClass alloc]init] autorelease];
        self.connObject = network;
        self.connObject.delegate = self;
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        NSString *categoryId=nil;
        if (indexPath.section==0)
        {
            categoryId=[[categoryArray_food objectAtIndex:indexPath.row] categoryID];

            if ([categoryId isEqualToString:@"87878787"])
            {
                [self OpenFoodOrAlcohalClicked];
                return;
            }
            [defaults setObject:@"" forKey:kitemType];
        }
        else
        {
            categoryId=[[categoryArray_bar objectAtIndex:indexPath.row] categoryID];
            if ([categoryId isEqualToString:@"86868686"])
            {
                [self OpenFoodOrAlcohalClicked];
                return;
            }
            [defaults setObject:@"Bar" forKey:kitemType];
        }
        [defaults synchronize];
        NSString* urlString=[[NSString alloc]initWithFormat:@"%@{\"category_id\":\"%@\" }",kItemsUrl,categoryId];
        [self.connObject makeRequestForUrl:urlString];
        [urlString release];
    }
    
    
}

-(void)setBackGroundColor:(UITableView *)tableView forIndexPath:(NSIndexPath*)indexPath
{
    if (indexPath.section==0)
    {
        [UIView animateWithDuration:1.0 animations:^{
            
            [theSearchBar.layer setBorderColor:[UIColor colorWithRed:234/255.0f  green:122/255.0f blue:77/255.0f alpha:1.0].CGColor] ;
            [tableView setSeparatorColor:[UIColor colorWithRed:230/255.0f  green:112/255.0f blue:65/255.0f alpha:1]];
            theSearchBar.barTintColor=kLightOrangeColor;
            [leftVw setBackgroundColor:kOrangeColor];
            [footerVw setBackgroundColor:kOrangeColor];
             [footerVw1 setBackgroundColor:kOrangeColor];
            [homeBtn setBackgroundColor:kOrangeColor];
            [newOrderBtn setBackgroundColor:kOrangeColor];
            [self.uiasView_currentOrderList.footerVw setBackgroundColor:kOrangeColor];
            color=@"orange";
            
        }];
    }
    else
    {
        [UIView animateWithDuration:1.0 animations:^{
            
            color=@"brown";
            [self.uiasView_currentOrderList.footerVw setBackgroundColor:[UIColor colorWithRed:121/255.0f green:50/255.0f blue:4/255.0f alpha:1.0]];
            [theSearchBar.layer setBorderColor:[UIColor colorWithRed:142.0/255.0 green:84.0/255.0 blue:46.0/255.0 alpha:1.0].CGColor] ;
            [tableView setSeparatorColor:[UIColor colorWithRed:142.0/255.0 green:84.0/255.0 blue:46.0/255.0 alpha:1.0]];
            theSearchBar.barTintColor=[UIColor colorWithRed:142.0/255.0 green:84.0/255.0 blue:46.0/255.0 alpha:1.0];
            [leftVw setBackgroundColor:kDarkBrownColor];
            [footerVw setBackgroundColor:kDarkBrownColor];
            [footerVw1 setBackgroundColor:kDarkBrownColor];
            [homeBtn setBackgroundColor:kDarkBrownColor];
            [newOrderBtn setBackgroundColor:kDarkBrownColor];
        }];
        
    }
    
}





#pragma mark - Scroll View Delegate

- (void)scrollViewDidScroll:(UIScrollView *)_scrollView
{
    
    
    CGFloat pageWidth = _scrollView.frame.size.width;
    
    int page = floor((_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) +1;
    
    NSLog(@"page :::%d",page);
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)OpenFoodOrAlcohalClicked
{
    
    
    
    if (!_uiasView_OpenFoodClass) {
        _uiasView_OpenFoodClass = [[OpenFoodClass alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height , self.view.frame.size.width, self.view.frame.size.height)];
    }
    [self.view addSubview:_uiasView_OpenFoodClass];
    [_uiasView_OpenFoodClass.closeBtn addTarget:self action:@selector(OpenFoodOrAlcohalClicked) forControlEvents:UIControlEventTouchUpInside];
     [_uiasView_OpenFoodClass.UpdateBtn addTarget:self action:@selector(OpenFoodOrAlcohalClicked) forControlEvents:UIControlEventTouchUpInside];
        
    if (!popUp_openFoodOrAlcohalShown) {
        [UIView animateWithDuration:0.40f animations:^{
            CGRect uiasViewFrame   = self.uiasView_OpenFoodClass.frame;
            uiasViewFrame.origin.y = 0 ;
            self.uiasView_OpenFoodClass.frame = uiasViewFrame;
          }
            completion:^(BOOL finished)
         {
             popUp_openFoodOrAlcohalShown  = true;
         }];
    }
    else
    {
        [UIView animateWithDuration:0.40f animations:^{
            CGRect uiasViewFrame   = self.uiasView_OpenFoodClass.frame;
            uiasViewFrame.origin.y = self.view.frame.size.height ;
            self.uiasView_OpenFoodClass.frame = uiasViewFrame;
        }
          completion:^(BOOL finished)
         {
             popUp_openFoodOrAlcohalShown  = false;
           [self.uiasView_OpenFoodClass removeFromSuperview];
         }];
        
//        NSUserDefaults *de = [NSUserDefaults standardUserDefaults];
//        [de setObject:@"" forKey:kitemType];
        ofoodAlcoDic = [[NSMutableDictionary alloc]init];
        [ofoodAlcoDic setObject:_uiasView_OpenFoodClass.instructionTxt.text forKey:@"ins"];
        [ofoodAlcoDic setObject:_uiasView_OpenFoodClass.amtTxt.text forKey:@"amt"];

    }
  }



-(void) homeBtnClicked
{
    
    if ([cartItemsArr count]>0) {
       
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You have items in cart to be submitted.\nAre you sure you want to discard it?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        [alertView setTag:30];
        [alertView show];
        [alertView release];
        
    }
    else
    {
        for (UIViewController *controller in self.navigationController.viewControllers) {
            
            
            if ([controller isKindOfClass:[HomeViewController class]]) {
                
                [self.navigationController popToViewController:controller
                                                      animated:YES];
                break;
            }
        }
    }
   
}
-(void) newOrderClicked
{
    // [self.navigationController popViewControllerAnimated:YES];
    if ([cartItemsArr count]>0) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You have items in cart to be submitted.\nAre you sure you want to discard it?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        [alertView setTag:40];
        [alertView show];
        [alertView release];
        
    }
    else
    {
        OrderTypeClass *orderType = [[OrderTypeClass alloc] init];
        [self setUiasView_orderType:orderType];
        [orderType release];
        [self.uiasView_orderType setFrame:CGRectMake(0, self.view.frame.size.width , self.view.frame.size.width, self.view.frame.size.height)];
        [self.uiasView_orderType.closeBtn addTarget:self action:@selector(orderTypeCloseBtnClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.uiasView_orderType.dineInBtn addTarget:self action:@selector(orderTypeDineBtnClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.uiasView_orderType.takeAwayBtn addTarget:self action:@selector(orderTypeTakeAwayBtnClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.uiasView_orderType.barBtn addTarget:self action:@selector(orderTypebarBtnClicked) forControlEvents:UIControlEventTouchUpInside];
        
        popUp_orderType=0;
        // show new order screen
        [UIView animateWithDuration:0.60f animations:^{
            
            
            CGRect uiasViewFrame   = self.uiasView_orderType.frame;
            uiasViewFrame.origin.y =0;
            self.uiasView_orderType.frame = uiasViewFrame;
            [self.view addSubview:self.uiasView_orderType];
            
            
        } completion:^(BOOL finished) {
            
            
        }];
        
    }
}
-(void) orderTypeCloseBtnClicked
{
    popUp_orderType=1;
    [UIView animateWithDuration:0.75f animations:^{
        
        
        CGRect uiasViewFrame   = self.uiasView_orderType.frame;
        uiasViewFrame.origin.y = self.view.frame.size.height ;
        
        self.uiasView_orderType.frame = uiasViewFrame;
        
        
        
    } completion:^(BOOL finished)
     {
         // completion block is important in each case. Use it
         [self.uiasView_orderType removeFromSuperview];
     }];
}

-(void) orderTypeDineBtnClicked
{
    UIImageView *imgvw=(UIImageView*)[[self.uiasView_orderType.dineInBtn subviews] objectAtIndex:0];
    if (![self.uiasView_orderType.dineInBtn isSelected])
    {
        [self.uiasView_orderType.dineInBtn setSelected:YES];
        [imgvw setImage:[UIImage imageNamed:@"radio_on"]];
        
        [self.uiasView_orderType.takeAwayBtn setSelected:NO];
        [self.uiasView_orderType.barBtn setSelected:NO];
        UIImageView *imgvw_split=(UIImageView*)[[self.uiasView_orderType.barBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_off"]];
        UIImageView *imgvw_split1=(UIImageView*)[[self.uiasView_orderType.takeAwayBtn
                                                  subviews] objectAtIndex:0];
        [imgvw_split1 setImage:[UIImage imageNamed:@"radio_off"]];
        
        
        
      
        NSMutableDictionary *dic_itemDetail=[[NSMutableDictionary alloc] init];
        [dic_itemDetail setObject:@"Waiter" forKey:kuserType];
        

        [self.navigationController popViewControllerAnimated:YES];
         [self orderTypeCloseBtnClicked];
         [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshView" object:nil userInfo:dic_itemDetail];
        [dic_itemDetail release];
        
    }
    else
    {
        [self.uiasView_orderType.dineInBtn setSelected:NO];
        [imgvw setImage:[UIImage imageNamed:@"radio_off"]];
        
        
    }
    
}
-(void) orderTypeTakeAwayBtnClicked
{
    UIImageView *imgvw=(UIImageView*)[[self.uiasView_orderType.takeAwayBtn subviews] objectAtIndex:0];
    if (![self.uiasView_orderType.takeAwayBtn isSelected])
    {
        [self.uiasView_orderType.takeAwayBtn setSelected:YES];
        [imgvw setImage:[UIImage imageNamed:@"radio_on"]];
        
        [self.uiasView_orderType.dineInBtn setSelected:NO];
        [self.uiasView_orderType.barBtn setSelected:NO];
        UIImageView *imgvw_split=(UIImageView*)[[self.uiasView_orderType.barBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_off"]];
        UIImageView *imgvw_split1=(UIImageView*)[[self.uiasView_orderType.dineInBtn
                                                  subviews] objectAtIndex:0];
        [imgvw_split1 setImage:[UIImage imageNamed:@"radio_off"]];
        
    }
    else
    {
        [self.uiasView_orderType.takeAwayBtn setSelected:NO];
        [imgvw setImage:[UIImage imageNamed:@"radio_off"]];
        
        
    }
    
}
-(void) orderTypebarBtnClicked
{
    UIImageView *imgvw=(UIImageView*)[[self.uiasView_orderType.barBtn subviews] objectAtIndex:0];
    if (![self.uiasView_orderType.barBtn isSelected])
    {
        [self.uiasView_orderType.barBtn setSelected:YES];
        [imgvw setImage:[UIImage imageNamed:@"radio_on"]];
        
        [self.uiasView_orderType.takeAwayBtn setSelected:NO];
        [self.uiasView_orderType.dineInBtn setSelected:NO];
        UIImageView *imgvw_split=(UIImageView*)[[self.uiasView_orderType.dineInBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_off"]];
        UIImageView *imgvw_split1=(UIImageView*)[[self.uiasView_orderType.takeAwayBtn
                                                  subviews] objectAtIndex:0];
        [imgvw_split1 setImage:[UIImage imageNamed:@"radio_off"]];
        
        
        
       
        NSMutableDictionary *dic_itemDetail=[[NSMutableDictionary alloc] init];
        [dic_itemDetail setObject:@"Bar Tender" forKey:kuserType];
        
        [self.navigationController popViewControllerAnimated:YES];
         [self orderTypeCloseBtnClicked];
         [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshView" object:nil userInfo:dic_itemDetail];
        [dic_itemDetail release];
    }
    else
    {
        [self.uiasView_orderType.barBtn setSelected:NO];
        [imgvw setImage:[UIImage imageNamed:@"radio_off"]];
        
        
    }
    
}

-(void) currentOrderList1:(id)sender
{
    
    popUp_currentOrderList=1;
    [UIView animateWithDuration:0.75f animations:^{
        
        
        CGRect uiasViewFrame   = self.uiasView_currentOrderList.frame;
        uiasViewFrame.origin.y = self.view.frame.size.width ;
        
        self.uiasView_currentOrderList.frame = uiasViewFrame;
        
        
        
    } completion:^(BOOL finished)
     {
         // completion block is important in each case. Use it
         [self.uiasView_currentOrderList removeFromSuperview];
         
         
         
         [footerVw setHidden:NO];
         [footerVw1 setHidden:YES];
         popUp_scrollItems=0;
         // show new order screen
         [UIView animateWithDuration:0.60f animations:^{
             
             
             CGRect uiasViewFrame   = scrollView_MenuItems.frame;
             uiasViewFrame.origin.x = 301;
             scrollView_MenuItems.frame = uiasViewFrame;
             // [self.view addSubview:scrollView_MenuItems];
             //[scrollView_MenuItems release];
             
         } completion:^(BOOL finished) {
             
         }];
         
         
     }];
    
    
    
}
-(void) currentOrderList:(id)sender
{
    if ([cartItemsArr count] >0)
    {
        popUp_scrollItems=1;
        [UIView animateWithDuration:0.75f animations:^{
            
            
            CGRect uiasViewFrame   = scrollView_MenuItems.frame;
            uiasViewFrame.origin.x = self.view.frame.size.height+301 ;
            
            scrollView_MenuItems.frame = uiasViewFrame;
            
            
            
        } completion:^(BOOL finished)
         {
             
             CGRect uiasViewFrame   = scrollView_MenuItems.frame;
             uiasViewFrame.origin.x = self.view.frame.size.height+300;
             scrollView_MenuItems.frame = uiasViewFrame;
             
             popUp_currentOrderList=0;
             [UIView animateWithDuration:0.60f animations:^{
                 
                 CurrentOrderList *currentOrderList ;
                 if (!currentOrderList) {
                     currentOrderList = [[CurrentOrderList alloc] init];
                 }
                 
                 
                 [self setUiasView_currentOrderList:currentOrderList];
                 [currentOrderList release];
                 [self.uiasView_currentOrderList setFrame:CGRectMake(311, self.view.frame.size.width , self.view.frame.size.width-332, self.view.frame.size.height-60)];
                 CGRect uiasViewFrame   = self.uiasView_currentOrderList.frame;
                 uiasViewFrame.origin.y =10;
                 uiasViewFrame.origin.x =311;
                 self.uiasView_currentOrderList.frame = uiasViewFrame;
                 [self.uiasView_currentOrderList setBgColor:color];
                 if ([color isEqualToString:@"brown"]) {
                      [self.uiasView_currentOrderList.footerVw setBackgroundColor:kDarkBrownColor];
                 } else {
                      [self.uiasView_currentOrderList.footerVw setBackgroundColor:kOrangeColor];
                 }
                 self.uiasView_currentOrderList.cartItems=(NSArray*)cartItemsArr;
                 self.uiasView_currentOrderList.rowCount=[self.uiasView_currentOrderList.cartItems count];
                 [self.uiasView_currentOrderList.tblList1 reloadData];
                 [self.uiasView_currentOrderList.updateOrderBtn addTarget:self action:@selector(submitOrderBtn:) forControlEvents:UIControlEventTouchUpInside];
               
                 
                 self.uiasView_currentOrderList.currentPriceLbl.text=self.currentCartPrice;
                 
//                 self.uiasView_currentOrderList.currentPriceLbl.text=@"harendra 2107199090";

                 
                 [self.view addSubview:self.uiasView_currentOrderList];
                 
                 
             } completion:^(BOOL finished) {
                 
                 
                 NSLog(@"SCROLLLVIEW  _______%@",self.itemDetailDict);
                 [footerVw1 setHidden:NO];
                 [footerVw setHidden:YES];
             }];
             
         }];
    }
}
-(void) addItemToCart:(id)sender
{
    itemDetails = [[OrderDetailsClass alloc] init];
    [itemDetails setFrame:CGRectMake(0, self.view.frame.size.width , self.view.frame.size.width, self.view.frame.size.height)];
    [itemDetails.closeBtn addTarget:self action:@selector(itemDetailsCloseBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [itemDetails.AddItemBtn addTarget:self action:@selector(addItem:) forControlEvents:UIControlEventTouchUpInside];
    [itemDetails.AddItemBtn setTag:[sender tag]];
    [itemDetails.itemDetails loadHTMLString:[[[self.itemDetailDict objectForKey:@"data"] objectAtIndex:[sender tag]] objectForKey:@"description"] baseURL:nil];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        
        __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.center = imageView.center;
        activityIndicator.hidesWhenStopped = YES;
        [imageView addSubview:activityIndicator];

        
        [itemDetails.imgView setImageWithURL:[NSURL URLWithString:[[[self.itemDetailDict objectForKey:@"data"] objectAtIndex:[sender tag]] objectForKey:@"photo_url"]]
                            placeholderImage:[UIImage imageNamed:@"settings.png"]
                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {  NSLog(@"image setting up ! ");
                          [activityIndicator removeFromSuperview];
                     }];
        
        
        
        
//        NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[[self.itemDetailDict objectForKey:@"data"] objectAtIndex:[sender tag]] objectForKey:@"photo_url"]]];
//        if (imgData)
//        {
//            UIImage *image = [UIImage imageWithData:imgData];
//            if (image)
//            {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [itemDetails.imgView setImage:image];
//                    
//                });
//            }
//            
//        }
    });
    
    
    itemDetails.popupHeader.text=[[[self.itemDetailDict objectForKey:@"data"] objectAtIndex:[sender tag]] objectForKey:@"title"];
    itemDetails.totalAmtTxtFld.text=[NSString stringWithFormat:@"$%@",[[[self.itemDetailDict objectForKey:@"data"] objectAtIndex:[sender tag]] objectForKey:@"price"]];
    [self setUiasView_itemDetails:itemDetails];
    [itemDetails release];
    NSLog(@"ADD ITEN TO CART :%@",[[[self.itemDetailDict objectForKey:@"data"] objectAtIndex:[sender tag]] objectForKey:@"price"]);
    
    itemDetailCls=[[Category_ItemDetail alloc] init];
    [itemDetailCls setCategoryID:[[[self.itemDetailDict objectForKey:@"data"] objectAtIndex:[sender tag]] objectForKey:@"category_id"]];
    [itemDetailCls setItemID:[[[self.itemDetailDict objectForKey:@"data"] objectAtIndex:[sender tag]] objectForKey:@"id"]];
    [itemDetailCls setItemName:[[[self.itemDetailDict objectForKey:@"data"] objectAtIndex:[sender tag]] objectForKey:@"title"]];
    [itemDetailCls setImgVw:itemDetails.imgView];
    
    
    
    
    popUp_itemDetails=0;
    // show new order screen
    [UIView animateWithDuration:0.60f animations:^{
        
        
        CGRect uiasViewFrame   = self.uiasView_itemDetails.frame;
        uiasViewFrame.origin.y =0;
        self.uiasView_itemDetails.frame = uiasViewFrame;
        
        [self.view addSubview:self.uiasView_itemDetails];
        
        
    } completion:^(BOOL finished) {
        
    }];
    
}
-(void)addItem :(id)sender
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [itemDetailCls setManagerID:itemDetails.managerID];
    [itemDetailCls setModifier1:itemDetails.isModifier1];
    [itemDetailCls setModifier2:itemDetails.isModifier2];
    [itemDetailCls setModifier3:itemDetails.isModifier3];
    [itemDetailCls setModifier4:itemDetails.isModifier4];
    [itemDetailCls setPrice:[NSString stringWithFormat:@"%.2f",([[[[self.itemDetailDict objectForKey:@"data"] objectAtIndex:[sender tag]] objectForKey:@"price"] floatValue] * [itemDetails.qtyNumLbl.text intValue])]];
    [itemDetailCls setQuantity:itemDetails.qtyNumLbl.text];
    [itemDetailCls setComplimentry:itemDetails.isComplimentary];
    [itemDetailCls setSpecialInstruction:itemDetails.instructionTxt.text];
    [itemDetailCls setItemType:[defaults objectForKey:kitemType]];
    
    NSMutableDictionary *dic_itemDetail=[[NSMutableDictionary alloc] init];
    [dic_itemDetail setObject:itemDetails.instructionTxt.text forKey:ksplInstr];
    [dic_itemDetail setObject:itemDetails.isModifier1 forKey:kmodifier1];
    [dic_itemDetail setObject:itemDetails.isModifier2 forKey:kmodifier2];
    [dic_itemDetail setObject:itemDetails.isModifier3 forKey:kmodifier3];
    [dic_itemDetail setObject:itemDetails.isModifier4 forKey:kmodifier4];
    [dic_itemDetail setObject:itemDetails.managerID forKey:kmanagerID];
    [dic_itemDetail setObject:itemDetails.isComplimentary forKey:kComplimentry];
    
    [self.cartItemUpdateArr addObject:dic_itemDetail];
    
    [cartItemsArr addObject:itemDetailCls];
    
    UILabel *lbl=(UILabel*)[[footerVw subviews] objectAtIndex:2];
    [lbl setText:[NSString stringWithFormat:@"$%.2f",(([[[[self.itemDetailDict objectForKey:@"data"] objectAtIndex:[sender tag]] objectForKey:@"price"] floatValue] * [itemDetails.qtyNumLbl.text intValue])+[[lbl.text substringFromIndex:1] floatValue])]];
    self.currentCartPrice=lbl.text;
    
    [self itemDetailsCloseBtnClicked];
    
}
-(void) itemDetailsCloseBtnClicked
{
//    [itemDetailCls release];
    popUp_itemDetails=1;
    [UIView animateWithDuration:0.75f animations:^{
        
        
        CGRect uiasViewFrame   = self.uiasView_itemDetails.frame;
        uiasViewFrame.origin.y = self.view.frame.size.height ;
        
        self.uiasView_itemDetails.frame = uiasViewFrame;
        
        
        
    } completion:^(BOOL finished)
     {
         // completion block is important in each case. Use it
         [self.uiasView_itemDetails removeFromSuperview];
     }];
}

-(void) searchBarSearchButtonClicked: (UISearchBar *) searchBar
{
    [categoryArray_bar removeAllObjects];
    [categoryArray_food removeAllObjects];
    [categoryArray_bar release];
    [categoryArray_food release];
    categoryArray_bar = [[NSMutableArray alloc] init];
    categoryArray_food = [[NSMutableArray alloc] init];
    
    for (int i=0; i < [self.filteredArray_bar count]; i++)
    {
        NSString *arrayString = [[self.filteredArray_bar objectAtIndex: i] categoryName];
        NSString *searchString = searchBar.text;
        
        NSComparisonResult searchResult = [arrayString compare:searchString options:(NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch) range:[arrayString rangeOfString:searchString options:(NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch)]];
        
        if (searchResult == NSOrderedSame)
        {
            [categoryArray_bar addObject: [self.filteredArray_bar objectAtIndex: i]];
            
        }
        else
        {
            
        }
        
        
        if ([categoryArray_bar count] > 0)
        {
            [tableViewOptions reloadData];
        }
        else
        {
            // tableView.hidden = YES;
            // Here we should show the message like “Your searching item is not available” in UILabel. Also we should set NO to UILabel hidden property.
        }
        
    }
    for (int i=0; i < [self.filteredArray_food count]; i++)
    {
        NSString *arrayString = [[self.filteredArray_food objectAtIndex: i] categoryName];
        NSString *searchString = searchBar.text;
        
        NSComparisonResult searchResult = [arrayString compare:searchString options:(NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch) range:[arrayString rangeOfString:searchString options:(NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch)]];
        
        if (searchResult == NSOrderedSame)
        {
            [categoryArray_food addObject: [self.filteredArray_food objectAtIndex: i]];
            
        }
        else
        {
            
        }
        
        
        if ([categoryArray_food count] > 0)
        {
            [tableViewOptions reloadData];
        }
        else
        {
            // tableView.hidden = YES;
            // Here we should show the message like “Your searching item is not available” in UILabel. Also we should set NO to UILabel hidden property.
        }
        
    }
    
}
-(void) searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    
    
    [categoryArray_food removeAllObjects];
    [categoryArray_bar removeAllObjects];
    [searchBar setText:@""];
    
    for (int k=0; k<[self.categoryArray_food1 count]; k++) {
        [categoryArray_food addObject:[self.categoryArray_food1 objectAtIndex:k]];
    }
    for (int k=0; k<[self.categoryArray_bar1 count]; k++) {
        [categoryArray_bar addObject:[self.categoryArray_bar1 objectAtIndex:k]];
    }
    
    NSLog(@"%@",self.categoryArray_food1);
    [tableViewOptions reloadData];
}
-(void) searchBar: (UISearchBar *) searchBar textDidChange:(NSString *) searchText
{
    
    UIView *view = [searchBar.subviews objectAtIndex:0];
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            UIButton *cancelButton = (UIButton *)subView;
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        }
    }
    [categoryArray_bar removeAllObjects];
    [categoryArray_food removeAllObjects];
    [categoryArray_bar release];
    [categoryArray_food release];
    categoryArray_bar = [[NSMutableArray alloc] init];
    categoryArray_food = [[NSMutableArray alloc] init];
    
    
    
    for (int i=0; i < [self.filteredArray_bar count]; i++)
    {
        NSString *arrayString = [[self.filteredArray_bar objectAtIndex: i] categoryName];
        NSString *searchString = searchBar.text;
        
        NSComparisonResult searchResult = [arrayString compare:searchString options:(NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch) range:[arrayString rangeOfString:searchString options:(NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch)]];
        
        if (searchResult == NSOrderedSame)
        {
            
            
            [categoryArray_bar addObject: [self.filteredArray_bar objectAtIndex: i]];
            
        }
        
        
        if ([categoryArray_bar count] > 0)
        {
            [tableViewOptions reloadData];
        }
        else
        {
            // tableView.hidden = YES;
            // Here we should show the message like “Your searching item is not available” in UILabel. Also we should set NO to UILabel hidden property.
        }
        
    }
    for (int i=0; i < [self.filteredArray_food count]; i++)
    {
        NSString *arrayString = [[self.filteredArray_food objectAtIndex: i] categoryName];
        NSString *searchString = searchBar.text;
        
        NSComparisonResult searchResult = [arrayString compare:searchString options:(NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch) range:[arrayString rangeOfString:searchString options:(NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch)]];
        
        if (searchResult == NSOrderedSame)
        {
            [categoryArray_food addObject: [self.filteredArray_food objectAtIndex: i]];
        }
        else
        {
            
        }
        
        
        if ([categoryArray_food count] > 0)
        {
            [tableViewOptions reloadData];
        }
        else
        {
            // tableView.hidden = YES;
            // Here we should show the message like “Your searching item is not available” in UILabel. Also we should set NO to UILabel hidden property.
        }
        
    }
    
}
- (void) removeConnectionClassObject
{
    self.connObject.delegate = nil;
    self.connObject = nil;
}
#pragma mark- MyCabuUserNetwork delegate methods

- (void) connectionEndWithError:(NSError*)error
{
    [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [self removeConnectionClassObject];
}

- (void) connectionEndedSuccefullyWithData:(NSData*)data
{
    NSError *localError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
   
    NSLog(@"MY dictionary====%@",dictionary);
    if (dictionary != nil)
    {
        if ([[dictionary objectForKey:@"cmd"] isEqualToString:@"items"])
        {
             self.itemDetailDict=[NSDictionary dictionaryWithDictionary:dictionary];
            NSString* result = [dictionary objectForKey:@"result"];
            
            if ([result isEqualToString:@"Success"])
            {
                for (UIImageView * tempview in [scrollView_MenuItems subviews]) {
                    [tempview removeFromSuperview];
                }
                int pages=0;
                
                       if ([[dictionary objectForKey:@"data"] count] %4 > 0 ) {
                    pages=(int)( [[dictionary objectForKey:@"data"] count]/4)+1;
                    
                }
                else if ([[dictionary objectForKey:@"data"] count] %4 == 0)
                {
                    pages=(int)([[dictionary objectForKey:@"data"] count] /4);
                    
                }
                
                
                scrollView_MenuItems.contentSize = CGSizeMake(self.view.frame.size.height-301, scrollView_MenuItems.frame.size.height * pages);
                int scrolViewHeight=scrollView_MenuItems.frame.size.height;
                int scrolViewWidth=scrollView_MenuItems.frame.size.width-20;
                
                NSLog(@"WIDTH---: %d  HEIGHT----: %d",scrolViewWidth,scrolViewHeight);
                NSLog(@"SCRLL VIEW WIDTH---: %f  HEIGHT----: %f",scrollView_MenuItems.frame.size.width,scrollView_MenuItems.frame.size.height
                      );
                
                int row,column,rowCount;
                
                if ([[dictionary objectForKey:@"data"] count] %2==0) {
                    rowCount=(int)([[dictionary objectForKey:@"data"] count] /2);
                } else {
                    rowCount=(int)([[dictionary objectForKey:@"data"] count] /2)+1;
                }
                
                int k=0;
                for (row=0; row<rowCount; row++)
                {
                    
                    for (column=0; column<2; column ++)
                    {
                        NSLog(@"COL: %d, ROW: %d",column,row);
                        
                        
                        if ([[dictionary objectForKey:@"data"] count]%2==0)
                        {
                            UIImageView *imgVw=[[UIImageView alloc] init];
                            if (column==0)
                            {
                                
                                [imgVw setFrame:CGRectMake((column *(scrolViewWidth/2))+10, 10+(row*(scrolViewHeight/2)), scrolViewWidth/2-10, scrolViewHeight/2-10)];
                            }
                            else
                            {
                                [imgVw  setFrame:CGRectMake((column *(scrolViewWidth/2))+10,(row*(scrolViewHeight/2))+10, scrolViewWidth/2-10, scrolViewHeight/2-10)];
                            }
                            
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                                
                                __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                                activityIndicator.center = imageView.center;
                                activityIndicator.hidesWhenStopped = YES;
                                [imageView addSubview:activityIndicator];

                                
                                [imgVw setImageWithURL:[NSURL URLWithString:[[[dictionary objectForKey:@"data"] objectAtIndex:k] objectForKey:@"photo_url"]]
                                      placeholderImage:[UIImage imageNamed:@"settings.png"]
                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {  NSLog(@"image setting up ! ");
                                                 
                                                  [activityIndicator removeFromSuperview];
                                             }];
                                
//                                NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[[dictionary objectForKey:@"data"] objectAtIndex:k] objectForKey:@"photo_url"]]];
//                                if (imgData)
//                                {
//                                    UIImage *image = [UIImage imageWithData:imgData];
//                                    if (image)
//                                    {
//                                        dispatch_async(dispatch_get_main_queue(), ^{
//                                            [imgVw setImage:image];  // harry
//                                                                });
//                                    }
//                                    
//                                }
                            });
                            
                            
                            [imgVw setBackgroundColor:[UIColor clearColor]];
                            [imgVw setUserInteractionEnabled:YES];
                            [scrollView_MenuItems addSubview:imgVw];
                            [imgVw release];
                            
                            UIButton *addItemBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                            [addItemBtn setFrame:CGRectMake(imgVw.frame.size.width-50, 0, 50, 50)];
                            [addItemBtn setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
                            [addItemBtn setTitle:@"+" forState:UIControlStateNormal];
//                            [addItemBtn setTag:k];
                            [addItemBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                            [addItemBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0f]];
                            [addItemBtn addTarget:self action:@selector(addItemToCart:) forControlEvents:UIControlEventTouchUpInside];
                            [imgVw addSubview:addItemBtn];
                            
                            UILabel *itemNameLbl=[[UILabel alloc] init];
                            [itemNameLbl setFrame:CGRectMake(imgVw.frame.size.width-150, 150, 150, 60)];
                            [itemNameLbl setNumberOfLines:2.0];
                            [itemNameLbl setFont:[UIFont systemFontOfSize:16.0f]];
                            itemNameLbl.textAlignment=NSTextAlignmentCenter;
                            itemNameLbl.textColor=[UIColor whiteColor];
                            [itemNameLbl setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
                            [itemNameLbl setText:[[[dictionary objectForKey:@"data"] objectAtIndex:k] objectForKey:@"title"]];
                            [imgVw addSubview:itemNameLbl];
                            
                            UIWebView *itemdescLbl=[[UIWebView alloc] init];
                            [itemdescLbl setFrame:CGRectMake(imgVw.frame.size.width-250, 215, 250, 65)];
                            [itemdescLbl loadHTMLString:[NSString stringWithFormat:@"<html><body  text=\"#FFFFFF\"  size=\"0\" >%@</body></html>", [[[dictionary objectForKey:@"data"] objectAtIndex:k] objectForKey:@"description"]] baseURL:nil];
                            [itemdescLbl setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
                            [itemdescLbl setOpaque:NO];
                            [imgVw addSubview:itemdescLbl];
                            
                            UILabel *itemPriceLbl=[[UILabel alloc] init];
                            [itemPriceLbl setFrame:CGRectMake(imgVw.frame.size.width-100, 285, 100, 60)];
                            [itemPriceLbl setNumberOfLines:2.0];
                            [itemPriceLbl setFont:[UIFont systemFontOfSize:17.0f]];
                            itemPriceLbl.textAlignment=NSTextAlignmentCenter;
                            itemPriceLbl.textColor=[UIColor whiteColor];
                            [itemPriceLbl setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
                            [itemPriceLbl setText:[NSString stringWithFormat:@"$%@",[[[dictionary objectForKey:@"data"] objectAtIndex:k] objectForKey:@"price"]]];
                            [imgVw addSubview:itemPriceLbl];
                            
                            UIButton *addItemBtnOverlay=[UIButton buttonWithType:UIButtonTypeCustom];
                            [addItemBtnOverlay setFrame:CGRectMake(0, 0, imgVw.frame.size.width, imgVw.frame.size.height)];
                            [addItemBtnOverlay setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                            [addItemBtnOverlay setTag:k];
                            [addItemBtnOverlay addTarget:self action:@selector(addItemToCart:) forControlEvents:UIControlEventTouchUpInside];
                            [imgVw addSubview:addItemBtnOverlay];
                            
                        }
                        else
                        {
                            if (row== rowCount-1)
                            {
                                UIImageView *imgVw=[[UIImageView alloc] init];
                                if (column==0)
                                {
                                    
                                    [imgVw setFrame:CGRectMake((column *(scrolViewWidth/2))+10, (row*(scrolViewHeight/2))+10, scrolViewWidth/2-10, scrolViewHeight/2-10)];
                                }
                                else
                                {
                                    [imgVw  setFrame:CGRectMake((column *(scrolViewWidth/2))+10,(row*(scrolViewHeight/2))+10, scrolViewWidth/2-10, scrolViewHeight/2-10)];
                                }
                                
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                                    
                                    __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                                    activityIndicator.center = imageView.center;
                                    activityIndicator.hidesWhenStopped = YES;
                                    [imageView addSubview:activityIndicator];
                                    
                                    [imgVw setImageWithURL:[NSURL URLWithString:[[[dictionary objectForKey:@"data"] objectAtIndex:k] objectForKey:@"photo_url"]]
                                          placeholderImage:[UIImage imageNamed:@"settings.png"]
                                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {  NSLog(@"image setting up ! ");
                                                  [activityIndicator removeFromSuperview];
                                                 }];
                                    
//                                    NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[[dictionary objectForKey:@"data"] objectAtIndex:k] objectForKey:@"photo_url"]]];
//                                    if (imgData)
//                                    {
//                                        UIImage *image = [UIImage imageWithData:imgData];
//                                        if (image)
//                                        {
//                                            dispatch_async(dispatch_get_main_queue(), ^{
//                                                [imgVw setImage:image];
//                                                
//                                              
//                                                
//                                            });
//                                        }
//                                        
//                                    }
                                });
                                
                                
                                [imgVw setBackgroundColor:[UIColor clearColor]];
                                [imgVw setUserInteractionEnabled:YES];
                                [scrollView_MenuItems addSubview:imgVw];
                                [imgVw release];
                                
                                UIButton *addItemBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                                [addItemBtn setFrame:CGRectMake(imgVw.frame.size.width-50, 0, 50, 50)];
                                [addItemBtn setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
                                [addItemBtn setTitle:@"+" forState:UIControlStateNormal];
                                [addItemBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                                [addItemBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0f]];
//                                [addItemBtn setTag:k]; // harry
//                                [addItemBtn addTarget:self action:@selector(addItemToCart:) forControlEvents:UIControlEventTouchUpInside];
                                [imgVw addSubview:addItemBtn];
                                
                                UILabel *itemNameLbl=[[UILabel alloc] init];
                                [itemNameLbl setFrame:CGRectMake(imgVw.frame.size.width-150, 150, 150, 60)];
                                [itemNameLbl setNumberOfLines:2.0];
                                [itemNameLbl setFont:[UIFont systemFontOfSize:16.0f]];
                                itemNameLbl.textAlignment=NSTextAlignmentCenter;
                                itemNameLbl.textColor=[UIColor whiteColor];
                                [itemNameLbl setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
                                [itemNameLbl setText:[[[dictionary objectForKey:@"data"] objectAtIndex:k] objectForKey:@"title"]];
                                [imgVw addSubview:itemNameLbl];
                                
                                UIWebView *itemdescLbl=[[UIWebView alloc] init];
                                [itemdescLbl setFrame:CGRectMake(imgVw.frame.size.width-250, 215, 250, 65)];
                                [itemdescLbl loadHTMLString:[NSString stringWithFormat:@"<html><body  text=\"#FFFFFF\"  size=\"5\" >%@</body></html>", [[[dictionary objectForKey:@"data"] objectAtIndex:k] objectForKey:@"description"]] baseURL:nil];
                                [itemdescLbl setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4]];
                                [itemdescLbl setOpaque:NO];
                                [imgVw addSubview:itemdescLbl];
                                
                                
                                UILabel *itemPriceLbl=[[UILabel alloc] init];
                                [itemPriceLbl setFrame:CGRectMake(imgVw.frame.size.width-100, 285, 100, 60)];
                                [itemPriceLbl setNumberOfLines:2.0];
                                [itemPriceLbl setFont:[UIFont systemFontOfSize:17.0f]];
                                itemPriceLbl.textAlignment=NSTextAlignmentCenter;
                                itemPriceLbl.textColor=[UIColor whiteColor];
                                [itemPriceLbl setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
                                [itemPriceLbl setText:[NSString stringWithFormat:@"$%@",[[[dictionary objectForKey:@"data"] objectAtIndex:k] objectForKey:@"price"]]];
                                [imgVw addSubview:itemPriceLbl];
                                
                                UIButton *addItemBtnOverlay=[UIButton buttonWithType:UIButtonTypeCustom];
                                [addItemBtnOverlay setFrame:CGRectMake(0, 0, imgVw.frame.size.width, imgVw.frame.size.height)];
                                [addItemBtnOverlay setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                                [addItemBtnOverlay setTag:k];
                                [addItemBtnOverlay addTarget:self action:@selector(addItemToCart:) forControlEvents:UIControlEventTouchUpInside];
                                [imgVw addSubview:addItemBtnOverlay];
                                
                                
                                break;
                            }
                            else
                            {
                                UIImageView *imgVw=[[UIImageView alloc] init];
                                if (column==0)
                                {
                                    
                                    [imgVw setFrame:CGRectMake((column *(scrolViewWidth/2))+10, (row*(scrolViewHeight/2))+10, scrolViewWidth/2-10, scrolViewHeight/2-10)];
                                }
                                else
                                {
                                    [imgVw  setFrame:CGRectMake((column *(scrolViewWidth/2))+10,(row*(scrolViewHeight/2))+10, scrolViewWidth/2-10, scrolViewHeight/2-10)];
                                }
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                                    
                                    
                                    __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                                    activityIndicator.center = imageView.center;
                                    activityIndicator.hidesWhenStopped = YES;
                                    [imageView addSubview:activityIndicator];

                                    
                                    [imgVw setImageWithURL:[NSURL URLWithString:[[[dictionary objectForKey:@"data"] objectAtIndex:k] objectForKey:@"photo_url"]]
                                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {  NSLog(@"image setting up ! ");
                                                  [activityIndicator removeFromSuperview];
                                                 }];
                                    
                                    
                                    
//                                    NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[[dictionary objectForKey:@"data"] objectAtIndex:k] objectForKey:@"photo_url"]]];
//                                    if (imgData)
//                                    {
//                                        UIImage *image = [UIImage imageWithData:imgData];
//                                        if (image)
//                                        {
//                                            dispatch_async(dispatch_get_main_queue(), ^{
//                                                [imgVw setImage:image];
//                                            });
//                                        }
//                                        
//                                    }
                                });
                                
                                // [imgVw setImage:[UIImage imageNamed:@"3.jpg"]];
                                [imgVw setBackgroundColor:[UIColor clearColor]];
                                [imgVw setUserInteractionEnabled:YES];
                                [scrollView_MenuItems addSubview:imgVw];
                                [imgVw release];
                                
                                
                                UIButton *addItemBtn=[UIButton buttonWithType:UIButtonTypeCustom];
                                [addItemBtn setFrame:CGRectMake(imgVw.frame.size.width-50, 0, 50, 50)];
                                [addItemBtn setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
                                [addItemBtn setTitle:@"+" forState:UIControlStateNormal];
//                                [addItemBtn setTag:k];
                                [addItemBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                                [addItemBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:18.0f]];
//                                [addItemBtn addTarget:self action:@selector(addItemToCart:) forControlEvents:UIControlEventTouchUpInside];
                                [imgVw addSubview:addItemBtn];
                                
                                UILabel *itemNameLbl=[[UILabel alloc] init];
                                [itemNameLbl setFrame:CGRectMake(imgVw.frame.size.width-150, 150, 150, 60)];
                                [itemNameLbl setNumberOfLines:2.0];
                                [itemNameLbl setFont:[UIFont systemFontOfSize:16.0f]];
                                itemNameLbl.textAlignment=NSTextAlignmentCenter;
                                itemNameLbl.textColor=[UIColor whiteColor];
                                [itemNameLbl setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
                                [itemNameLbl setText:[[[dictionary objectForKey:@"data"] objectAtIndex:k] objectForKey:@"title"]];
                                [imgVw addSubview:itemNameLbl];
                                
                                UIWebView *itemdescLbl=[[UIWebView alloc] init];
                                [itemdescLbl setFrame:CGRectMake(imgVw.frame.size.width-250, 215, 250, 65)];
                                [itemdescLbl loadHTMLString:[NSString stringWithFormat:@"<html><body  text=\"#FFFFFF\"  size=\"5\" >%@</body></html>", [[[dictionary objectForKey:@"data"] objectAtIndex:k] objectForKey:@"description"]] baseURL:nil];
                                [itemdescLbl setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
                                [itemdescLbl setOpaque:NO];
                                [imgVw addSubview:itemdescLbl];
                                
                                UILabel *itemPriceLbl=[[UILabel alloc] init];
                                [itemPriceLbl setFrame:CGRectMake(imgVw.frame.size.width-100, 285, 100, 60)];
                                [itemPriceLbl setNumberOfLines:2.0];
                                [itemPriceLbl setFont:[UIFont systemFontOfSize:17.0f]];
                                itemPriceLbl.textAlignment=NSTextAlignmentCenter;
                                itemPriceLbl.textColor=[UIColor whiteColor];
                                [itemPriceLbl setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
                                [itemPriceLbl setText:[NSString stringWithFormat:@"$%@",[[[dictionary objectForKey:@"data"] objectAtIndex:k] objectForKey:@"price"]]];
                                [imgVw addSubview:itemPriceLbl];
                                
                                UIButton *addItemBtnOverlay=[UIButton buttonWithType:UIButtonTypeCustom];
                                [addItemBtnOverlay setFrame:CGRectMake(0, 0, imgVw.frame.size.width, imgVw.frame.size.height)];
                                [addItemBtnOverlay setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                                [addItemBtnOverlay setTag:k];
                                [addItemBtnOverlay addTarget:self action:@selector(addItemToCart:) forControlEvents:UIControlEventTouchUpInside];
                                [imgVw addSubview:addItemBtnOverlay];
                                
                            }
                        }
                        k++;
                    }
                    
                }
                
            }
            else
            {
                
                [Utils showAlertView:result  message: [dictionary objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            }
            
        }
        else if ([[dictionary objectForKey:@"cmd"] isEqualToString:@"ordersubmit"])
        {
            NSString* result = [dictionary objectForKey:@"result"];
            
            if ([result isEqualToString:@"Success"])
            {
                //[Utils showAlertView:result  message: [dictionary objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:result message:[dictionary objectForKey:@"msg"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView setTag:20];
                [alertView show];
                [alertView release];
            }
            else
            {
                [Utils showAlertView:result  message: [dictionary objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            }
            
        }
        else
        {
            NSString* result = [dictionary objectForKey:@"result"];
            
            categoryArray_food=[[NSMutableArray alloc] init];
            categoryArray_bar=[[NSMutableArray alloc] init];
            self.categoryArray_food1=[[NSMutableArray alloc] init];
            self.categoryArray_bar1=[[NSMutableArray alloc] init];
            self.filteredArray_food =[[NSMutableArray alloc] init];
            self.filteredArray_bar =[[NSMutableArray alloc] init];
            
            if ([result isEqualToString:@"Success"])
            {
                
                int count_foodCategory=(int)[[dictionary objectForKey:@"food"] count];
                int count_barCategory=(int)[[dictionary objectForKey:@"bar"] count];
                
                
                for (int i=0; i<count_foodCategory; i++)
                {
                    Category_menu *category=[[Category_menu alloc] init];
                    [category setCategoryID:[[[dictionary objectForKey:@"food"] objectAtIndex:i] objectForKey:@"id"]];
                    [category setCategoryName:[[[dictionary objectForKey:@"food"] objectAtIndex:i] objectForKey:@"category_name"]];
                    [categoryArray_food addObject:category];
                    [self.categoryArray_food1 addObject:category];
                    [self.filteredArray_food addObject:category];
                    [category release];
                }
                
                
                for (int i=0; i<count_barCategory; i++) {
                    Category_menu *category=[[Category_menu alloc] init];
                    [category setCategoryID:[[[dictionary objectForKey:@"bar"] objectAtIndex:i] objectForKey:@"id"]];
                    [category setCategoryName:[[[dictionary objectForKey:@"bar"] objectAtIndex:i] objectForKey:@"category_name"]];
                    [categoryArray_bar addObject:category];
                    [self.categoryArray_bar1 addObject:category];
                    [self.filteredArray_bar addObject:category];
                    [category release];
                }
                
                //                [self.categoryArray_food1 retain];
                //                [self.categoryArray_bar1 retain];
                
                Category_menu *category=[[Category_menu alloc] init];   // for permanent tab food/alcohal
                [category setCategoryID:@"87878787"];
                [category setCategoryName:@"OPEN FOOD"];
                [categoryArray_food insertObject:category atIndex:0];
                
                Category_menu *categoryALCO=[[Category_menu alloc] init];   // for permanent tab food/alcohal
                [categoryALCO setCategoryID:@"86868686"];
                [categoryALCO setCategoryName:@"OPEN ALCOHAL"];
                [categoryArray_bar insertObject:categoryALCO atIndex:0];
                [tableViewOptions reloadData];
            }
            else
            {
                      [Utils showAlertView:result  message: [dictionary objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            }
        }
    }
    else
    {
        [Utils showAlertView:@"Alert" message:@"There has been a network error, Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    [self removeConnectionClassObject];
}
- (void)deleteItemNotification:(NSNotification *)notification
{
    NSDictionary *dictionary = [notification userInfo];
    [self.cartItemUpdateArr removeObjectAtIndex:[[dictionary valueForKey:@"row_deleted"] integerValue]];
    [cartItemsArr removeObjectAtIndex:[[dictionary valueForKey:@"row_deleted"] integerValue]];
}
- (void) useNotificationWithString:(NSNotification *)notification //use notification method and logic
{
    
    
    NSDictionary *dictionary = [notification userInfo];
    NSLog(@"stringValueToUse --> %@",dictionary);
    popUp_updateOrderList=0;
    UpdateOrderDetails *updateOrderItem = [[UpdateOrderDetails alloc] init];
    [updateOrderItem setManagerID:[dictionary valueForKey:kmanagerID]];
    [updateOrderItem.instructionTxt setText:[dictionary valueForKey:ksplInstr]];
    [updateOrderItem setIsModifier1:[dictionary valueForKey:kmodifier1]];
    [updateOrderItem setIsModifier2:[dictionary valueForKey:kmodifier2]];
    [updateOrderItem setIsModifier3:[dictionary valueForKey:kmodifier3]];
    [updateOrderItem setIsModifier4:[dictionary valueForKey:kmodifier4]];
    [updateOrderItem setIsComplimentary:[dictionary valueForKey:kComplimentry]];
    [self setUiasView_updateOrderList:updateOrderItem];
    [updateOrderItem release];
    
     [self.uiasView_updateOrderList.closeBtn setTag:[[dictionary valueForKey:@"row"] integerValue]];
    [self.uiasView_updateOrderList.closeBtn addTarget:self action:@selector(closeUpdateItemBtn:) forControlEvents:UIControlEventTouchUpInside];
     [self.uiasView_updateOrderList.UpdateBtn addTarget:self action:@selector(closeUpdateItemBtn:) forControlEvents:UIControlEventTouchUpInside];
   
    [self.uiasView_updateOrderList setFrame:CGRectMake(0, self.view.frame.size.height , self.view.frame.size.width, self.view.frame.size.height)];
    
   
    
    [UIView animateWithDuration:0.40f animations:^{
        
       
        CGRect uiasViewFrame   = self.uiasView_updateOrderList.frame;
        uiasViewFrame.origin.y =0;
        self.uiasView_updateOrderList.frame = uiasViewFrame;
        [self.view addSubview:self.uiasView_updateOrderList];

        
    } completion:^(BOOL finished) {
        [self.uiasView_currentOrderList.tblList1 setUserInteractionEnabled:YES]; // enable disable for prevanting double tap
        [self.uiasView_updateOrderList setUI];
    }];
}
- (void) closeUpdateItemBtn:(id)sender
{
    popUp_updateOrderList=1;
    [UIView animateWithDuration:0.40f animations:^{
        
        
        CGRect uiasViewFrame   = self.uiasView_updateOrderList.frame;
        uiasViewFrame.origin.y = self.view.frame.size.height ;
        
        self.uiasView_updateOrderList.frame = uiasViewFrame;
        
        
        
    } completion:^(BOOL finished)
     {
        
         NSMutableDictionary *dic_itemDetail=[[NSMutableDictionary alloc] init];
         [dic_itemDetail setObject:self.uiasView_updateOrderList.instructionTxt.text forKey:ksplInstr];
         [dic_itemDetail setObject:self.uiasView_updateOrderList.isModifier1 forKey:kmodifier1];
         [dic_itemDetail setObject:self.uiasView_updateOrderList.isModifier2 forKey:kmodifier2];
         [dic_itemDetail setObject:self.uiasView_updateOrderList.isModifier3 forKey:kmodifier3];
         [dic_itemDetail setObject:self.uiasView_updateOrderList.isModifier4 forKey:kmodifier4];
         [dic_itemDetail setObject:self.uiasView_updateOrderList.managerID forKey:kmanagerID];
         [dic_itemDetail setObject:self.uiasView_updateOrderList.isComplimentary forKey:kComplimentry];
         
         [self.cartItemUpdateArr removeObjectAtIndex:[sender tag]];
         [self.cartItemUpdateArr insertObject:dic_itemDetail atIndex:[sender tag]];

         [self.uiasView_updateOrderList removeFromSuperview];
     }];

}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==40)
    {
        if (buttonIndex==0) {
            OrderTypeClass *orderType = [[OrderTypeClass alloc] init];
            [self setUiasView_orderType:orderType];
            [orderType release];
            [self.uiasView_orderType setFrame:CGRectMake(0, self.view.frame.size.width , self.view.frame.size.width, self.view.frame.size.height)];
            [self.uiasView_orderType.closeBtn addTarget:self action:@selector(orderTypeCloseBtnClicked) forControlEvents:UIControlEventTouchUpInside];
            [self.uiasView_orderType.dineInBtn addTarget:self action:@selector(orderTypeDineBtnClicked) forControlEvents:UIControlEventTouchUpInside];
            [self.uiasView_orderType.takeAwayBtn addTarget:self action:@selector(orderTypeTakeAwayBtnClicked) forControlEvents:UIControlEventTouchUpInside];
            [self.uiasView_orderType.barBtn addTarget:self action:@selector(orderTypebarBtnClicked) forControlEvents:UIControlEventTouchUpInside];
            
            popUp_orderType=0;
            // show new order screen
            [UIView animateWithDuration:0.60f animations:^{
                
                
                CGRect uiasViewFrame   = self.uiasView_orderType.frame;
                uiasViewFrame.origin.y =0;
                self.uiasView_orderType.frame = uiasViewFrame;
                [self.view addSubview:self.uiasView_orderType];
                
                
            } completion:^(BOOL finished) {
                
                
            }];

        }
    }
    else
    {
        if (buttonIndex==0)
        {
            for (UIViewController *controller in self.navigationController.viewControllers)
            {
            if ([controller isKindOfClass:[HomeViewController class]]) {
                    
                    [self.navigationController popToViewController:controller
                                                          animated:YES];
                    break;
                }
            }
        }
    }
}

-(void) submitOrderBtn:(id)sender
{
    if ([cartItemsArr count]==0)
    {
        [Utils showAlertView:@"Alert" message:@"Please add an item to the Order" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    
//    else if (!iSAgreeForTax)
//    {
//        [Utils showAlertView:@"Alert" message:@"Please check Tax exempt checkmark " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//
//    }
    
    else
    {
            NSMutableArray *arrForJson=[[NSMutableArray alloc] init];
            int j=(int)[cartItemsArr count]-1;
            for (int k=0; k<[cartItemsArr count]; k++)
     
            {
                NSMutableDictionary *dic_itemDetail=[[NSMutableDictionary alloc] init];
                
                UILabel *lblPrice=(UILabel*)[[[[[[[self.uiasView_currentOrderList.tblList1 subviews] objectAtIndex:0] subviews] objectAtIndex:j] contentView] subviews] objectAtIndex:6];// harry because of crashing adding more itemes upto 8 - 10 , need solve
                UILabel *lblQty=(UILabel*)[[[[[[[self.uiasView_currentOrderList.tblList1 subviews] objectAtIndex:0] subviews] objectAtIndex:j] contentView] subviews] objectAtIndex:4];// harry because of crashing adding more itemes upto 8 - 10 , need solve
                
                [dic_itemDetail setObject:[(Category_ItemDetail*)[cartItemsArr objectAtIndex:k] itemName]  forKey:@"item_name"];
                [dic_itemDetail setObject:[(Category_ItemDetail*)[cartItemsArr objectAtIndex:k] itemID] forKey:@"item_id"];
                [dic_itemDetail setObject:[(Category_ItemDetail*)[cartItemsArr objectAtIndex:k] categoryID] forKey:@"category_id"];
                
                [dic_itemDetail setObject:lblQty.text forKey:@"quantity"];// harry because of crashing adding more itemes upto 8 - 10 , need solve
                [dic_itemDetail setObject:[lblPrice.text substringFromIndex:1] forKey:@"price"];// harry because of crashing adding more itemes upto 8 - 10 , need solve
                [dic_itemDetail setObject:[(Category_ItemDetail*)[cartItemsArr objectAtIndex:k] itemType] forKey:@"bar"];
                [dic_itemDetail setObject:[[self.cartItemUpdateArr objectAtIndex:k] objectForKey:ksplInstr] forKey:@"instruction"];
                [dic_itemDetail setObject:[[self.cartItemUpdateArr objectAtIndex:k] objectForKey:kmodifier1] forKey:@"modifier1"];
                [dic_itemDetail setObject:[[self.cartItemUpdateArr objectAtIndex:k] objectForKey:kmodifier2] forKey:@"modifier2"];
                [dic_itemDetail setObject:[[self.cartItemUpdateArr objectAtIndex:k] objectForKey:kmodifier3] forKey:@"modifier3"];
                [dic_itemDetail setObject:[[self.cartItemUpdateArr objectAtIndex:k] objectForKey:kmodifier4] forKey:@"modifier4"];
                [dic_itemDetail setObject:[[self.cartItemUpdateArr objectAtIndex:k] objectForKey:kmanagerID] forKey:@"manager_id"];
                [dic_itemDetail setObject:[[self.cartItemUpdateArr objectAtIndex:k] objectForKey:kComplimentry] forKey:@"complimentary"];
                
                j--;
                 
                [arrForJson addObject:dic_itemDetail];
//                NSLog(@"data source111 ----%@",[[[self.uiasView_currentOrderList.tblList1 subviews] objectAtIndex:0] subviews] );//harry because of crashing adding more itemes upto 8 - 10 , need solve
//                NSLog(@"data source qty----%@",[[[[[[[self.uiasView_currentOrderList.tblList1 subviews] objectAtIndex:0] subviews] objectAtIndex:k] contentView] subviews] objectAtIndex:4] ); // harry because of crashing adding more itemes upto 8 - 10 , need solve
                
                
//                NSLog(@"data source price----%@",lblPrice.text );// harry because of crashing adding more itemes upto 8 - 10 , need solve
            }
            
            // NSDictionary *allPersonDic=[NSDictionary dictionaryWithObject:arrForJson forKey:@"Data"];
            NSError* error;
            NSData* jsonData = [NSJSONSerialization dataWithJSONObject:arrForJson
                                                               options:kNilOptions error:&error];
            
            
            [arrForJson release];
            NSString* strWithImg = [[NSString alloc] initWithData:jsonData
                                                         encoding:NSUTF8StringEncoding] ;
            NSLog(@"JSON-- %@",strWithImg );
        NSString * oAmt = [ofoodAlcoDic objectForKey:@"amt"];
        NSString * oInstruction = [ofoodAlcoDic objectForKey:@"ins"];

        
//            NSString *jsonString=  [strWithImg stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            if (![Utils connected])
            {
                [Utils showAlertView:@"Network Error" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            }
            else
            {
                NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
                [def synchronize];
                ServerConnectionClass* network = [[[ServerConnectionClass alloc]init] autorelease];
                self.connObject = network;
                self.connObject.delegate = self;
                NSString* urlString=[[NSString alloc]initWithFormat:@"%@{\"user_id\":\"%d\",\"table_id\":\"%d\", \"open_food\":\"%@\",\"open_bar\":\"%@\",\"amount\":\"%@\",\"instruction\":\"%@\",\"order_details\":%@}",kSubmitOrderUrl,[iRestaurantsGlobalDataClass getInstance].loggedUserId,[_tableID intValue],@"1",@"1",oAmt,oInstruction,strWithImg];
                [self.connObject makeRequestForUrl:urlString];
                [urlString release];
            }
        }
    }

@end


