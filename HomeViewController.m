//
//  HomeViewController.m
//  iROS
//
//  Created by iMac Apple on 04/07/14.
//  Copyright (c) 2014 Dex Consulting. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "SplitPaymentByPeopleViewController.h"
#import "ViewController.h"
#import "rasterPrinting.h"
#import "LayoutViewController.h"
#import "MenuViewController.h"
#import "PaymentOptionVC.h"
#include "iROS/iRestaurantsGlobalDataClass.h"
#include "OrderDecriptionForPay.h"
#include "LAYOUTRESTOBARVC.h"
#include "TripAdjustmentVC.h"

@interface HomeViewController ()

@end

@implementation HomeViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
      
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    IsGotoPayment =false;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kIsEmpLoggedIn];

}

-(void)viewDidAppear:(BOOL)animated
{
    [self allOrders];
    if (  [iRestaurantsGlobalDataClass getInstance].attendanceFlag == 0) {
//        [self btnTime:nil];
    }
  
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIView *headerVw=[[UIView alloc] init];
    headerVw.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
    {
        [headerVw setBackgroundColor:kDarkBrownColor];
    }
    else
    {
        [headerVw setBackgroundColor:kOrangeColor];
    }
    
    [self.view addSubview:headerVw];
    [headerVw release];
    
    UIImageView *mainbg_img = [[UIImageView alloc] init];
    mainbg_img.frame=CGRectMake(30, 17.5, 200, 95);
    mainbg_img.userInteractionEnabled=TRUE;
    mainbg_img.opaque = YES;
    [mainbg_img setImage:[UIImage imageNamed:@"logo2.png"]];
    [headerVw addSubview:mainbg_img];
    [mainbg_img release];

    
    
    UIButton * attendanceBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [attendanceBtn setFrame:CGRectMake(self.view.frame.size.width-335, 38, 60, 60)];
    if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
    {
         [attendanceBtn setBackgroundImage:[UIImage imageNamed:@"graph_bar"] forState:UIControlStateNormal];
    }
    else
    {
         [attendanceBtn setBackgroundImage:[UIImage imageNamed:@"graph"] forState:UIControlStateNormal];
    }
   
    [attendanceBtn addTarget:self action:@selector(btnTime:) forControlEvents:UIControlEventTouchUpInside];
    [headerVw addSubview:attendanceBtn];
    
    
    
    UIButton * empIdsBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [empIdsBtn setFrame:CGRectMake(attendanceBtn.frame.origin.x+62, attendanceBtn.frame.origin.y, attendanceBtn.frame.size.width, attendanceBtn.frame.size.height)];
    if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
    {
        [empIdsBtn setBackgroundImage:[UIImage imageNamed:@"empId_gray.png"] forState:UIControlStateNormal];
    }
    else
    {
        [empIdsBtn setBackgroundImage:[UIImage imageNamed:@"empId.png"] forState:UIControlStateNormal];
    }
    
    [empIdsBtn addTarget:self action:@selector(btnTime:) forControlEvents:UIControlEventTouchUpInside];
    [headerVw addSubview:empIdsBtn];

    UIButton * printSettingsBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [printSettingsBtn setFrame:CGRectMake(empIdsBtn.frame.origin.x+62, attendanceBtn.frame.origin.y, attendanceBtn.frame.size.width, attendanceBtn.frame.size.height)];
    if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
    {
       [printSettingsBtn setBackgroundImage:[UIImage imageNamed:@"print_bar"] forState:UIControlStateNormal];
    }
    else
    {
        [printSettingsBtn setBackgroundImage:[UIImage imageNamed:@"print"] forState:UIControlStateNormal];
    }
    
    [printSettingsBtn addTarget:self action:@selector(printerSettings:) forControlEvents:UIControlEventTouchUpInside];
    [headerVw addSubview:printSettingsBtn];
    
    UIButton * orderTypeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [orderTypeBtn setFrame:CGRectMake(printSettingsBtn.frame.origin.x+62, attendanceBtn.frame.origin.y, attendanceBtn.frame.size.width, attendanceBtn.frame.size.height)];

    if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
    {
        [orderTypeBtn setBackgroundImage:[UIImage imageNamed:@"settings_bar"] forState:UIControlStateNormal];
    }
    else
    {
         [orderTypeBtn setBackgroundImage:[UIImage imageNamed:@"settings"] forState:UIControlStateNormal];
    }
   
    [orderTypeBtn addTarget:self action:@selector(placeNewOrder) forControlEvents:UIControlEventTouchUpInside];
    [headerVw addSubview:orderTypeBtn];
    
    UIButton * graphBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [graphBtn setFrame:CGRectMake(orderTypeBtn.frame.origin.x+62, attendanceBtn.frame.origin.y, attendanceBtn.frame.size.width, attendanceBtn.frame.size.height)];

    if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
    {
        [graphBtn setBackgroundImage:[UIImage imageNamed:@"signin_bar"] forState:UIControlStateNormal];
    }
    else
    {
        [graphBtn setBackgroundImage:[UIImage imageNamed:@"signin"] forState:UIControlStateNormal];
    }
    
    [graphBtn addTarget:self action:@selector(logOut) forControlEvents:UIControlEventTouchUpInside];
    [headerVw addSubview:graphBtn];
    
    UIView *sectionView=[[UIView alloc] init];
    sectionView.frame=CGRectMake(0, 120, self.view.frame.size.width, 50);
    [sectionView setBackgroundColor:[UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1]];
    [headerVw addSubview:sectionView];
    [sectionView release];
    
    UILabel *Emp_nameLbl=[[UILabel alloc] init];
    Emp_nameLbl.frame=CGRectMake(headerVw.frame.size.width/2-70, 50.0, 120, 50);
    [Emp_nameLbl setText:@"Harendra S"];
    //    [Emp_nameLbl setText:_empname];
    [Emp_nameLbl setTextColor:[UIColor whiteColor]];
    [Emp_nameLbl setFont:[UIFont systemFontOfSize:22.0f]];
    [Emp_nameLbl setTextAlignment:NSTextAlignmentCenter];
    [Emp_nameLbl setBackgroundColor:[UIColor clearColor]];
    [headerVw addSubview:Emp_nameLbl];
    [Emp_nameLbl release];

    
    UILabel *userTypeLbl=[[UILabel alloc] init];
    userTypeLbl.frame=CGRectMake(30, 0, 150, 50);
    [userTypeLbl setText:@"Waiter/Bartender"];
    [userTypeLbl setTextAlignment:NSTextAlignmentCenter];
    [userTypeLbl setBackgroundColor:[UIColor clearColor]];
    [sectionView addSubview:userTypeLbl];
    [userTypeLbl release];
    
    UILabel *tableIDLbl=[[UILabel alloc] init];
    tableIDLbl.frame=CGRectMake(170, 0, 150, 50);
    [tableIDLbl setText:@"Table ID"];
    [tableIDLbl setTextAlignment:NSTextAlignmentCenter];
    [tableIDLbl setBackgroundColor:[UIColor clearColor]];
    [sectionView addSubview:tableIDLbl];
    [tableIDLbl release];
    
    UILabel *orderStatusLbl=[[UILabel alloc] init];
    orderStatusLbl.frame=CGRectMake(320, 0, 150, 50);
    [orderStatusLbl setText:@"Order Status"];
    [orderStatusLbl setTextAlignment:NSTextAlignmentCenter];
    [orderStatusLbl setBackgroundColor:[UIColor clearColor]];
    [sectionView addSubview:orderStatusLbl];
    [orderStatusLbl release];
    
    UILabel *timeLbl=[[UILabel alloc] init];
    timeLbl.frame=CGRectMake(470, 0, 150, 50);
    [timeLbl setText:@"Time"];
    [timeLbl setTextAlignment:NSTextAlignmentCenter];
    [timeLbl setBackgroundColor:[UIColor clearColor]];
    [sectionView addSubview:timeLbl];
    [timeLbl release];
    
    UILabel *payLbl=[[UILabel alloc] init];
    payLbl.frame=CGRectMake(640, 0, 120, 50);
    [payLbl setText:@"Payment"];
    [payLbl setTextAlignment:NSTextAlignmentCenter];
    [payLbl setBackgroundColor:[UIColor clearColor]];
    [sectionView addSubview:payLbl];
    [payLbl release];
    
    UILabel *printLbl=[[UILabel alloc] init];
    printLbl.frame=CGRectMake(770, 0, 120, 50);
    [printLbl setText:@"Print"];
    [printLbl setTextAlignment:NSTextAlignmentCenter];
    [printLbl setBackgroundColor:[UIColor clearColor]];
    [sectionView addSubview:printLbl];
    [printLbl release];
    
    UILabel *TipLbl=[[UILabel alloc] init];
    TipLbl.frame=CGRectMake(875, 0, 120, 50);
    [TipLbl setText:@"Tip Adjustment"];
    [TipLbl setTextAlignment:NSTextAlignmentCenter];
    [TipLbl setBackgroundColor:[UIColor clearColor]];
    [sectionView addSubview:TipLbl];
    [TipLbl release];
    
    UITableView *tblList=[[UITableView alloc] init];
    self.tblList=tblList;
    [tblList release];
    self.tblList.frame=CGRectMake(0, 173, self.view.frame.size.width, self.view.frame.size.height-245);
    [self.tblList setDelegate:self];
    [self.tblList setDataSource:self];
    [headerVw addSubview:self.tblList];
    
    
    UIView *footerVw=[[UIView alloc] init];
    footerVw.frame=CGRectMake(0, self.tblList.frame.origin.y+self.tblList.frame.size.height+3, self.view.frame.size.width, self.view.frame.size.width-footerVw.frame.origin.y);
    [footerVw setBackgroundColor:[UIColor whiteColor]];
    [headerVw addSubview:footerVw];
    [footerVw release];
    
    
    UIButton * newOrderBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [newOrderBtn setFrame:CGRectMake(30, 15, 120, 40)];
    if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
    {
        [newOrderBtn setBackgroundColor:kDarkBrownColor];
    }
    else
    {
        [newOrderBtn setBackgroundColor:kOrangeColor];
    }
    
    [newOrderBtn setTitle:@"Take Away" forState:UIControlStateNormal];
    [newOrderBtn.titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
    [newOrderBtn addTarget:self action:@selector(TakeAwayClicked) forControlEvents:UIControlEventTouchUpInside];
    [footerVw addSubview:newOrderBtn];
    
    if ([[defaults objectForKey:kuserType] isEqualToString:@"Manager"])
    {
        [newOrderBtn setHidden:YES];
    }
    else
    {
        [newOrderBtn setHidden:NO];
    }
    
    UIButton * allOrderBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [allOrderBtn setFrame:CGRectMake(165, 15, 120, 40)];
    if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
    {
        [allOrderBtn setBackgroundColor:kDarkBrownColor];
    }
    else
    {
        [allOrderBtn setBackgroundColor:kOrangeColor];
    }
    
    [allOrderBtn setTitle:@"Table Layout" forState:UIControlStateNormal];
     [newOrderBtn.titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
    [allOrderBtn addTarget:self action:@selector(TablLayoutClicked:) forControlEvents:UIControlEventTouchUpInside];
    [footerVw addSubview:allOrderBtn];
   
    if ([[defaults objectForKey:kuserType] isEqualToString:@"Manager"])
    {
        [allOrderBtn setHidden:YES];
    }
    else
    {
        [allOrderBtn setHidden:NO];
    }
    
    
    UIButton * tipAdjBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [tipAdjBtn setFrame:CGRectMake(300, 15, 150, 40)];
    if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
    {
        [tipAdjBtn setBackgroundColor:kDarkBrownColor];
    }
    else
    {
        [tipAdjBtn setBackgroundColor:kOrangeColor];
    }
    
    [tipAdjBtn setTitle:@"Tip Adjustment" forState:UIControlStateNormal];
    [newOrderBtn.titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
    [tipAdjBtn addTarget:self action:@selector(ClickOnTipBtn:) forControlEvents:UIControlEventTouchUpInside];
    [footerVw addSubview:tipAdjBtn];
    
    
    
    
    
    UILabel *poweredLbl=[[UILabel alloc] init];
    poweredLbl.frame=CGRectMake(850, 35, 190, 40);
    [poweredLbl setText:@"Powered By:Webtye"];
    [poweredLbl setTextAlignment:NSTextAlignmentCenter];
    [poweredLbl setBackgroundColor:[UIColor clearColor]];
    [poweredLbl setFont:[UIFont systemFontOfSize:12.0f]];
    if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
    {
        [poweredLbl setTextColor:kDarkBrownColor];
    }
    else
    {
        [poweredLbl setTextColor:kOrangeColor];
    }

    
    [footerVw addSubview:poweredLbl];
    [poweredLbl release];
      
    PaymentTypeClass *newOrder = [[PaymentTypeClass alloc] init];
    [self setUiasView:newOrder];
    [self.uiasView setFrame:CGRectMake(0, self.view.frame.size.height , self.view.frame.size.width, self.view.frame.size.height)];
    [self.uiasView.closeBtn addTarget:self action:@selector(closeBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.uiasView.goBtn addTarget:self action:@selector(okBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    
    OrderTypeClass *orderType = [[OrderTypeClass alloc] init];
    [self setUiasView_orderType:orderType];
    [self.uiasView_orderType setFrame:CGRectMake(0, self.view.frame.size.height , self.view.frame.size.height, self.view.frame.size.width)];
    [self.uiasView_orderType.closeBtn addTarget:self action:@selector(orderTypeCloseBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.uiasView_orderType.dineInBtn addTarget:self action:@selector(orderTypeDineBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.uiasView_orderType.takeAwayBtn addTarget:self action:@selector(orderTypeTakeAwayBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.uiasView_orderType.barBtn addTarget:self action:@selector(orderTypebarBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    MarkAttendanceClass *markAttendance = [[MarkAttendanceClass alloc] init];
    [self setUiasView_markAttendance:markAttendance];
    [self.uiasView_markAttendance setFrame:CGRectMake(0, self.view.frame.size.height , self.view.frame.size.width, self.view.frame.size.height)];
    [self.uiasView_markAttendance.closeBtn addTarget:self action:@selector(attendanceCloseBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    
    SearchPrinterClass *printersList = [[SearchPrinterClass alloc] init];
    [self setUiasView_printerList:printersList];
    [self.uiasView_printerList setFrame:CGRectMake(0, self.view.frame.size.height , self.view.frame.size.width, self.view.frame.size.height)];
    [self.uiasView_printerList.closeBtn addTarget:self action:@selector(printerListCloseBtnClicked) forControlEvents:UIControlEventTouchUpInside];

    [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(updateTableView) userInfo:nil repeats:YES];
}

- (void) updateTableView
{
    [self.tblList reloadData];
}
- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
 
    
    return [self.allOrdersArr count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//    if (cell == nil)  // not need becase we need button index in gotopayment fuction for un visible cell
//    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        
        UILabel *lblAdmin=[[UILabel alloc]initWithFrame:CGRectMake(10,0,150,51)];
        [lblAdmin setTag:10];
        [lblAdmin setBackgroundColor:[UIColor clearColor]];
        [lblAdmin setTextAlignment:NSTextAlignmentCenter];

        [cell.contentView addSubview:lblAdmin];
        [lblAdmin release];
        
        
        UILabel *lblToken=[[UILabel alloc]initWithFrame:CGRectMake(lblAdmin.frame.origin.x +210,0,62,51)];
        [lblToken setBackgroundColor:[UIColor clearColor]];
        [lblToken setTag:20];
        [cell.contentView addSubview:lblToken];
        [lblToken release];
        
        UILabel *lblOrder=[[UILabel alloc]initWithFrame:CGRectMake(lblToken.frame.origin.x +92,0,150,51)];
        
        [lblOrder setTag:30];
        [lblOrder setTextAlignment:NSTextAlignmentCenter];
        [lblOrder setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:lblOrder];
        [lblOrder release];
        
        
        UILabel *lblTime=[[UILabel alloc]initWithFrame:CGRectMake(lblOrder.frame.origin.x +160,0,150,51)];
        
        [lblTime setTag:40];
        [lblTime setTextAlignment:NSTextAlignmentCenter];
        [lblTime setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:lblTime];
        [lblTime release];
        
        UIButton *btnPayment=[[UIButton alloc]initWithFrame:CGRectMake(lblTime.frame.origin.x +190,10,80,31)];
        [btnPayment setBackgroundColor:[UIColor clearColor]];
        
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [btnPayment setTitleColor:kDarkBrownColor forState:UIControlStateNormal];
        }
        else
        {
            [btnPayment setTitleColor:kOrangeColor forState:UIControlStateNormal];
        }
        [btnPayment.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
        [btnPayment setTag:indexPath.row+2000];
        [btnPayment addTarget:self action:@selector(GoToProcessPayment:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:btnPayment];
        [btnPayment.layer setBorderWidth:1.0f];
        
        
        UIButton *btnPrinter=[[UIButton alloc]initWithFrame:CGRectMake(btnPayment.frame.origin.x +100,0,120,51)];
        [btnPrinter setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:btnPrinter];
        [btnPrinter setTag:indexPath.row+1000];
        UIImageView *imgvw2=[[UIImageView alloc] init];
        [imgvw2 setFrame:CGRectMake(30, -2, 60, 60)];
        
       
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [btnPayment.layer setBorderColor:kDarkBrownColor.CGColor];
            [imgvw2 setImage:[UIImage imageNamed:@"print_icon_bar.png"]];
        }
        else
        {
            [btnPayment.layer setBorderColor:kOrangeColor.CGColor];
            [imgvw2 setImage:[UIImage imageNamed:@"print_icon.png"]];
        }
         [btnPrinter addSubview:imgvw2];
        
        UIButton *btnTip=[[UIButton alloc]initWithFrame:CGRectMake(btnPrinter.frame.origin.x +135,10,80,31)];
        [btnTip setBackgroundColor:[UIColor clearColor]];
        [btnTip setTag:12775];
        if ([[defaults objectForKey:kuserType] isEqualToString:@"Bar Tender"])
        {
            [btnTip setTitleColor:kDarkBrownColor forState:UIControlStateNormal];
            [btnTip.layer setBorderColor:kDarkBrownColor.CGColor];
        }
        else
        {
            [btnTip setTitleColor:kOrangeColor forState:UIControlStateNormal];
            [btnTip.layer setBorderColor:kOrangeColor.CGColor];

        }
        [btnTip.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
//        [btnTip setTag:indexPath.row+2000];
        [btnTip addTarget:self action:@selector(ClickOnTipBtn:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:btnTip];
        [btnTip.layer setBorderWidth:1.0f];

//    }
//    UILabel *lblAdmin=(UILabel*)[cell viewWithTag:10];

    NSString *str = [[self.allOrdersArr objectAtIndex:indexPath.row] objectForKey:@"user_type"];
    if (str && ![str isKindOfClass:[NSNull class]] ) {
        [lblAdmin setText:str];
    }
    
//    UILabel *lblToken=(UILabel*)[cell viewWithTag:20];
    [lblToken setText:[[self.allOrdersArr objectAtIndex:indexPath.row] objectForKey:@"table_id"]];
    
//    UILabel *lblOrder=(UILabel*)[cell viewWithTag:30];
    [lblOrder setText:[[self.allOrdersArr objectAtIndex:indexPath.row] objectForKey:@"order_status"]];
    if ([[lblOrder text]isEqualToString:@"Done"]) {
        [lblOrder setTextColor:[UIColor greenColor]];
    }
    else{
        [lblOrder setTextColor:[UIColor blackColor]];

    }
    
//    UILabel *lblTime=(UILabel*)[cell viewWithTag:40];
    [lblTime setText:[self dateConversion:indexPath.row]];
    
   
    UIButton *btnEnterTip=(UIButton*)[cell viewWithTag:12775];
  
//    UIButton *btnPayment=(UIButton*)[cell viewWithTag:indexPath.row+2000];
    btnPayment=(UIButton*)[cell viewWithTag:indexPath.row+2000];
    [btnPayment setTitle:[[self.allOrdersArr objectAtIndex:indexPath.row] objectForKey:@"payment_status"] forState:UIControlStateNormal];
    
    if ([btnPayment.titleLabel.text isEqualToString:@"Paid"]) {
        [btnPayment setUserInteractionEnabled:false];
        [btnEnterTip setUserInteractionEnabled:false];
        [cell setUserInteractionEnabled:false];
        [btnEnterTip setTitle:@"Tip settled" forState:UIControlStateNormal];
    }
    else
    {
        [btnPayment setUserInteractionEnabled:true];
        [btnEnterTip setUserInteractionEnabled:true];
        [btnEnterTip setTitle:@"Enter Tip " forState:UIControlStateNormal];
        [cell setUserInteractionEnabled:true];



    }
    
    
    
    // UIButton *btnPrinter=(UIButton*)[cell viewWithTag:indexPath.row+1000];
    
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 51;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![Utils connected])
    {
        [Utils showAlertView:@"Network Error" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    else
    {
        ServerConnectionClass* network = [[[ServerConnectionClass alloc]init] autorelease];
        self.connObject = network;
        self.connObject.delegate = self;
        tableID=[[self.allOrdersArr objectAtIndex:indexPath.row] objectForKey:@"table_id"];
        NSString* urlString=[[NSString alloc]initWithFormat:@"%@{\"order_id\":\"%@\" }",kCurrentOrderDetailsUrl,[[self.allOrdersArr objectAtIndex:indexPath.row] objectForKey:@"order_id"]];
        [self.connObject makeRequestForUrl:urlString];
        [urlString release];
    }
}


-(NSString*)dateConversion:(int)row
{
    NSDate *today=[NSDate date];
    NSDateComponents *weekdayComponents;
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate *date1=[dateFormatter dateFromString:[[self.allOrdersArr objectAtIndex:row] objectForKey:@"created_on"]];
    
    [dateFormatter release];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
   
    weekdayComponents =[calendar components: NSHourCalendarUnit | NSMinuteCalendarUnit  fromDate:date1 toDate:today options:NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit];
    
    [calendar release];
    NSInteger hour=[weekdayComponents hour];
    NSInteger minutes =[weekdayComponents minute];
   
    NSString *strHour=[NSString stringWithFormat:@"%ld",(long)hour];
    NSString *strMin=[NSString stringWithFormat:@"%d",minutes];
    NSLog(@"%@,%@ ",strHour,strMin);


    return [NSString stringWithFormat:@"%@:%@",strHour,strMin];
 }



#pragma mark - Button Actions

- (void)btnTime:(id)sender
{
    [iRestaurantsGlobalDataClass getInstance].attendanceFlag = 1;
    popUp_markAttendance=0;
 
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    BOOL flag = [[defaults objectForKey:kIsEmpLoggedIn]boolValue];
    
    if (flag) {
        [_uiasView_markAttendance.clockInBtn setHidden:true];
        [_uiasView_markAttendance.clockOutBtn setHidden:false];

    }
    else  {
        [_uiasView_markAttendance.clockOutBtn setHidden:true];
        [_uiasView_markAttendance.clockInBtn setHidden:false];

    }

    // show new order screen
    [UIView animateWithDuration:0.60f animations:^{
        
        
        CGRect uiasViewFrame   = self.uiasView_markAttendance.frame;
        uiasViewFrame.origin.y =0;
        self.uiasView_markAttendance.frame = uiasViewFrame;
        [self.view addSubview:self.uiasView_markAttendance];
        
        
    } completion:^(BOOL finished) {
        
        
    }];


    
}
-(void)attendanceCloseBtnClicked
{
    popUp_markAttendance=1;
    [UIView animateWithDuration:0.75f animations:^{
        
        
        CGRect uiasViewFrame   = self.uiasView_markAttendance.frame;
        uiasViewFrame.origin.y = self.view.frame.size.height ;
        
        self.uiasView_markAttendance.frame = uiasViewFrame;
        
        
        
    } completion:^(BOOL finished)
     {
         // completion block is important in each case. Use it
         [self.uiasView_markAttendance removeFromSuperview];
     }];
}

-(void)paymentBtnClicked:(UIButton*)btn
{
    /*
    orderIdForPayment = btn.tag;
    popUp=0;
    // show new order screen
    [UIView animateWithDuration:0.60f animations:^{
        
        
        CGRect uiasViewFrame   = self.uiasView.frame;
        uiasViewFrame.origin.y =0;
        self.uiasView.frame = uiasViewFrame;
        [self.view addSubview:self.uiasView];
        
        [self.uiasView.proceedBtn addTarget:self action:@selector(GoToProcessPayment:) forControlEvents:UIControlEventTouchUpInside];
        
        
    } completion:^(BOOL finished) {
        
  
    }];*/
}
-(void) closeBtnClicked
{
    popUp=1;
    [UIView animateWithDuration:0.75f animations:^{
        
        
        CGRect uiasViewFrame   = self.uiasView.frame;
        uiasViewFrame.origin.y = self.view.frame.size.height ;
        
        self.uiasView.frame = uiasViewFrame;
        
        
        
    } completion:^(BOOL finished)
     {
         // completion block is important in each case. Use it
         [self.uiasView removeFromSuperview];
     }];
}


-(void) printerListCloseBtnClicked
{
    popUp_printerList=1;
    [UIView animateWithDuration:0.75f animations:^{
        
        
        CGRect uiasViewFrame   = self.uiasView_printerList.frame;
        uiasViewFrame.origin.y = self.view.frame.size.width ;
        
        self.uiasView_printerList.frame = uiasViewFrame;
        
        
        
    } completion:^(BOOL finished)
     {
         // completion block is important in each case. Use it
         [self.uiasView_printerList removeFromSuperview];
     }];
}

-(void) orderTypeCloseBtnClicked
{
    popUp_orderType=1;
    [UIView animateWithDuration:0.75f animations:^{
        
        
        CGRect uiasViewFrame   = self.uiasView_orderType.frame;
        uiasViewFrame.origin.y = self.view.frame.size.height ;
        
        self.uiasView_orderType.frame = uiasViewFrame;
        
        
        
    } completion:^(BOOL finished)
     {
         // completion block is important in each case. Use it
         [self.uiasView_orderType removeFromSuperview];
     }];
}

-(void) orderTypeDineBtnClicked
{
    UIImageView *imgvw=(UIImageView*)[[self.uiasView_orderType.dineInBtn subviews] objectAtIndex:0];
    if (![self.uiasView_orderType.dineInBtn isSelected])
    {
        [self.uiasView_orderType.dineInBtn setSelected:YES];
        [imgvw setImage:[UIImage imageNamed:@"radio_on"]];
        
        [self.uiasView_orderType.takeAwayBtn setSelected:NO];
        [self.uiasView_orderType.barBtn setSelected:NO];
        UIImageView *imgvw_split=(UIImageView*)[[self.uiasView_orderType.barBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_off"]];
        UIImageView *imgvw_split1=(UIImageView*)[[self.uiasView_orderType.takeAwayBtn
                                                  subviews] objectAtIndex:0];
        [imgvw_split1 setImage:[UIImage imageNamed:@"radio_off"]];
        
        
        
        
        LayoutViewController *layoutVC=[[LayoutViewController alloc] init];
        [layoutVC setUserType:@"Waiter"];
        [self.navigationController pushViewController:layoutVC animated:YES];
        [layoutVC release];
        [self orderTypeCloseBtnClicked];
       
    }
    else
    {
        [self.uiasView_orderType.dineInBtn setSelected:NO];
        [imgvw setImage:[UIImage imageNamed:@"radio_off"]];
        
        
    }

}
-(void) orderTypeTakeAwayBtnClicked
{
    UIImageView *imgvw=(UIImageView*)[[self.uiasView_orderType.takeAwayBtn subviews] objectAtIndex:0];
    if (![self.uiasView_orderType.takeAwayBtn isSelected])
    {
        [self.uiasView_orderType.takeAwayBtn setSelected:YES];
        [imgvw setImage:[UIImage imageNamed:@"radio_on"]];
        
        [self.uiasView_orderType.dineInBtn setSelected:NO];
        [self.uiasView_orderType.barBtn setSelected:NO];
        UIImageView *imgvw_split=(UIImageView*)[[self.uiasView_orderType.barBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_off"]];
        UIImageView *imgvw_split1=(UIImageView*)[[self.uiasView_orderType.dineInBtn
                                                  subviews] objectAtIndex:0];
        [imgvw_split1 setImage:[UIImage imageNamed:@"radio_off"]];
    }
    else
    {
        [self.uiasView_orderType.takeAwayBtn setSelected:NO];
        [imgvw setImage:[UIImage imageNamed:@"radio_off"]];
    }
}

-(void) orderTypebarBtnClicked
{
    UIImageView *imgvw=(UIImageView*)[[self.uiasView_orderType.barBtn subviews] objectAtIndex:0];
    if (![self.uiasView_orderType.barBtn isSelected])
    {
        [self.uiasView_orderType.barBtn setSelected:YES];
        [imgvw setImage:[UIImage imageNamed:@"radio_on"]];
        [self.uiasView_orderType.takeAwayBtn setSelected:NO];
        [self.uiasView_orderType.dineInBtn setSelected:NO];
        UIImageView *imgvw_split=(UIImageView*)[[self.uiasView_orderType.dineInBtn subviews] objectAtIndex:0];
        [imgvw_split setImage:[UIImage imageNamed:@"radio_off"]];
        UIImageView *imgvw_split1=(UIImageView*)[[self.uiasView_orderType.takeAwayBtn
                                                  subviews] objectAtIndex:0];
        [imgvw_split1 setImage:[UIImage imageNamed:@"radio_off"]];
        LayoutViewController *layoutVC=[[LayoutViewController alloc] init];
         [layoutVC setUserType:@"Bar Tender"];
        [self.navigationController pushViewController:layoutVC animated:YES];
        [layoutVC release];
        [self orderTypeCloseBtnClicked];
    }
    else
    {
        [self.uiasView_orderType.barBtn setSelected:NO];
        [imgvw setImage:[UIImage imageNamed:@"radio_off"]];
    }
}
- (IBAction)btnSettingPrinter:(id)sender
{
    
}

- (IBAction)TablLayoutClicked:(id)sender
{
//    LayoutViewController *layoutVC=[[LayoutViewController alloc] init];

    LAYOUTRESTOBARVC *layoutVC=[[LAYOUTRESTOBARVC alloc] init];
//    [layoutVC setUserType:@"Waiter"];
    [self.navigationController pushViewController:layoutVC animated:YES];
    [layoutVC release];
}

-(void)TakeAwayClicked
{
    MenuViewController *menuVC=[[MenuViewController alloc] init];
//    [menuVC setFromClass:@"LayoutClass"];
    [menuVC setTableID:@"Take Away"]; // tag is alredy seting up earlier method
    [menuVC setFromClass:@"LayoutClass"]; // for using same funtionality like table layout
    [self.navigationController pushViewController:menuVC animated:YES];
    [menuVC release];
    
}

- (void) btnSettings:(id)sender
{
    popUp_orderType=0;
    // show new order screen
    [UIView animateWithDuration:0.60f animations:^{
        
        
        CGRect uiasViewFrame   = self.uiasView_orderType.frame;
        uiasViewFrame.origin.y =0;
        self.uiasView_orderType.frame = uiasViewFrame;
        [self.view addSubview:self.uiasView_orderType];
        
        
    } completion:^(BOOL finished) {
        
        
    }];
}

- (void)printerSettings:(id)sender
{
//    ViewController *viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil] ;
//    [self.navigationController pushViewController:viewController animated:YES];
//    [viewController release];
          
    
    NSArray *array = [[SMPort searchPrinter:@"TCP:"] retain];
    self.uiasView_printerList.foundPrinters=array;
    self.uiasView_printerList.delegate=self;
    popUp_printerList=0;
    // show new order screen
    [UIView animateWithDuration:0.60f animations:^{
        
        
        CGRect uiasViewFrame   = self.uiasView_printerList.frame;
        uiasViewFrame.origin.y =0;
        self.uiasView_printerList.frame = uiasViewFrame;
        [self.view addSubview:self.uiasView_printerList];
        
        
    } completion:^(BOOL finished) {
        
        
    }];

//        searchView = [[SearchPrinterViewController alloc] initWithNibName:@"SearchPrinterViewController" bundle:nil];
//        searchView.foundPrinters = array;
//        searchView.delegate = self;
//        [self presentViewController:searchView animated:YES completion:nil];
//        [searchView release];
        [array release];
   

}
-(void)logOut
{
    /* harry testing only
    rasterPrinting *rasterPrintingVar = [[rasterPrinting alloc] initWithNibName:@"rasterPrinting" bundle:[NSBundle mainBundle]];
    [self presentViewController:rasterPrintingVar animated:YES completion:nil];
    [rasterPrintingVar setOptionSwitch:NO];
    [rasterPrintingVar release];
     
     */
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void) okBtnClicked
{
    SplitPaymentByPeopleViewController *newVC=[[SplitPaymentByPeopleViewController alloc] init];
    [self.navigationController pushViewController:newVC animated:YES];
    [newVC release];
}

- (void)returnSelectedCellText
{
      NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
      selectedPortName = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"PrinterPortName"]];
//    selectedPortName = [searchView lastSelectedPortName];
    
    if ((selectedPortName != nil) && ([selectedPortName isEqualToString:@""] == NO))
    {
        //uitextfield_portname.text = selectedPortName;
    }
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//    [prefs setObject:selectedPortName forKey:@"PrinterPortName"];
//    [prefs synchronize];
    
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self printerListCloseBtnClicked];
    
    if (selectedPortName !=nil) {
        [self setPortInfo];
        
        NSString *portName = [AppDelegate getPortName];
        NSString *portSettings = [AppDelegate getPortSettings];
        portName = [AppDelegate getPortName];
        portSettings = [AppDelegate getPortSettings];
        
        
        rasterPrinting *rasterPrintingVar = [[rasterPrinting alloc] initWithNibName:@"rasterPrinting" bundle:[NSBundle mainBundle]];
        [self presentViewController:rasterPrintingVar animated:YES completion:nil];
        [rasterPrintingVar setOptionSwitch:NO];
        [rasterPrintingVar release];
    }
    
    message = [NSString stringWithFormat:@"Portname - %@ \n PortNAme pref - %@ - " , [AppDelegate getPortName], selectedPortName];
    [self sendMail];
 }

+ (void)setPortName:(NSString *)m_portName
{
    [AppDelegate setPortName:m_portName];
}

+ (void)setPortSettings:(NSString *)m_portSettings
{
    [AppDelegate setPortSettings:m_portSettings];
}

- (void)setPortInfo
{
    NSString *localPortName = [NSString stringWithString: selectedPortName];
    [HomeViewController setPortName:localPortName];
    
    [HomeViewController setPortSettings:@"Standard"];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:selectedPortName forKey:@"PrinterPortName"];
    
    [prefs setObject:selectedPortName forKey:@"PortSettings"];
    
}
- (void) placeNewOrder
{
    OrderTypeClass *orderType = [[OrderTypeClass alloc] init];
    [self setUiasView_orderType:orderType];
    [orderType release];
    [self.uiasView_orderType setFrame:CGRectMake(0, self.view.frame.size.width , self.view.frame.size.width, self.view.frame.size.height)];
    [self.uiasView_orderType.closeBtn addTarget:self action:@selector(orderTypeCloseBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.uiasView_orderType.dineInBtn addTarget:self action:@selector(orderTypeDineBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.uiasView_orderType.takeAwayBtn addTarget:self action:@selector(orderTypeTakeAwayBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.uiasView_orderType.barBtn addTarget:self action:@selector(orderTypebarBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    
    popUp_orderType=0;
    // show new order screen
    [UIView animateWithDuration:0.60f animations:^{
        
        
        CGRect uiasViewFrame   = self.uiasView_orderType.frame;
        uiasViewFrame.origin.y =0;
        self.uiasView_orderType.frame = uiasViewFrame;
        [self.view addSubview:self.uiasView_orderType];
        
        
    } completion:^(BOOL finished) {
        
        
    }];
    
}


-(void)sendMail
{
    NSString * msg = [NSString stringWithFormat:@"FOUND PRINTER  - %@",message];
    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setSubject:@"PRINTER DETAILS"];
    [mailComposer setMessageBody:msg isHTML:NO];
    [self presentViewController:mailComposer animated:NO completion:nil];
}

#pragma mark - mail compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:NO completion:nil];
    
}


- (void) allOrders
{
    if (![Utils connected])
    {
        [Utils showAlertView:@"Network Error" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    else
    {
        ServerConnectionClass* network = [[[ServerConnectionClass alloc]init] autorelease];
        self.connObject = network;
        self.connObject.delegate = self;
//        NSString* urlString=[[NSString alloc]initWithFormat:@"%@{\"status\":\"1\" }",kAllOrdersUrl];
        NSString* urlString=[[NSString alloc]initWithFormat:@"%@{\"user_id\":\"%d\" }",kCurrentOrdersUrl,[iRestaurantsGlobalDataClass getInstance].loggedUserId];
        [self.connObject makeRequestForUrl:urlString];
        [urlString release];
    }
}

-(IBAction)GoToProcessPayment:(UIButton*)btn
{
      if (![Utils connected])
    {
        [Utils showAlertView:@"Network Error" message:@"Please check your internet connection." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    else
    {
        ServerConnectionClass* network = [[[ServerConnectionClass alloc]init] autorelease];
        self.connObject = network;
        self.connObject.delegate = self;
        NSString* urlString=[[NSString alloc]initWithFormat:@"%@{\"order_id\":\"%@\" }",kCurrentOrderDetailsUrl,[[self.allOrdersArr objectAtIndex:(btn.tag-2000)] objectForKey:@"order_id"]];
        [iRestaurantsGlobalDataClass getInstance].orderidForpay = [[self.allOrdersArr objectAtIndex:(btn.tag-2000)] objectForKey:@"order_id"];
        
        [self.connObject makeRequestForUrl:urlString];
        [urlString release];
        IsGotoPayment = YES;
    }

    
    /*
    
//    [iRestaurantsGlobalDataClass getInstance].amtToPay = amtTobePay;
    
    [self closeBtnClicked]; // close panel befor go next, preventing crash on poping back to this view
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"iPad" bundle:nil];   // testing
    OrderDecriptionForPay *ADFP = (OrderDecriptionForPay *)[storyboard instantiateViewControllerWithIdentifier:@"OrderDecriptionForPay"];
//    ADFP.orderListArr  = arr.mutableCopy;
    
    [iRestaurantsGlobalDataClass getInstance].orderidForpay = [[self.allOrdersArr objectAtIndex:(btn.tag-2000)] objectForKey:@"order_id"];
    [self.navigationController  pushViewController:ADFP animated:YES ];
     
     */
     
}



- (void) removeConnectionClassObject
{
    self.connObject.delegate = nil;
    self.connObject = nil;
}
#pragma mark- MyCabuUserNetwork delegate methods

- (void) connectionEndWithError:(NSError*)error
{
    [Utils showAlertView:@"Connection Alert" message:@"There has been a network error, Please try again.\nThank You." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [self removeConnectionClassObject];
}

- (void) connectionEndedSuccefullyWithData:(NSData*)data
{
    NSError *localError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    iRestaurantsGlobalDataClass *obj=[iRestaurantsGlobalDataClass getInstance];
     
    NSLog(@"MY dictionary====%@",dictionary);
    if (dictionary != nil)
    {
        NSString* result = [dictionary objectForKey:@"result"];
        if ([[dictionary objectForKey:@"cmd"] isEqualToString:@"orderall"] || [[dictionary objectForKey:@"cmd"] isEqualToString:@"ordercurrent"])
        {
        
            
            
            if ([result isEqualToString:@"Success"])
            {
                
                self.allOrdersArr=[NSArray arrayWithArray:[dictionary objectForKey:@"order_detail"]];
                [self.tblList reloadData];
                
                for (int k=0; k<[[dictionary objectForKey:@"waiter_table"] count]; k++) {
                    
                    [obj.tableBookedArr_food addObject:[[[dictionary objectForKey:@"waiter_table"] objectAtIndex:k] objectForKey:@"waiter_table"]];
                    
                }
                for (int k=0; k<[[dictionary objectForKey:@"bar_table"] count]; k++)
                {
                    
                    [obj.tableBookedArr_bar addObject:[[[dictionary objectForKey:@"bar_table"] objectAtIndex:k] objectForKey:@"bar_table"]];
                    
                }
            }
            else
            {
                
                [Utils showAlertView:result  message: [dictionary objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            }
            
        }
       else if ([[dictionary objectForKey:@"cmd"] isEqualToString:@"orderitemdetail"] && !IsGotoPayment )
       {
           if ([result isEqualToString:@"Success"])
           {
               NSArray *arr=[NSArray arrayWithArray:[dictionary objectForKey:@"order_detail"]];
               MenuViewController *menuVC=[[MenuViewController alloc] init];
               [menuVC setTableID:tableID];
               [menuVC setFromClass:@"HomeClass"];
               
               NSMutableArray *cartItemsArr=[[NSMutableArray alloc] init];
               NSMutableArray *cartItemUpdateArr=[[NSMutableArray alloc] init];
               for (int k=0; k<[arr count]; k++) {
                   
                   Category_ItemDetail *itemDetailCls=[[Category_ItemDetail alloc] init];
                   [itemDetailCls setCategoryID:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"category_id"]];
                   [itemDetailCls setItemID:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"item_id"]];
                   [itemDetailCls setItemName:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"item_name"]];
                   [itemDetailCls setManagerID:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"manager_id"]];
                   [itemDetailCls setModifier1:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"modifier1"]];
                   [itemDetailCls setModifier2:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"modifier2"]];
                   [itemDetailCls setModifier3:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"modifier3"]];
                   [itemDetailCls setModifier4:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"modifier4"]];
                   [itemDetailCls setPrice:    [[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"price"] ];
                   [itemDetailCls setQuantity: [[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"quantity"]];
                   [itemDetailCls setComplimentry:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"complimentry"]];
                   [itemDetailCls setSpecialInstruction:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"instructions"]];
                   
                   UIImageView *itemImg=[[UIImageView alloc]initWithFrame:CGRectMake(10,10,140,131)];
                   
                  // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
     /*  // harry crashing
                   
                   NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"photo_url"]]];
                       if (imgData)
                       {
                           UIImage *image = [UIImage imageWithData:imgData];
                           if (image)
                           {
                               [itemImg setImage:image];
                              // dispatch_async(dispatch_get_main_queue(), ^{
                                   [itemDetailCls setImgVw:itemImg];
                                   
                              // });
                           }
                       }
*/
                   //});
                  
                    //[itemDetailCls setImgVw:itemDetails.imgView];
                   
                   NSMutableDictionary *dic_itemDetail=[[NSMutableDictionary alloc] init];
                   [dic_itemDetail setObject:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"instructions"] forKey:ksplInstr];
                   [dic_itemDetail setObject:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"modifier1"] forKey:kmodifier1];
                   [dic_itemDetail setObject:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"modifier2"] forKey:kmodifier2];
                   [dic_itemDetail setObject:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"modifier3"] forKey:kmodifier3];
                   [dic_itemDetail setObject:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"modifier4"] forKey:kmodifier4];
                   [dic_itemDetail setObject:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"manager_id"] forKey:kmanagerID];
                   [dic_itemDetail setObject:[[[dictionary objectForKey:@"order_detail"] objectAtIndex:k] objectForKey:@"complimentry"] forKey:kComplimentry];
                   
                   [cartItemUpdateArr addObject:dic_itemDetail];
                   [cartItemsArr addObject:itemDetailCls];
                   [dic_itemDetail release];
                   [itemDetailCls release];
               }
               [menuVC setCartItemUpdateArr:cartItemUpdateArr];
               [menuVC setCartItemsArr_toupdate:cartItemsArr];
               [self.navigationController pushViewController:menuVC animated:YES];
               [menuVC release];
               [cartItemsArr release];
               [cartItemUpdateArr release];



           }
       }
        
       else if ([[dictionary objectForKey:@"cmd"] isEqualToString:@"orderitemdetail"] && IsGotoPayment)
       {
           if ([result isEqualToString:@"Success"])
           {
               NSArray *arr=[NSArray arrayWithArray:[dictionary objectForKey:@"order_detail"]];             
               float amtTobePay= 0.0;
               for (int k=0; k<[arr count]; k++)
               {
                   NSDictionary *tempDic = [arr objectAtIndex:k];
                   amtTobePay += [[tempDic objectForKey:@"price"]floatValue];
                }
               
               [iRestaurantsGlobalDataClass getInstance].amtToPay = amtTobePay;
           
//               UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"iPad" bundle:nil];   // testing
//               UIViewController *controller = (UIViewController *)
//               [storyboard instantiateViewControllerWithIdentifier:@"1"];
//               [self.navigationController  pushViewController:controller animated:YES ];
  
              
               [self closeBtnClicked]; // close panel befor go next, preventing crash on poping back to this view
               
               UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"iPad" bundle:nil];   // testing
               OrderDecriptionForPay *ADFP = (OrderDecriptionForPay *)[storyboard instantiateViewControllerWithIdentifier:@"OrderDecriptionForPay"];
               ADFP.orderListArr  = arr.mutableCopy;
               [self.navigationController  pushViewController:ADFP animated:YES ];
           }
       }
    }
    else
    {
        [Utils showAlertView:@"Alert" message:@"There has been a network error, Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    
    [self removeConnectionClassObject];
}


-(IBAction)ClickOnTipBtn:(id)sender
{
    [iRestaurantsGlobalDataClass getInstance];
    TripAdjustmentVC *VC=[[TripAdjustmentVC alloc] init];
    [self.navigationController pushViewController:VC animated:YES];
    [VC release];

    return; // we don't want to read below lines harrry
    
    
    
    
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter Tip Amount:" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
  
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [[alertView textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
    [[alertView textFieldAtIndex:0] setDelegate:self];


//    [alertView setTag:10];
    [alertView show];
    [alertView release];


}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1)
    {
        UITextField *txtTipAmt = [alertView textFieldAtIndex:0];
//        NSString *aStr = [txtTipAmt text];
    }
}



@end
